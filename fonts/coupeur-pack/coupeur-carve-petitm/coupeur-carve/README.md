# Coupeur Carve

The Coupeur Carve is a fork derivated from the <a href="https://github.com/cooperhewitt/cooperhewitt-typeface">Cooper Hewitt Family</a>, created by <a href="http://vllg.com/news/214">~Chester Jenkins~</a> for the Cooper Hewitt Museum.

Licensed under SIL Open Font License (http://scripts.sil.org/cms/scripts/page.php?item\_id=OFL-FAQ\_web)

It was created in a weekend workshop (18th and 19th of June, 2016), organised by <a href="http://velvetyne.fr">Velvetyne Type Foundry</a>, a French foundry dedicated to libre, open-source fonts.

These fonts are a collaboration between <a href="mailto:jeannefrantz@gmail.com">Jeanne Frantz</a>, <a href="www.behance.net/CamilleRigouChemin">Camille Rigou-Chemin</a> and <a href="http://vincentricard.co">Vincent Ricard</a>.

## Design

We took three weights of the greatly designed Cooper Hewitt typeface, and forked it. We carved angles into the round letters. Some became strictly geometrical. We hope you will love using it as much as we do.
