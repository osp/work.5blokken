# Ma fonte

Fonte basée sur un principe de graisse inversé.
Créé par Domitille Debret, Florian Michelet et Saulou Margaux à l’occasion du workshop FontFonkFork organisé par la fonderie Velvetyne à la Générale, Paris, 19/06/2016.

## License

Mafonte porte la licence [SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")
