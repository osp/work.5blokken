# Ma fonte

Fork inspiré par le museum d'histoire , le squelette de la Font.
Éditée sur Processing et Glyph.
Police réalisée par Arthur Dinant lors du Workshop FontFonkFork.

## License

Mafonte porte la licence [SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")
