# Coupeur Ligadom Medium

This Fork of Cooper Hewitt typography is full of ligatures and random characters. It is designed to evoke the irregularity of letters written by hand, in order to give back a sensibility to Cooper Hewitt typography.

Pauline Pourcelot
Tibo
Fanny Guilhen

Workshop FontFonkFork

## License

Coupeur Ligadom Medium porte la licence [SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")
