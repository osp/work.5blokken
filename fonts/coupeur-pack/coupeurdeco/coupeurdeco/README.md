# Coupeur Deco

Coupeur Deco is based upon the bold version of Cooper Hewitt. Its lively and decorative features draw inspiration from the american popular culture and Cooper Hewitt Museum's collections.
Coupeur Deco is a prototype, an incomplete font, made during a 2-day workshop. It is under the SIL license, feel free to use it, complete it, fork it, etc.

Yohanna My Nguyen

## License

[SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")
