# Coupeur Almeida

Version incise du Cooper Hewitt par Thomas Bouillet.
Dessiné lors du workshop Font Fonk Fork organisé par VTF en juin 2016.

## License

Coupeur Almeida porte la licence [SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")
