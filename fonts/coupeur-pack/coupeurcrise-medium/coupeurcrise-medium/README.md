# CoupeurCrise

CoupeurCrise est un caractère forké du CooperHewitt (dessiné par Chester Jenkins pour le musée du même nom). Il a été dessiné par Aurélia de Azambuja dans le cadre d'un workshop de 2 jours à la Générale organisé par la fonderie Velvetyne (http://velvetyne.fr).

## License
CoupeurCrise porte la licence [SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")
