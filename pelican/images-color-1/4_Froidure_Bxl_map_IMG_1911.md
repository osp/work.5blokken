title: 4_Froidure_Bxl_map_IMG_1911
type: image
size: medium
category: 1
cols: 16 / span 8
rows: page-9-8 / span 6

<div style="position: relative; top: -8mm" markdown="true">
![]({attach}4_Froidure_Bxl_map_IMG_1911.jpg)

De Marollen en Anneessens blijven de armste wijken van de Vijfhoek (Froidure, 1957)

Les Marolles et Anneessens, toujours les quartiers les plus démunis du Pentagone (Froidure, 1957)

</div>