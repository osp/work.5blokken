import math

def get_article_order (article):
  try:
    if hasattr(article, 'order'):
      return int(article.order)
    else:
      return math.inf
  except:
    return math.inf