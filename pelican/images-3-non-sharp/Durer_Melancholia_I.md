title: Durer_Melancholia_I.jpg
type: image
rows: page-9-1 / span 10
cols: 2 / span 6
size: medium
category: 3


![]({attach}Durer_Melancholia_I.jpg)

De engel, in gepeins verzonken, komt niet aan daden toe - Albrecht Dürer, 1514

L’ange, plongé dans la contemplation, est incapable d’agir - Albrecht Dürer, 1514 