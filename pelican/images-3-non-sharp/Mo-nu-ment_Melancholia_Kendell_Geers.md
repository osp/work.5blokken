title: Mo-nu-ment_Melancholia_Kendell_Geers.jpg
type: image
rows: page-8-5 / span 3
cols: 20 /span 3
size: medium
category: 3


![]({attach}Mo-nu-ment_Melancholia_Kendell_Geers.jpg)

Sculptuur van Kendell Geers aan de 5 blokken, in de vorm van Dürer’s polyeder

Sculpture de Kendell Geers, aux 5 blocs, dans la forme du polyèdre de Dürer