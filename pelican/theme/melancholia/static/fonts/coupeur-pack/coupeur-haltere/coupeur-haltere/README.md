# Coupeur Haltère
Coupeur Haltère est un fork du Cooper Hewitt par Alexandre Lescieux & Hadrien Bulliat, réalisé dans le cadre d’un workshop organisé par VTF.

## License

Mafonte porte la licence [SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")
