# Coupeur Texte

Ce caractère est un fork du Cooper Hewitt Medium destiné à un usage de texte courant, réalisé dans le cadre du workshop Font Fonk Fork organisé par Velvetyne Type Foundry à Paris le 18-19 juin 2016. Il a été dessiné par Léa Rolland (learolland@yahoo.fr).

## License

Coupeur Texte porte la licence [SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")
