# Coupeur de Légumes

Cette fonte qui est un fork de la Cooper Hewitt-Book à été réalisée  par Clément Baudouin, étudiant à l'ERG dans le cadre du workshop FontFonkFork organisée par l'équique de Velvetyne Type Foundry à La Générale à Paris les 18 et 19 juin 2016.
Son processus de création à débuté par une envie de déconstruction et remplacement de formes par dautres existantes aussi dans la Cooper Hewitt. Suite à ces expériences est venue l'envie de création d'ouvertertures dans ces caractères très férmés, la volonté était non seulement de l'adoucir mais aussi de lui ajouter de l'aiguiser.

## License

Coupeur de Légumes porte la licence [SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")
