# Coupeur Mono

Réalisée dans le cadre d'un atelier organisé par la fonderie [Velvetyne](http://velvetyne.fr/) à [La Générale](http://ff.fr) en juin 2016, la Coupeur Mono est une dérivation du caractère Cooper Hewitt réalisé par Chester Jenkins en 2014 dans le cadre d'une commande d'identité visuelle pour le musée du même nom à New York.

La volonté du musée de proposer cette typographie sous licence libre correspond à des interrogations contemporaines liées à la création et à l'économie en général, c'est un exemple encourageant qui montre qu'une alternative à la propriété intelectuelle et au modèle capitaliste est possible. Les licences libres et le travail collaboratif aparaissent avec le développement de logiciels à l'aune de l'informatique. La programmation se caractérisant notament par l'utilisation de typographies à espace fixe, l'ajout de caractès monospace à la famille Cooper Hewitt est un hommage aux pionners du libre.

## License

Mafonte porte la licence [SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")



**Martin Campillo & Laurie Giraud, MAOUS Collectif 2016**