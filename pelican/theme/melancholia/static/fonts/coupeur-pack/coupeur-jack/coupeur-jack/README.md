# Coupeur Jack

Version semi-serif du Cooper Hewitt qui envoie du couteau par Hugo Dumont.
Dessiné lors du workshop Font Fonk Fork organisé par VTF en juin 2016.

## License

Coupeur Jack porte la licence [SIL Open Font License (OFL)](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL "SIL Open Font License")
