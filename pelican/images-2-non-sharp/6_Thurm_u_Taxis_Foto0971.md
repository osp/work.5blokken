title: 6_Thurm_u_Taxis_Foto0971
type: image
rows: page-8-1 / span 4
cols: 5 / span 4
source: references
size: large
category: 2


![]({attach}6_Thurm_u_Taxis_Foto0971.jpg)

Tour & Taxis (foto 2012), vroeger publiek domein, nu eigendom van de holding Ackermans & van Haaren

Tour & Taxis (photo 2012), avant domaine public, appartient au holding Ackermans & van Haaren
