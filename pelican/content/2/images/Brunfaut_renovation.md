title: Brunfaut_renovation
type: image
rows: page-9-1 / span 4
cols: 19 / span 5
size: medium
category: 2
source: references


![]({attach}Brunfaut_renovation.png)

De ‘zesde’ toren (Brunfaut-straat, Molenbeek) wordt gerenoveerd

La ’sixième’ tour (rue Brunfaut, Molenbeek) sera rénovée
