Title: Sous le <br>marais, les <br>lucioles
Type: essay
category: 3
rows: page-7-1
lang: fr
count: 5

### De la mélancolie

#### Raf Custers

*Mélancolie aux 5 blocs (pourquoi ne se révoltent-ils pas?)* : voilà qui
n’est pas un titre commode. Quand on se veut moderne, on donne à son
produit un nom de 2-3 syllabes maximum (Up-site, TheOne, Belair ...).
Sommes-nous modernes ? Oui. Fabriquons-nous un produit ? Non. Aussi le
titre convient-il fort bien.

<span class="number">1</span>Les premières fois que nous nous sommes rendus aux 5 blocs, il y
régnait un sentiment indécis. L’hiver se traînait, il faisait gris, il
n’y avait jamais grand-monde à voir dehors. M., qui y animait le *Projet
de Cohésion Sociale*, disait : les gens sont défaitistes. Nous ne
mettions pas ce constat en doute, nous étions enclins à suivre M. parce
que nous étions nouveaux dans le quartier et parce qu’il y travaillait
depuis deux ou trois ans déjà. Que les gens soient abattus, nous
pouvions le comprendre. Nous savions que ces blocs allaient être rasés
au bout du compte. On ne pouvait rien y faire. Nous pensions : les gens
sont en état de choc. Car ils devront quitter leur quartier pour faire
de la place à un public plus ‘civilisé' et mieux nanti.
{: .section-start}

Voilà comment notre raison nous expliquait la situation que nous
pensions rencontrer aux 5 blocs. Mais cette analyse tout en blanc et
noir empêche de voir d’autres événements. Car les habitants des 5 blocs
réagissent, de toutes sortes de façons. Il y sont bien obligés. Quand
nous avons commencé à le comprendre, le titre du spectacle coulait de
source : *Mélancolie aux 5 blocs (pourquoi ne se révoltent-ils pas?)*.

<span class="number">2</span>À partir de début 2017, nous sommes allés aux 5 blocs avec des
chanteurs et des chanteuses de la chorale. Nous avons pratiqué une sorte
d’ethnographie en amateurs : observer comment les gens vivent, sentir
des sensations, les odeurs des cuisines, les bruits venant des fenêtres,
dans les portiques, écouter les habitants, manger avec eux, bavarder,
passer quand on a le temps, remettre de temps à autre en ordre ce qu’on
a vécu et voir si on peut y lire une trame.
{: .section-start}

Nous étions occupés à tout cela depuis un trimestre à peine quand le
thème de la ‘mélancolie’ s’est imposé à nous. Par un détour fort
lointain, en fait. C’est qu’une conférence venait d'être donnée à
l’université de São Paulo au Brésil avec pour titre ‘La mélancolie dans
l’inégalité urbaine'. Un exposé à propos des bidonvilles brésiliens.
D’où la question : ce thème peut-il s’appliquer aux cités et aux
logements sociaux chez nous ?

La conférence pouvait se lire [**en
ligne**](https://erminiamaricato.net/2017/16/melancolia-na-desigualdade-urbana/).
Elle était l’œuvre d’une experte sur le sujet des favelas au Brésil,
l’ancienne vice-ministre Erminia Maricato. J’admirais le travail
d’Erminia Maricato. J’avais appris son existence à Rio de Janeiro, lors
des grandes manifestations de rue de 2013, quand elle avait participé à
la rédaction de *Cidades Rebeldes*, un livre sur la ville en tant que
scène des révoltes sociales.

Le mouvement de protestation éclata en mai-juin. Très vite, des millions
de personnes sont descendues en rue dans toutes les grandes villes du
Brésil. Elles étaient furieuses parce qu’on avait augmenté le prix des
tickets de métro et de bus alors qu’au même moment, on dépensait des
milliards pour construire les stades de deux méga-événements
qu’accueillait le Brésil, la coupe du monde de football de 2014 et les
Jeux olympiques d'été de 2016. À l'été 2013, le mouvement s’est élargi.
Il était désormais dirigé contre l’ensemble du système brésilien, dominé
par une caste blanche hyper-conservatrice et raciste. Nul, dans ce
système, n’avait anticipé cette révolte urbaine. La caste des décideurs
était perplexe. La seule réaction qu’ils pussent imaginer fut de
recourir à une répression musclée.

Au plus fort des protestations, Maricato et d’autres auteurs publièrent
*Cidades Rebeldes*.[^1] Le livre s’inspirait de *Rebel Cities*, un
livre de David Harvey dont un article figurait dans l’ouvrage brésilien.
Dans son article, Maricato expliquait les fissures traversant le système
brésilien. Elle citait des chiffres sur le fléau que constitue, encore
et toujours, la violence exercée contre le peuple dans les villes. Les
auteurs des homicides appartiennent souvent à la police mais ‘les
victimes des assassinats sont généralement jeunes, noirs et habitant
dans les périphéries les plus éloignées des métropoles'.

Les gens connaissent un état de mélancolie collective, soutient Maricato
dans son exposé de 2016 à propos des favelas et des banlieues lointaines
des métropoles. Cet état d’esprit vient des conditions dans lesquelles
ils doivent vivre, dans les bidonvilles, qu’ils ont d’abord occupés
clandestinement, où ils ont ensuite bâti eux-mêmes des logements et
installé des équipements comme l’eau et l'électricité, mais d’où ils
peuvent également être chassés à tout moment. Venant des favelas, ils
font quotidiennement la navette pendant des heures dans des transports
en commun inexistants pour se rendre à leur travail, le plus souvent au
noir, dans le centre des villes. Telle est la fissure qui traverse le
Brésil.

La thèse de Maricato me parlait.

Serait-il possible que les habitants des 5 blocs à Bruxelles, une sorte
de bidonville en somme, connaissent un état de mélancolie ?

Et que pouvait bien être, dans ce cas, cette mélancolie ?

<span class="number">3</span>On confond souvent mélancolie (M) et nostalgie. J’ai même entendu
quelqu’un parler un jour de ‘nostalgie mélancolique'. Alors que ce sont
deux notions différentes. La nostalgie, c’est rêver à des épisodes
idylliques de notre jeunesse, notre famille, nos amis et nos aventures,
sachant bien que l’idylle est passée et qu’elle ne reviendra plus
jamais.
{: .section-start}

La mélancolie ne regarde pas derrière soi, c’est la tristesse d’ici et
maintenant, c’est une pause, un arrêt qui dure peu ou longtemps, jusqu'à
ce qu’il y ait à nouveau une suite. Vu de cette façon, la mélancolie
n’est pas un état mais une dynamique qui évolue et peut se renverser. Il
le faut d’ailleurs. Car la vie continue. Nous devons donc nous détacher
de notre tristesse.

<span class="number">4</span>Il faut, à ce point de notre récit, dire quelques mots de la chorale
et de son histoire récente. En janvier 2017, le Brussels
Brecht-Eislerkoor donnait au Kaaitheater les dernières représentations
de Waanvlucht, Fuirlafolie. Nous clôturions ainsi notre contribution à
la commémoration de la Première Guerre Mondiale, la Grande Guerre.
Waanvlucht (et son premier volet Say No) parlait du fait de prendre les
armes ou de refuser de le faire, de la désertion, de la paix. Le projet
nous avait pris trois ans.
{: .section-start}

Alors même que Waanvlucht prenait fin, nous nous sommes lancés dans un
nouveau projet. Nous avions décidé que ce projet traiterait des
*commons*, c’est-à-dire de la façon dont des groupes d’hommes et de
femmes collaborent afin de prendre de nouvelles initiatives et de
réaliser de nouveaux équipements dans l’intérêt de tous.

En cherchant une manière de préciser ce thème général, nous sommes
tombés sur les 5 blocs, les tours de logements situées au Rempart des
Moines à Bruxelles. Nous avions là un patrimoine public, c’est-à-dire
des *commons*, avec 320 logements sociaux habités par plus de 800
personnes, mais le secteur public ne s’en occupait guère : depuis vingt
ans, les blocs étaient abandonnés à leur sort et il ne restait plus
qu’une solution : les raser !

Nos questions étaient : n’y avait-il pas moyen de faire autre chose ?
N’y a-t-il aucune façon de faire en sorte que les gens puissent habiter
convenablement en ville ? Et pouvions-nous, chœur brechtien, faire
entendre notre voix dans ce débat ?

Pour comprendre ce qui se passait, nous avons pris contact avec les
habitants et avec les associations qui travaillaient au Rempart des
Moines. Début 2017, notre plan prévoyait de les rencontrer régulièrement
deux années durant afin de relever leurs sentiments et leurs aspirations
et de les traduire en chansons que nous interpréterions (en 2019) dans
un spectacle musical. Le calendrier était trop optimiste. Nous nous
sommes rendu compte que ce dossier était d’une grande complexité et les
réactions des habitants du Rempart des Moines très variées. Il nous
faudrait davantage de temps pour bien sentir ce qui se passait aux 5
blocs. Dès le départ, nous avons compris que les gens étaient inquiets
pour leur avenir. Ils se posaient beaucoup de questions pratiques. Le
moindre incident était gonflé, comme si l’expulsion était pour demain.

<span class="number">5</span>En mars (ou était-ce avril) 2017 se produisit l’un de ces incidents
typiques. Des panneaux s'étaient détachés de la façade d’un des 5 blocs
et étaient tombés sur le trottoir. C'était dangereux. Le Logement
bruxellois, une société immobilière de service public, réagit aussitôt.
Quelques jours plus tard, elle fit placer des grillages autour de
chacune des cinq tours, pour la sécurité des habitants. Mais au lieu de
rassurer les gens, ces grillages furent une source supplémentaire de
nervosité. La rumeur disait que « les travaux vont commencer ». Ce
n'était pas le cas, mais ça prouvait bien que la société de logements
(le propriétaire) avait un devoir d’explication. Depuis qu’on avait
décidé de raser les tours, la société de logements n’avait rencontré
physiquement les habitants qu’une seule fois pour ‘communiquer’ sur le
plan des travaux. On leur avait montré un schéma avec un calendrier. Le
message était que la démolition ne commencerait que dans les années
2020. Mais ce n’est pas parce qu’on projette une fois des diapositives
avec un calendrier que tout le monde a compris ce calendrier \[on notera
qu’en 2018, tous les panneaux des façades ont été refixés solidement,
les uns après les autres\].
{: .section-start}

<span class="number">6</span>Nos premières rencontres au Rempart des Moines ont eu lieu à
l’intérieur, avec les habitants qui fréquentaient le *Projet de Cohésion
Sociale* (PCS) ou le Forum qui rassemblait surtout des groupes de femmes
et servait de la soupe. Les premiers récits se sont fait jour. Les
ascenseurs, continuellement en panne, étaient une source permanente
d’agacement, en tout cas pour les plus âgés qui devaient parfois
franchir 6-7 étages à pied par l’escalier avec leurs commissions, même
s’il y avait toujours des plus jeunes pour les y aider. Au PCS, il y eut
une fois un père qui attendait de pouvoir regagner son appartement. Il
montra des photos sur son GSM où l’on voyait ses enfants mordus par des
punaises, leur cou et leur dos remplis de taches rouges. Mais il ne
s'énervait pas, il n’en faisait pas toute une affaire. Ils ont
désinfecté son appartement et il attendait, comme si ça faisait partie
de la routine.
{: .section-start}

Un jour, M. nous dit : ‘ici, les gens sont défaitistes'. Mais parlait-il
au nom de tous ? Ou exprimait-il surtout son propre désenchantement ?
Car quand il prenait une initiative, les réactions étaient bien maigres.
Ainsi : Mochélan, un performer de Charleroi, avait animé un atelier avec
des jeunes pour écrire des textes. Mais ça n’avait pas donné
grand-chose. Le PCS n’avait même pas obtenu de Mochélan le court-métrage
réalisé pendant l’atelier. Donc, quand M. disait que les 5 blocs
baissaient les bras, était-ce bien exact ?

<span class="number">7</span>Nous voulions avoir des réponses concrètes, comme : comment vivent
les gens dans les logements sociaux du Logement bruxellois ? Comment se
comporte le propriétaire ? Peut-on encore trouver à Bruxelles des
logements à un prix abordable ? Un peu d’histoire s’est ajoutée, sur la
façon dont on a installé les blocs dans le quartier dans les années 1960
et sur ce qu'était le fameux quartier du Coin du Diable.
{: .section-start}

Mais il se passait aussi des choses moins visibles. Car en fait, on
chassait les habitants. En fait, ils devaient faire de la place pour un
autre type d’habitants de la ville, la classe moyenne qui a davantage de
moyens. Et pourtant, nous n’avons pas rencontré de révolte, pas de
protestation collective. Les gens réagissaient de manière isolée, chacun
dans leur coin. Face à nous, qui venions de l’extérieur, ils restaient
prudents.

*Qui sème la misère, récolte la colère* dit un slogan des manifestations
de rue. Mais ce slogan ne trouvait pas à s’appliquer ici, ce n'était pas
aussi simple. Pourquoi ? Telle était la question. Et aussi : peut-on
comprendre ce qui se passe dans la tête des habitants ? Nous devions
voir au-delà des aspects matériels et toucher du doigt les mentalités,
l'état d’esprit des habitants.

C’est alors que j’ai entendu Erminia Maricato parler de la mélancolie
dans les bidonvilles du Brésil – et que la ‘mélancolie’ est devenue un
thème nouveau dans le travail de la chorale sur les 5 blocs.

<span class="number">8</span>Par ‘mélancolie’ on entend encore toujours une tristesse un peu
désespérée, celle généralement d’un individu. C’est dû à l’origine du
mot. Le grec *melan khole* signifie bile noire. La bile est une
sécrétion produite par le foie (‘une sécrétion qui, lorsqu’elle a
produit ses effets, est évacuée avec les selles’). Mais aussi longtemps
qu’on ne l’a pas su, les chirurgiens croyaient que la bile noire
déterminait l’humeur des gens. On était bilieux, grognon et imbuvable
aux yeux des proches.
{: .section-start}

Un autre mot pour signifier cet état était *acedia*, ce que nous
traduirions aujourd’hui par ‘aigreur'. Pour certains penseurs chrétiens
comme Thomas d’Aquin, *acedia* n'était rien de moins qu’un des sept
péchés capitaux, surtout parce que l’homme aigri oublie de pratiquer la
vertu chrétienne de la générosité. Cette générosité, il fallait surtout
en témoigner à l'égard du dieu chrétien. Albrecht Dürer en a fait une
gravure, *Melencolia* (1514). L’image est pleine des codes de l'époque :
on y voit un ange désabusé entouré d’objets inutiles, un quadrant
magique, un chérubin sur une meule, un sablier où le temps s'écoule.

C’est ainsi que la mélancolie s’est trouvé pourvue d’une connotation
négative qui ne l’a plus quittée. On lui donne donc aussi ce sens
morbide. L’homme (ou la femme) mélancolique souffre de dépression, est
incapable d’agir, paralysé. La mélancolie (M) se réduit à n'être qu’une
affection psychopathologique.

<span class="number">9</span>Mais ne sommes-nous véritablement rien d’autre que des personnalités
aigries, minables, figées, éteintes ? Oui, il se peut qu’on ait été
injuste envers nous, qu’on nous ait pris ce qui nous était cher, qu’on
nous ait empêché d’avoir ce que nous espérions ; il se peut que nous
ayons échoué, que nous soyons abattus. Ça nous fait mal, nous nous
taisons et nous baissons les bras, aujourd’hui en tout cas mais pas pour
toujours. Car au bout d’un moment, on se reprend en mains, on prend
courage, pour continuer à vivre, pour se relever, pour résister. On ne
peut faire autrement.
{: .section-start}

L’auteur allemand W.G. Sebald a été un pas plus loin et a lancé une idée
totalement opposée: la mélancolie n’est pas une maladie, c’est le
médicament. Sebald en est arrivé à cette conclusion en se penchant sur
les horreurs de la Deuxième Guerre mondiale, plus précisément les
bombardements des villes allemandes.

Les ‘bons’ effectuaient alors des *Strategic Bombings* sur les ‘mauvais'
en Allemagne à partir des aéroports anglais. Avec deux objectifs :
détruire l’industrie de guerre nazie et démoraliser la population. Aucun
de ces deux objectifs ne put aboutir. Mais le résultat fut effrayant. La
seule Royal Air Force (RAF) a entrepris 400.000 vols en direction de 131
villes allemandes, dont certaines ont été bombardées une seule fois,
d’autres à plusieurs reprises. La RAF a largué au total 1 million de
tonnes de bombes, détruit 3,5 millions de logements, fait 7,5 millions
de sans-abri et tué 600.000 civils. W.G. Sebald (°1944) a rassemblé ces
chiffres pour des conférences données en 1997 à Zurich.[^2]

Écrasés par les tapis de bombes tombant sur les villes allemandes, les
habitants avaient toutes les raisons d'être abattus. Mais ils n’ont pas
craqué. Après chaque bombardement, ils se remettaient à l’ouvrage, dans
les ruines, parmi les cadavres et dans la puanteur, avec un désir irréel
de continuer. Était-ce fierté obstinée ? Étaient-ils comme anesthésiés
et complètement perdus ? Sebald ne le comprend pas toujours non plus.
Mais c’est de cette confusion qu’il a ensuite tiré ses conclusions. La
mélancolie, c’est re-penser le désastre, et le faire, c’est déjà
résister.

Dans *Die Beschreibung des Unglücks*, il écrit :

> Melancholy, the rethinking of the disaster we are in,  
> shares nothing with the desire for death,  
> it is a form of resistance.  
> When, with a fixed gaze, melancholy again reconsiders  
> just how things could have gone this far, it becomes clear that the dynamic of inconsolability  
> and of knowledge are identical in function.  
> In the description of the disaster lies the possibility of overcoming it.

> La mélancolie, re-penser le désastre qui nous frappe,  
> n’a rien à voir avec un désir de mort,  
> c’est une forme de résistance.  
> Quand la mélancolie, le regard fixe,  
> se demande à nouveau comment on a pu en arriver là,  
> on se rend compte alors que la dynamique de l’inconsolation  
> et de la connaissance ont une fonction identique.  
> C’est la description du désastre qui permet de le surmonter.  
> Extrait de l’exposition *Melancholia. A Sebald Variation [^3]

<span class="number">10</span>Aux 5 blocs, le défaitisme (si tant est qu’il y en ait eu) n’a pas
duré longtemps. C’est que les gens savaient bien de quoi il retournait.
La société de logements sociaux ne faisait pas son travail. A-t-elle
assez de logements pour reloger tous ceux qui doivent quitter les 5
blocs ? D’autres blocs de logements sont rénovés, donc pourquoi pas le
Rempart des Moines ? Des dizaines de milliers de Bruxellois sont sur des
listes d’attente, certains depuis 15 ans déjà ! Et voilà que
disparaissent au Rempart des Moines 110 logements sociaux de plus.
Autant de questions, autant de raisons d'être mécontent.
{: .section-start}

Ce qui manquait, aux 5 blocs, c'était quelque chose ou quelqu’un capable
de rassembler les habitants afin qu’ils puissent porter leurs plaintes
jusqu’au propriétaire. Ni le conseil des locataires ni les associations
ne le faisaient. Mais quand des activistes ont pris ce rôle sur eux,
notamment à propos du coût du chauffage (parfois supérieur au loyer),
les habitants se sont mobilisés. Faire pression sur les décideurs, ça a
de l’effet, et les habitants s’en sont rendu compte eux aussi.

<span class="number">11</span>Et donc, pour le dire encore une fois, la mélancolie n’est pas une
condition, c’est une fissure. Entre perte et désir, entre échec et
continuation, entre hésiter et parler.
{: .section-start}

> Que pena siente el alma  
> Cuando la suerte impia  
> Se opone a los recuerdos  
> Que anhela el corazón  
> Ah ! La douleur que ressent l'âme  
> Quand le destin, implacable,  
> Se dresse contre les souvenirs  
> Dont le coeur est plein  
> Violetta Parra

<span class="number">12</span>Les gens reprennent, se reprennent, encore et toujours. C’est
l’histoire de Texaco, un bidonville à la Martinique. Cette île des
Antilles est toujours une colonie française, même si les Français
préfèrent appeler la Martinique un ‘département d’outre-mer'. Le mot
'colonie’ charrie trop de sang et d’humiliations. Il rappelle
l’esclavage et les traitements inhumains infligés pendant des siècles au
petit peuple par les classes supérieures, blanches.
{: .section-start}

Ceux qui ne pouvaient supporter l’esclavagisme cherchaient à gagner la
ville et y construisirent leurs propres logements, des huttes en fait,
avec des matériaux trouvés dans la ville. C’est ainsi que s’est
développé Texaco, un bidonville installé sur un terrain en friche à côté
d’entrepôts de la firme américaine qui porte ce nom. La police vient et
rase les huttes. Mais les habitants reconstruisent Texaco. Que faire
d’autre ?

Ceux de Texaco ont leurs côtés mesquins, leurs commérages. Mais ils sont
solidaires. C’est ainsi que débute ‘Texaco', le livre écrit en 1992 par
l’auteur martiniquais Patrick Chamoiseau.[^4] Un beau jour, un homme
vient mesurer le terrain où les urbanistes ont prévu d’aménager une
route. L’arpenteur est accueilli avec hostilité, on lui jette une pierre
à la tête, tout le monde trouve que c’est aller trop loin mais tous
estiment qu’il l’a bien mérité, nous avons construit Texaco de nos
mains, Texaco est à nous, *Touche pas à Texaco*.

Procurez-vous ce livre, lisez-le, il est magnifique. Parce qu’on y
trouve des phrases extraordinaires. Comme celle-ci, sur *la dame Etoilus
qui de l’alphabet ignorait même les blancs entre les vingt-six lettres*.

Le grand mérite de ce livre est de montrer comment la ville devrait être
et quel regard on peut poser sur les gens de la cité qui doivent partir
parce qu’ils sont, littéralement, dans le chemin. Ce livre traite aussi,
en d’autres termes, des 5 blocs à Bruxelles et de plein d’autres cités
dans autant d’autres métropoles sur la planète. Patrick Chamoiseau a
beaucoup appris des gens qui ont vécu à Texaco (le quartier existe
vraiment) et des archives de Fort-de-France, la capitale de la
Martinique.

#### Deux fragments.

Le premier fragment pourrait se rapporter mot pour mot aux 5 blocs, qui
sont stigmatisés depuis des années comme étant une erreur, une
aberration :

> En écoutant la Dame, j’eus soudain le sentiment  
> qu’il n’y avait dans cet enchevêtrement  
> cette poétique de cases vouée au désir de vivre,  
> aucun contresens majeur  
> qui ferait de ce lieu, Texaco, une aberration.  
> Au-delà des bouleversements insolites des cloisons,  
> du béton, du fibrociment et des tôles,  
> au-delà des coulées d’eaux qui dévalaient les pentes,  
> des flaques stagnant, des écarts aux règles de salubrité urbaine,  
> il existait une cohérence à décoder,  
> qui permettait à ces gens-là de vivre aussi parfaitement,  
> et aussi harmonieusement qu’il était possible d’y vivre,  
> à ce niveau de conditions.  

Le second fragment comporte un étonnant conseil créole pour la ville
tout court :

> Au centre, une logique urbaine occidentale,  
> alignée, ordonnée, forte comme la langue française.  
> De l’autre, le foisonnement ouvert de la langue créole  
> dans la logique de Texaco.  
> Mêlant ces deux langues, rêvant de toutes les langues,  
> la ville créole parle en secret un langage neuf  
> et ne craint plus Babel.  
> (...)  
> La ville créole restitue à l’urbaniste qui voudrait l’oublier  
> les souches d’une identité neuve :  
> multilingue, multiraciale, multi-historique, ouverte  
> sensible à la diversité du monde.

Tout a changé.

Chamoiseau entrecoupe la trame de son roman de fragments de ce genre. Ce
sont des notes qu’il attribue à un urbaniste doté d’un solide bon sens,
qu’il aurait trouvées dans une bibliothèque de Fort-de-France. Elles ont
bien l’air de véritables notes, mais il est parfaitement possible que
Chamoiseau les ait inventées. Ce qui n’enlève rien à leur pertinence.

<span class="number">13</span>La lecture de Patrick Chamoiseau a changé notre regard sur les 5
blocs du Rempart des Moines. Et pas seulement celle de son roman
*Texaco*. Dans *Écrire en pays dominé*, il décrit le chemin qu’il a dû
accomplir pour trouver sa voix d'écrivain.[^5] Pour cela, il a dû
mettre sous la loupe ‘le désastre de la colonisation’ (pour employer les
termes de W.G. Sebald), et accomplir des actes de résistance successifs.
{: .section-start}

Mais il est extrêmement difficile de rompre avec les règles et la
rhétorique des décideurs. Ils parviennent à convaincre les gens qu’ils
ne signifient rien. Ces nègres, ces nègres-habitants, subissent depuis
des siècles un lavage de cerveau : ‘vous ne pouvez vous en sortir seuls,
vous êtes des incapables, des arriérés face au ‘développement''. Et les
gens le croient. Ils se mettent même à penser qu’ils doivent vivre comme
au Centre du monde : en France, en Europe. Le *brainwash* fonctionne
parfaitement, nous dit Chamoiseau. C’est une oppression non violente,
mais efficace. ‘Bien après la fin des attentats coloniaux, ces
autodépréciations perdurent et perdurent encore, actionnées mécaniques
par le seul dégoût ambigu de nous-mêmes que la domination néo-coloniale
alimente sans cesse’ (p. 61). Va-t-en essayer de t’y soustraire, écrit
Chamoiseau. Mais ‘toute déshumanisation s’accompagne d’un arc tendu vers
l’humanisation'. Quelqu’inhumain que soit le système dominant, on y
trouve toujours des germes de résistance.

<span class="number">14</span>En juin 2017, Chamoiseau fait paraître *Frères migrants*, où il
s’oppose à sa façon au racisme et à la xénophobie.[^6] L'élection de
M. Trump, écrit-il par exemple, a débuté au moment où le premier émigré
s’est noyé avec sa famille dans un océan d’indifférence. Pas plus que
les cités populaires, l’immigration n’est quelque chose d’anormal. Au
contraire, c’est une ‘décence ' et un Droit poétique : aller-venir et
dévirer de par les rives du monde sont un Droit poétique.
{: .section-start}

Poétique ne se confond pas avec la poésie. D’après Chamoiseau, la poésie
exprime la façon dont on regarde le monde, alors que la ‘poétique ‘est
une attitude : voilà comment je me tiens dans le monde, face aux
dikkeneks, aux faux-culs et aux faux-collants.

Poétique ? Lorsque le roi Baudouin prononçait son message de Noël à la
télévision dans les années soixante, tourner un bouton de la TV pour que
l’image soit entièrement déformée dans le sens vertical, LOL.

Poétique ? Une tarte dans la gueule de Bill Gates, LOL.

Poétique ? Agir consciemment, pas besoin de rajouter des mots.

Chamoiseau vient d’une île (une ‘petite île, à en croire les Français),
d’une région qui a une longue histoire d’esclavage et d’oppression
coloniale. Sa poétique est militante : anti-coloniale, antiraciste,
humaine. Et pourtant, il ne brandit jamais de drapeau. Face à la
barbarie, dit-il dans une interview à propos de *Frères migrants* paru
dans Libération[^7], il n’y a qu’un récit possible : fasciner les gens
en leur présentant des lucioles.

#### Chamoiseau :

> Il faut précipiter dans les esprits tout ce que l'économie néo-libérale ignore :  
> des essaims de lucioles, des arches de sensations, des boules d’imagesnouvelles  
> susceptibles de déclencher du nouveau et de laisser entrevoir,  
> sinon des leçons ou des recettes toutes faites,  
> mais des possibilités autres.

Il m’a semblé que ces lucioles trouvaient parfaitement leur place dans
le paysage des 5 blocs : des caïds et des crocodiles se bagarrent dans
le marais, on lave les cerveaux et on installe des grues, mais
en-dessous de tout ça brillent des lumières scintillantes.

<span class="number">15</span>Je croyais que Chamoiseau avait créé lui-même cette image des
lucioles. Mais non, il l’a trouvée chez d’autres. Dans les premières
pages de son livre *Frères migrants*, il donne pas moins de quatre
références. La première, et la plus importante, renvoie à Pier Paolo
Pasolini.
{: .section-start}

En février 1975, Pasolini écrit un article dans le journal italien
Corriere della Sera.[^8] Plus de six mois avant qu’il ne soit
assassiné. Dans cet article, Pasolini analyse la situation politique en
Italie. Elle est plus que critique. Un coup d’État néo-fasciste se
prépare, les Brigate Rosse commettent des attentats, ce sont les ‘années
de plomb'.

Mais Pasolini écrit, à l’entame de son article, ce qui suit : *‘il y a
dix ans environ quelque chose s’est passé ... Et permettez-moi de donner
une définition poétique, littéraire, de ce qui s’est passé, après tout
je suis un écrivain. Au début des années 1960, les lucioles se sont
mises à mourir dans notre pays, tuées par la pollution de l’air,
l’acidité des rivières, la saleté des canaux dans les campagnes. Ce fut
un choc. Après quelques années, plus une seule luciole n’avait
survécu.'*

Dans ce fragment, Pasolini lance un simple appel : devenons des
personnes chaleureuses comme nous l'étions auparavant, c’est-à-dire ici
avant les années ‘60, ces années où l’Italie s’est laissée emporter par
le tourbillon de la société de consommation. Il concluait son long
traité politique en disant qu'à tout prendre, il était disposé à
échanger une méga-multinationale comme Montedison contre une seule
luciole.

En Italien, *luciola* signifie luciole mais aussi ‘prostituée’ ou ‘la
dame qui te conduit à ta place au cinéma en te précédant avec sa lampe'.
Peut-être Pasolini voulait-il pointer la contradiction entre les
lumières inspirantes des lucioles et la lumière aveuglante de la
consommation forcée.

Pasolini était pessimiste. Il était réellement possédé par une
mélancolie de la perte.

Les lucioles, par contre, sont vivantes. Leur mélancolie est pétillante,
opère discrètement et en souterrain, sachant qu’elles sont nombreuses,
les lumières de la résistance.

[^1]:  *Cidades Rebeldes, Passe livre as manifestações que tomaram as
    ruas do Brasil*, Ed. Boitempo, São Paulo, juillet 2013, 112 p.

[^2]:  W.G. Sebald, *De la destruction comme élément de l’histoire
    naturelle*, Ed. Babel, 2004. Titre original : *Luftkrieg und
    Literatur*, 2001, 155 p.

[^3]:  *Extrait de l’exposition *Melancholia. A Sebald Variation*,
    King’s College Londres, 21 septembre-10 décembre 2017 - notre
    traduction, le texte original étant: *Melancholie, das Überdenken
    des sich vollziehenden Unglücks, hat aber mit Todessucht nichts
    gemein. Sie ist eine Form des Widerstands. Und auf dem Niveau der
    Kunst vollends ist ihre Funktion alles andere als blos reaktiv oder
    reaktionär. Wenn sie, starren Blicks, noch einmal nachrechnet, wie
    es nur so hat kommen können, dann zeigt es sich, das die Motorik der
    Trostlosigkeit und diejenige des Erkenntnis identische Exekutiven
    sind. Die Beschreibung des Unglücks schliet in sich die Möglichkeit
    su einer Überwindung ein.

[^4]:  Patrick Chamoiseau, *Texaco*, Ed. Gallimard, Paris, 1992, 435 p.

[^5]:  Patrick Chamoiseau, Écrire* en pays dominé*, Folio/Ed. Gallimard,
    1997

[^6]:  Patrick Chamoiseau, *Frères migrants*, Ed. Seuil, 2017, 139 p.

[^7]:  Patrick Chamoiseau, *Les flux migratoires sont comme un réveil du
    sang de la terre*, Libération, 4 juin 2017

[^8]:  Pier Paolo Pasolini, *Il vuoto del potere in Italila *(Le vide du
    pouvoir en Italie), Corriere della Sera, 1<sup>er</sup> février 1975

<figure markdown=true>

![]({attach}Sous-le-marais_les-lucioles.png)

Onder het moeres de lichtjes

Sous le marais les lucioles

</figure>
