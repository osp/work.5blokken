Title: Onder het moeras, de lichtjes
type: essay
category: 3
rows: page-7-1 / span 20
cols: left / center-left
fill: auto
count: 4
lang: nl

### Over melancholie
#### Raf Custers

*Melancholie aan de 5 blokken (waarom revolteren ze niet?)* is bepaald
geen roepnaam. Wie eigentijds wil zijn, geeft zijn product een naam van
hoogstens 2-3 lettergrepen (Up-site, TheOne, Belair...). Zijn wij
eigentijds? Ja. Maken wij een product? Nee. Daarom staat onze titel daar
op zijn plaats.

<span class="number">1</span>Die eerste keren aan de 5 blokken hing daar een onbestemd gevoel. De
winter sleepte aan, het was grijs, buiten waren er nooit veel mensen te
zien. M., die daar het *Projet de Cohésion Sociale* animeerde, zei: de
mensen zijn defaitistisch. We trokken dat oordeel niet in twijfel, we
waren geneigd M. te geloven omdat wij hier nieuw waren en hij er toch al
twee of drie jaar werkte.
{: .section-start}

Als er verslagenheid heerste, dan konden we dat ook begrijpen. We wisten
dat deze blokken over zoveel tijd gesloopt zouden worden. De mensen
konden tegen dat sloopplan niet op. We dachten: de mensen zijn in shock.
Ze zouden immers weg moeten uit hun wijk om plaats te maken voor een
'beschaafder’ en koopkrachtiger publiek.

Zo legden we met ons verstand de situatie uit die we aan de 5 blokken
meenden aan te treffen. Maar met zo’n zwart-wit-uitleg zie je andere
gebeurtenissen niet. De bewoners van de 5 blokken reageren immers wèl,
op allerlei manieren. Ze moeten wel. Toen we dat begonnen te snappen,
kwam de titel van ons stuk: *Melancholie aan de 5 blokken (waarom
revolteren ze niet?).*

<span class="number">2</span>Vanaf begin 2017 zakten we met zangers en zangeressen uit ons koor
af naar de 5 blokken. We begonnen er als amateurs ‘etnografisch’ werk te
doen: observeren hoe het leven verliep, sensaties voelen, geuren uit
keukens, geluiden uit ramen, uit en in de portieken, luisteren naar de
mensen, meeëten, meepalaveren, passeren wanneer er even tijd was, af en
toe de ervaringen op een rij en zien of er daardoor een draad te trekken
viel.
{: .section-start}

We werkten er nog maar een trimester toen zich het thema van de
'melancholie’ opdrong. Langs een ruime omweg feitelijk. Aan de
universiteit van Saõ Paulo, Brazilië was namelijk een spreekbeurt
gegeven met als titel ‘Melancholie in de stedelijke ongelijkheid'
(*Melancolia na desigualdade urbana)*. Het exposé ging over Brazilië's
*bidonvilles*. Toen rees de vraag of dit thema soms van toepassing is op
de cités en sociale woonwijken bij ons?

De spreekbeurt stond
[*online*](https://erminiamaricato.net/2017/05/16/melancolia-na-desigualdade-urbana/).
Ze was gegeven door één van de experts van de Braziliaanse
sloppenbuurten (de favela’s), de gewezen vice-minister Erminia Maricato.
Ik bewonderde het werk van Erminia Maricato. Ik had het leren kennen in
Rio de Janeiro, tijdens de massale straatprotesten van 2013, toen zij
meeschreef aan *Cidades Rebeldes*[^1], een boekje over de stad als het
toneel van maatschappelijke revolte.

De protestbeweging brak uit in mei-juni. Al gauw trokken er letterlijk
miljoenen door de straten van alle grote Braziliaanse steden. De mensen
waren razend dat de prijzen van bus- en metro-tickets waren opgeslagen,
terwijl er op datzelfde moment miljarden werden gespendeerd aan de
stadions voor twee mega-events waarvoor Brazilië gastland was, de
Wereldcup Voetbal van 2014 en de Olympische Spelen van 2016 In de zomer
van 2013 breidde de beweging zich uit. Ze keerde zich nu tegen het hele
Braziliaanse bestel dat door een aartsconservatieve en racistische witte
kaste wordt gedomineerd. Niemand in dat bestel had deze stedelijke
opstand verwacht, de kaste van beslissers stond perplex. Zij konden maar
één reactie bedenken: staalharde repressie.

In het heetst van de protesten brachten Maricato en andere schrijvers
*Cidades Rebeldes* uit. Het was geïnspireerd door het boek *Rebel
Cities* van David Harvey, van wie er ook een artikel in het boekje
staat. In haar artikel legde Maricato uit hoe gespleten het bestel in
Brazilië is. Ze haalde cijfers aan over de voortwoekerende plaag van het
geweld tegen het volk in de steden. De daders van de *homicides* behoren
dikwijls tot de politie maar ‘de slachtoffers van de moorden zijn
hoofdzakelijk jong, zwart, arm en woonachtig in de verste rand van de
metropolen'.

De mensen verkeren in een staat van collectieve melancholie, aldus
Maricato in haar lezing in 2016 over de favela’s en de verste randen van
de metropolen. Die gemoedstoestand komt van de omstandigheden waarin de
mensen moeten leven, in de sloppenwijken, die ze eerst klandestien
hebben bezet, waar ze dan zelf woningen hebben gebouwd en
nutsvoorzieningen zoals water en elektriciteit hebben aangelegd, maar
waar ze ook op elk moment verjaagd kunnen worden. Vanuit die favela’s
pendelen ze urenlang met onbestaand openbaar vervoer naar hun werk,
meestal zwartwerk, in de centra van de steden. Dat is Brazilië's
gespletenheid.

Maricato’s stelling sprak me aan.

Zou het kunnen dat de bewoners van de 5 blokken in Brussel, ook een
soort van sloppenwijk, in een staat van melancholie verkeerden?

En wat was dan die melancholie?

<span class="number">3</span>Melancholie (M) wordt dikwijls verward met nostalgie. Ik heb zelfs
ooit iemand over ‘melancholische nostalgie’ horen praten. Terwijl het
twee verschillende begrippen zijn.
{: .section-start}

Nostalgie mijmert over idyllische episoden van onze jeugd, onze familie,
onze vrienden en avonturen, wel wetend dat de idylle passé is en nooit
meer terugkomt.

Melancholie kijkt niet achterom, het is weemoed van hier en nu, het is
een pauze, een oponthoud dat kort duurt of lang, tot er weer een vervolg
komt. Als je het zo bekijkt, is melancholie geen toestand maar een
dynamiek die verschuift en kan omslaan. Dat moet ook. Want het leven
gaat voort. We moeten ons dus losmaken uit onze weemoed.

<span class="number">4</span>Hier hoort wat koorgeschiedenis. In januari 2017 speelde ons koor,
het Brussels Brecht-Eislerkoor in het Kaaitheater de laatste
voorstellingen van Waanvlucht. Daarmee sloten we onze bijdrage aan de
herdenking van de Eerste Wereldoorlog, de Grote Oorlog, af. Waanvlucht
(en het eerste luik *Say No*) ging over de wapens opnemen of dat
weigeren, over desertie, over vrede. Het project had drie jaar geduurd.
{: .section-start}

Terwijl Waanvlucht afliep, begonnen we een nieuw koorproject. We hadden
uitgemaakt dat het over de *commons* zou gaan, dat wil zeggen over hoe
groepen van mensen samenwerken om voor hun collectieve belang ook nieuwe
initiatieven te nemen en nieuwe voorzieningen te realiseren.

Toen we zochten hoe we dat algemene thema konden preciseren, kwamen we
uit bij de 5 blokken, de woontorens aan de Papenvest in Brussel. Hier
had je nu een openbaar patrimonium, dat wil zeggen gemeengoed
(*commons)*, met 320 sociale woningen, bewoond door ruim 800 mensen,
maar zie hoe laks de publieke sector ermee omging: de blokken waren al
twintig jaar verwaarloosd zodat er maar één uitkomst overbleef, slopen!

Onze vragen waren: kon dat echt niet anders? zijn er dan geen manieren
om mensen ordentelijk in de stad te laten wonen? en kunnen wij, als
Brechtiaans koor, in dat debat onze stem laten horen?

Om te begrijpen wat er gaande was, zochten we aansluiting bij de
bewoners en de verenigingen van de Papenvest. Begin 2017 voorzag ons
plan dat we hen twee jaar lang regelmatig zouden ontmoeten om hun
gevoelens en verzuchtingen op te tekenen en ze te vertalen in liederen
die we (in 2019) in een zangstuk zouden vertolken. De timing was te
optimistisch. We ondervonden hoe complex dit ene dossier was en hoe
divers de reacties van de mensen van de Papenvest. We hadden meer tijd
nodig om voeling te krijgen met de 5 blokken. Vanaf het begin hoorden we
dat de mensen ongerust waren over hun toekomst. Ze hadden heel veel
praktische vragen. Het minste incident werd opgeblazen, alsof de
uitdrijving voor morgen was.

<span class="number">5</span>In maart (of was het april, 2017) gebeurde één van die typische
incidenten. Aan één van de 5 blokken waren panelen losgekomen van de
gevel en ze ploften naar beneden op de stoep. Er konden ongelukken
gebeuren. De Brusselse Woning, een openbare woningmaatschappij,
reageerde snel. Enkele dagen later al liet ze heras-hekken plaatsen rond
alle vijf de woontorens, voor de veiligheid van de bewoners. Maar in
plaats van de mensen gerust te stellen, brachten die hekken nog meer
nervositeit. Het gerucht ging circuleren dat “de werken gaan beginnen”!
Dat was niet waar, maar het toonde dat de woningmaatschappij (de
huisbaas) nog véél uit te leggen had. Sinds de beslissing van de sloop
had de woningmaatschappij nog maar één keer oog in oog gestaan met de
bewoners om te ‘communiceren’ over het plan van de werken. Er was een
schema getoond met een tijdslijn. De boodschap was dat de sloop pas
ergens in de jaren 2020 zou beginnen. Maar het is niet omdat je één keer
dia’s projecteert met een kalender, dat iedereen de kalender heeft
begrepen. \[In 2018 nota bene werden alle gevelpanelen één voor één goed
vastgevezen\].
{: .section-start}

<span class="number">6</span>Onze eerste ontmoetingen aan de Papenvest gebeurden binnen, met de
bewoners die gingen buurten bij het *Projet de Cohésion Sociale* (PCS),
of bij Forum dat vooral vrouwen in groepen samenbracht en soep opdiende.
De eerste verhalen kwamen. De liften, continu defect, waren een
voortdurende bron van ergernis, zeker voor de ouderen die soms 6-7
verdiepingen te voet omhoog moesten met hun winkelzakken langs de trap,
al waren er altijd wel jongeren die hielpen.
{: .section-start}

Bij het PCS zat een keer een vader te wachten tot hij weer in zijn flat
kon. Op zijn gsm liet hij foto’s zien van hoe zijn kinderen gebeten
waren door *punaises* (bedwantsen), hun nek en rug zaten vol rode
vlekken. Maar hij wond zich niet op, hij maakte zich niet druk. Ze
desinfecteerden zijn flat, hij wachtte, alsof dit bij de routine hoorde.

M. zei op een dag: ‘hier heerst defaitisme’. Maar sprak hij voor
iedereen? Of drukte hij vooral zijn eigen ontgoocheling uit? Want als
hij een initiatief nam, dan bleef de respons beperkt. Bij voorbeeld:
Mochélan, een performer uit Charleroi, had een workshop geanimeerd met
de jongeren om teksten te schrijven. Veel vuur had dat niet gegeven. Het
PCS had van Mochélan niet eens de korte film gekregen die tijdens de
workshop was gemaakt. Dus, als M. zei dat de 5 blokken de kop lieten
hangen, klopte dat dan wel?

<span class="number">7</span>We wilden tastbare zaken weten, zoals: hoe leven de mensen in de
sociale woningen van de Brusselse Woning? hoe handelt de huisbaas? kan
je in Brussel nog betaalbare woningen vinden? Er kwam een brok
geschiedenis bij, over hoe de blokken in de jaren 1960 in de wijk waren
neergepoot en over wat voor een wijk de beruchte Duivelshoek was
geweest.
{: .section-start}

Maar hier speelden ook ontastbare zaken. Want feitelijk gesproken werden
de bewoners verjaagd. Feitelijk gesproken zouden ze moeten plaatsmaken
voor een ander soort stadsbewoners, voor de middenklasse die het beter
heeft. En toch vonden we geen revolte, toch troffen we geen
groepsprotest aan. De mensen waren in hun eentje, in zichzelf gekeerd.
Tegenover ons, die van buiten kwamen, bleven ze voorzichtig.

Er is zo’n straat-slogan *Qui sème la misère, récolte la colère*. Maar
deze slogan ging hier niet op, zo eenvoudig lag dat niet. Waarom niet?
was de vraag, en ook: valt het te vatten wat er in de hoofden van de
bewoners omgaat? We moesten verder kijken dan het materiële en ook de
mentaliteit aftasten, de gemoedstoestand van de bewoners.

Toen hoorde ik Erminia Maricato over melancholie in de sloppenwijken in
Brazilië en werd ‘melancholie’ een nieuw thema in het werk van het koor
over de 5 blokken.

<span class="number">8</span>Met melancholie wordt nog altijd hoofdzakelijk een naargeestige
treurigheid bedoeld, en meestal ook van een individu. Dat is aan de
oorsprong van het woord te wijten. Het Griekse *melan khole* betekent
*zwarte gal*. Gal is een slijm afgescheiden door de lever (‘een slijm
dat als het is uitgewerkt weer met de stoelgang wordt verwijderd’). Maar
toen dat nog niet geweten was, meenden chirurgijnen dat de zwarte gal
het humeur bepaalde. Je was dan zwartgallig, nors en ongenietbaar voor
je naaste omgeving.
{: .section-start}

Een ander woord voor zwartgalligheid was *acedia*, dat we vandaag als
'verzuring’ zouden vertalen. Voor christelijke denkers zoals Thomas van
Aquino was *acedia* zonder meer één van de zeven hoofd- of doodzonden,
vooral omdat de verzuurde mens vergat de christelijke deugd van
edelmoedigheid te beoefenen. Die edelmoedigheid moest bovenal aan het
christelijke opperwezen worden betoond.

Albrecht Dürer maakte er een gravure van, *Melencolia I* (1514). Dat
beeld zit boordevol met de codes van die tijd: je ziet een lusteloze
engel, omringd door nutteloze werktuigen, een magische kwadrant, een
cherubijn op een molensteen, een zandloper waarin de tijd voorbijgaat.

Melancholie kreeg zo een negatief cachet en heeft dat niet meer
kwijtgespeeld. Aan melancholie (M) wordt dus ook die ziekelijke
betekenis gegeven. De melancolic-us/-a lijdt dan aan neerslachtigheid,
aan *déprime* en raakt maar niet uit die staat van verlamming. Dit is
(M) gereduceerd tot psychopathologische aandoening.

<span class="number">9</span>Maar zijn we dan echt niet meer dan zure, minabele, versteende,
uitgebluste gevallen? Ja, het kan dat ons onrecht is aangedaan, dat ons
een dierbaarheid is afgenomen, dat iets waarop we hoopten ons is
onthouden; het kan dat wij faalden, mislukten en verslagen zijn. Dat
voelt wrang, we zwijgen dan en buigen, voor nu althans maar niet voor
altijd. Want op de duur herpakt (‘vermant’) een mens zich toch weer en
schept moed, om voort te jakkeren, om op te staan, om in verzet te gaan.
Een mens kan niet anders.
{: .section-start}

De Duitse schrijver W.G. Sebald ging een stap verder en vormde zich dit
averechts idee: melancholie is geen ziekte, het is het medicijn. Sebald
trok die conclusie door te spitten in de horror van de Tweede
Wereldoorlog, en meer bepaald de bombardementen van de Duitse steden.

De ‘goeden’ voerden toen vanuit vliegvelden in Engeland *Strategic
Bombings* uit op de ‘slechten’ in Duitsland. Met twee objectieven: de
nazi-oorlogsindustrie vernietigen en de bevolking demoraliseren. Ze
slaagden in geen van beide. Maar wat ze aanrichtten, was
huiveringwekkend. De Royal Air Force (RAF) alleen ondernam 400.000
vluchten naar 131 Duitse steden, waarvan sommige één keer werden
gebombardeerd, andere opnieuw en opnieuw en opnieuw. De RAF dropte in
totaal 1 miljoen ton bommen, vernielde 3,5 miljoen woningen, maakte 7,5
miljoen mensen dakloos en bracht 600.000 burgers om. W.G. Sebald (°1944)
zocht de cijfers bij elkaar voor lezingen die hij in 1997 in Zürich
gaf.[^2]

Toen de vuurzeeën door de Duitse steden raasden, hadden hun bewoners
alle redenen om verslagen te zijn. Maar zij kraakten niet. Na elk
bommentapijt gingen ze weer aan de slag, tussen het puin, tussen de
kadavers en de stank, met een onwezenlijke drang om voort te doen. Was
het hardnekkige trots? waren ze verdoofd en compleet van de wereld? Ook
voor Sebald blijft het onbegrijpelijk.

Maar in die verwarring ligt de stof waaruit hij nadien zijn opvatting
vormt.

Melancholie is de rampspoed her-denken, en wie dat doet, pleegt al
verzet.

In *Die Beschreibung des Unglucks* schrijft hij het zo:

> Melancholy, the rethinking of the disaster we are in,  
> shares nothing with the desire for death.  
> It is a form of resistance.  
> When, with a fixed gaze, melancholy again reconsiders  
> just how things could have gone this far,  
> it becomes clear that the dynamic of inconsolability  
> and of knowledge are identical in function.  
> In the description of the disaster lies the possibility of overcoming it.

> Melancholie, het herdenken van onze rampspoed,
> heeft niets met een doodsverlangen vandoen.  
> Het is een vorm van verzet.  
> Als de melancholie goed toekijkt en overdenkt  
> hoe het zover is kunnen komen,  
> dan blijken ontroostbaarheid en inzicht dezelfde functie te hebben.  
> Als je de rampspoed beschrijft, kan je haar te boven komen.

Uit de expositie *Melancholia. A Sebald Variation*[^3]

<span class="number">10</span>Aan de 5 blokken heeft het defaitisme (als dat er al was) niet lang
geduurd. De mensen wisten namelijk hoe de vork aan de steel zat. De
sociale woonmaatschappij deed haar werk niet. Heeft zij genoeg woningen
om iedereen te her-huisvesten die weg moet aan de 5 blokken? Andere
woonblokken worden gerenoveerd, dus waarom de Papenvest niet? In Brussel
staan tienduizenden mensen op wachtlijsten, sommigen al sinds 15 jaar!
en nu gaan aan de Papenvest alweer 110 sociale woningen verdwijnen.
Vragen en redenen genoeg voor ontevredenheid.
{: .section-start}

Aan de 5 blokken ontbrak gewoon iets of iemand die de bewoners
samenbracht zodat zij hun klachten naar de huisbaas konden dragen. De
huurdersraad en de verenigingen deden dat niet. Maar toen activisten wèl
die rol opnamen, onder andere rond de verwarmingskosten (soms hoger dan
de huur) kwamen de bewoners in beweging. En druk zetten op de beslissers
heeft effect, dat merkten de bewoners ook.

<span class="number">11</span>Opnieuw, melancholie is dus geen toestand, het is een tweestrijd.
Tussen verlies en verlangen, tussen tegenslag en toch weer voortdoen,
tussen aarzelen en spreken.
{: .section-start}

> Que pena siente el alma\  
> Cuando la suerte impia\  
> Se opone a los recuerdos\  
> Que anhela el corazón    

> Wat een pijn voelt de ziel  
> als het lot, hard als het is,  
> die verlangens beknot  
> waar het hart zo vol van is

Violeta Parra

<span class="number">12</span>Mensen beginnen altijd opnieuw. Dat is het verhaal van Texaco, een
sloppenwijk op Martinique. Dit Antiliaanse eiland is nog altijd een
Franse kolonie, al horen de Fransen dat niet graag, zij noemen
Martinique een ‘overzees departement'. Er hangt teveel bloed en
vernedering aan het woord ‘kolonie'. Het roept herinneringen op aan de
slavernij en de onmenselijke behandeling die de gewone mensen eeuwenlang
vanwege de witte bovenklasse moesten ondergaan.
{: .section-start}

Zij die het slavenwerk niet uithielden, trachtten de stad te bereiken en
bouwden daar hun eigen woningen, hutten eigenlijk, met de materialen die
ze in de stad konden vinden. Zo groeide Texaco, een *bidonville* op
braakland naast olie-opslagtanks van de Amerikaanse firma met dezelfde
naam. De politie komt en sloopt de hutten. Maar de mensen bouwen Texaco
weer op. Wat moeten ze anders?

De mensen van Texaco hebben hun kleine kanten en hun roddels. Maar ze
hangen wel aan elkaar.

Zo begint ook ‘Texaco', het boek uit 1992 van de Martiniquaanse
schrijver Patrick Chamoiseau.[^4] Op een dag komt een man het land
opmeten waar de stadsplanners een steenweg hebben gepland. De landmeter
wordt vijandig onthaald, iemand mikt een steen tegen zijn hoofd, dat
vindt iedereen te ver gaan al vindt ook iedereen het zijn verdiende
loon, wij hebben Texaco namelijk met onze blote handen opgebouwd, Texaco
is van ons, *Touche pas à Texaco*.

Zie dat je ‘Texaco’ te pakken krijgt, lees dit boek, ik vind het
prachtig. Omdat er wonderlijke zinnen in staan. Zoals deze over *la dame
Etoilus qui de l’alphabet ignorait même les blancs entre les vingt-six
lettres,* die dus zelfs van de spaties tussen de letters van het alfabet
geen benul had.

De grote verdienste van dit boek: het toont hoe de stad moet zijn en hoe
je kunt kijken naar de mensen van een cité die weg moeten omdat ze in de
weg wonen. Dit boek gaat met andere woorden ook over de 5 blokken in
Brussel en zoveel andere cités in zoveel andere alle metropolen van de
aardbol. Patrick Chamoiseau haalde veel wijsheid bij de mensen die in
Texaco hebben geleefd (de wijk bestaat echt) en uit de archieven van
Fort-de-France, de hoofstad van Martinique.

#### Twee fragmenten.

Het eerste fragment zou letterlijk over de 5 blokken kunnen gaan, die al
decennia als een fout (*une aberration*) worden gestigmatizeerd:

> En écoutant la Dame, j’eus soudain le sentiment  
> qu’il n’y avait dans cet echevêtrement,  
> cette poétique de cases vouée au désir de vivre,  
> aucun contresens majeur  
> qui ferait de ce lieu, Texaco, un aberration.  
> Au-delà des bouleversements insolites des cloisons,  
> du béton, du fibrociment et des tôles,  
> au-delà des coulées d’eaux qui dévalaient les pentes,  
> des flaques stagnants, des écarts aux règles de salubrité urbaine,  
> il existait une cohérence à décoder,  
> qui permettait à ces gens-là de vivre aussi parfaitement,  
> et aussi harmonieusement qu’il était possible d’y vivre,  
> à ce niveau de conditions.  


> Terwijl ik zo naar de Dame luisterde, bedacht ik ineens  
> dat er in deze verstrengeling, in deze poëtiek van hutten  
> die getuigen van een wil om te leven,  
> echt niets ongerijmds te vinden was  
> dat zou maken dat Texaco een miskleun was.  
> Want keek je verder dan de raar geschikte schotten  
> en het beton, het eternit en de golfplaten,  
> en verder dan de greppels of de plassen met vies water,  
> en alles dat vloekte met de regels van de stedelijke netheid,  
> dan kon je toch een samenhang ontwaren  
> die toeliet dat de mensen daar zo volmaakt  
> en harmonieus konden leven als  
> in die omstandigheden maar mogelijk was.

Het tweede fragment heeft dit merkwaardige creoolse advies voor de stad
*tout court*:

> Au centre, une logique urbaine occidentale,  
> alignée, ordonnée, forte comme la langue française.  
> De l’autre, le foisonnement ouvert de la langue créole  
> dans la logique de Texaco.  
> Mêlant ces deux langues, rêvant de toutes les langues,  
> la ville créole parle en secret un langage neuf  
> et ne craint plus Babel.  
> (...)  
> La ville créole restitue à l’urbaniste qui voudrait l’oublier  
> les souches d’une identité neuve:  
> multilingue, multiraciale, multi-historique, ouverte,  
> sensible à la diversité du monde.  
> Tout à changé.  

 
> Je hebt aan de ene kant de Westerse stedelijke logica  
> waar alles recht is en strak, zoals de onwribare Franse taal.  
> Maar je hebt ook het creools dat ongegeneerd krioelt,  
> net zoals het er in Texaco aan toegaat.  
> Meng die twee, meng alle talen,  
> en je krijgt de creoolse stad  
> waar ze hun eigen slang praten  
> en ze niet bang zijn van de pluriformiteit.  
> Zo richt de creoolse stad zich tot de stadsplanner  
> en zegt hem, voor het geval hij het zou vergeten,  
> wat de essentie van de stad zou moeten zijn:  
> in de stad spreken ze alle talen,  
> vind je alle rassen, herinneren ze zich de geschiedenissen,  
> zijn ze open van geest en gevoelig  
> voor wat er anders is in de wereld.  
> Dat is compleet het tegendeel van de stad van nu.

Met zulke fragmenten onderbreekt Chamoiseau het verhaal van de roman.
Het zijn notities die hij toeschrijft aan een stadsplanner met gezond
verstand en die hij in een bibliotheek in Fort-de-France zouden hebben
gevonden. Het lijken echte notities te zijn, maar Chamoiseau kan ze ook
verzonnen hebben. Dat maakt ze niet minder pertinent.

<span class="number">13</span>Dankzij Patrick Chamoiseau is onze kijk op de 5 blokken van de
Papenvest veranderd. En het is niet bij zijn roman '*Texaco*' gebleven.
In *Ecrire en pays dominé* beschrijft hij welke weg hij heeft moeten
afleggen om als schrijver zijn eigen stem te vinden.[^5] Daarvoor moest
hij ‘de rampspoed van de kolonisatie’ onderzoeken (om het met W.G.
Sebald te zeggen) en heeft hij de ene verzetsdaad na de andere moeten
pleggen.
{: .section-start}

Maar breken met de regels en de retoriek van de beslissers is
aartsmoeilijk. Zij prenten de mensen in dat ze nog minder dan niets
betekenen. Die negers, *ces nègres-habitants*, worden al eeuwen
gehersenspoeld, dat ze het alleen niet kunnen, dat ze onbekwaam zijn,
achter gewoon op de "Ontwikkeling". De mensen geloven het. Ze gaan zelfs
denken dat ze moeten leven zoals in het Centrum van de wereld: in
Frankrijk, in Europa. De brainwash werkt perfect, aldus Chamoiseau. Het
is geweldloze maar efficiënte onderdrukking. ‘Het koloniale geweld is al
lang verleden tijd, maar die zelf-ontwaarding blijft maar duren, omdat
ze aangedreven wordt door de *dégoût* die wij voelen voor onszelf, en
die altijd opnieuw door de neo-koloniale dominantie wordt gevoed.'(p.61)
Onttrek je daar maar eens aan, schrijft Chamoiseau. Maar ‘met elke
ontmenselijking wordt een boog naar humanisering opgespannen'. Hoe
onmenselijk ook het dominante systeem, altijd zijn er kiemen van verzet.

<span class="number">14</span>In juni 2017 brengt Chamoiseau *Frères Migrants* uit waarin hij
zich op zijn manier afzet tegen racisme en vreedemlingenhaat.[^6] De
verkiezing van M. Trump, schrijft hij bij voorbeeld, is begonnen toen de
eerste uitwijkeling met zijn familie verdronk in een zee van
onverschilligheid. Net zo min als volkse cités abnormaal zouden zijn,
net zo min is migratie dat. Het is integendeel fatsoenlijk (*une
décence*) en een poëtisch Recht: *aller-venir et dévirer de par les
rives du monde sont un Droit poétique*.
{: .section-start}

*Poétique* is niet hetzelfde als poëzie. Volgens Chamoiseau drukt poëzie
uit hoe je naar de wereld kijkt, terwijl *poétique* een houding is: zo
sta je in de wereld, tegenover de *vinstermikken* (‘dikkenekken',
scheldnaam voor de Hasselaren), de *schelleschaaïters* en de
*faux-collants*.

*Poétique*? Toen koning Boudewijn in de 1960ies op de televisie kwam
voor de Kerstboodschap, draaiden wij op de TV aan een knopje zodat het
beeld vertikaal helemaal uitrok, LOL.

*Poétique*? Een taart in het bakkes van Bill Gates, alom LOL.

*Poétique*? Bewust doen, daar komen verder geen woorden aan te pas.

Chamoiseau komt van een eiland (een ‘klein eiland’ volgens Frankrijk),
uit een gebied met een lange geschiedenis van slavernij en koloniale
onderdrukking. Zijn *poétique* is militant anti-koloniaal, militant
anti-racistisch, militant menselijk, terwijl hij toch nooit met de
strijdvlag zwaait.

Tegenover de barbarij, zegt hij in een interview over *Frères Migrants*
met Libération[^7], hebben wij maar één verhaal: de mensen met zwermen
vuurvliegjes begeesteren.

#### Chamoiseau:

> Il faut précipiter dans les esprits tout ce que l’économie néolibérale ignore :  
> des essaims de lucioles, des arches de sensations, des boules d’images nouvelles  
> susceptibles de déclencher du nouveau et de laisser entrevoir,  
> sinon des leçons ou des recettes toutes faites,  
> mais des possibilités autres.

#### Vrij vertaald:

> We moeten de geesten inspireren met alles  
> wat de neo-liberale economie wegsteekt:  
> zwermen van vuurvliegjes, wonderlijke sensaties, wervelwinden van
> nieuwe beelden  
> die niet zozeer kant en klare recepten opdienen  
> maar wel laten voelen hoe het anders kan.

Ik vond dat die vuurvliegjes perfect pasten in een landschap van de 5
blokken: bovenaan het moeras als een slagveld van caïds en alligatoren
en *brainwash* en bouwkranen, maar daaronder heldere lichtjes.

<span class="number">15</span>Ik meende dat Chamoiseau het beeld van de *lucioles* (de
vuurvliegjes, de lichtjes) had bedacht. Maar nee, hij haalde het bij
anderen. Vooraan in zijn boekje *Frères Migrants* geeft hij liefst vier
referenties. Pier Paolo Pasolini is de eerste en belangrijkste.
{: .section-start}

In februari 1975 schrijft Pasolini een artikel in de Italiaanse krant
Corriere della Sera.[^8] Dat is ruim een half jaar vòòr hij wordt
vermoord. In dat artikel analyseert Pasolini de politieke situatie in
Italië. Die is meer dan kritiek. Er is een neo-fascistische staatsgreep
ophanden, de Brigate Rosse plegen aanslagen, in Italië zijn dit ‘de
Jaren van Lood'.

Maar Pasolini begint het artikel zo: *‘tien jaar geleden of zo gebeurde
er iets ... En sta me toe een poëtisch-literaire omschrijving te geven
van wat toen gebeurde, ik ben immers schrijver. Aan het begin van de
jaren 1960 begonnen de vuurvliegjes in ons land uit te sterven, vanwege
de luchtvervuiling en vanwege de zure rivieren en de smerige kanalen op
het platteland. Dat was een schok. Na enkele jaren waren er gewoon geen
vuurvliegjes meer'.*

Met dit fragment deed Pasolini een simpele oproep in: laten we gewoon
terug warme mensen zijn zoals we dat vroeger waren, voor de jaren ‘60
bedoelde hij, en dus voor Italië zich liet meezuigen in de maalstroom
van de consumptiemaatschappij. Hij besloot zijn lange politieke traktaat
met te zeggen dat hij gerust een mega-multinational als Montedison voor
één vuurvliegje zou willen ruilen.

Het Italiaanse *lucciola* betekent vuurvliegje maar ook ‘prostituee’ of
'de dame die je in de cinema met een lichtje naar je plaats begeleidt'.
Misschien wilde Pasolini de contradictie aanduiden tussen de
inspirerende lichtjes van de *lucciole* en het verblindende licht van de
consumptiedwang.

Pasolini was pessimistisch. Hij was echt in de ban van een melancholie
van het verlies.

De *lucioles* daarentegen leven. Zij tonen een sprankelende melancholie
die discreet en *souterrain* opereert, wetend dat ze met veel zijn, de
lichtjes van het verzet.

[^1]: *Cidades Rebeldes. Passe livre e as manifestações que tomaram as
    ruas do Brasil*, Ed. Boitempo, Saõ Paulo, juli 2013, 112p

[^2]: W.G. Sebald, *De la destruction comme élément de l’histoire
    naturelle*, Ed. Babel, 2004 - Oorspronkelijke titel: *Luftkrieg und
    Literatur*, 2001, 155p.

[^3]: *Melancholia. A Sebald Variation*, King’s College London 21
    september - 10 december 2017 - De originele tekst luidt:
    *Melancholie, das Überdenken des sich vollziehenden Unglücks, hat
    aber mit Todessucht nichts gemein. Sie ist eine Form des
    Widerstands. Und auf dem Niveau der Kunst vollends ist ihre Funktion
    alles andere als blos reaktiv oder reaktionär. Wenn sie, starren
    Blicks, noch einmal nachrechnet, wie es nur so hat kommen können,
    dann zeigt es sich, das die Motorik der Trostlosigkeit und diejenige
    des Erkenntnis identische Exekutiven sind. Die Beschreibung des
    Unglücks schliet in sich die Möglichkeit su einer Überwindung ein.*

[^4]: Patrick Chamoiseau, *Texaco*, Ed. Gallimard, Parijs, 1992, 435p.

[^5]: Patrick Chamoiseau, *Ecrire en pays dominé*, Folio/Ed. Gallimard,
    1997

[^6]: Patrick Chamoiseau, *Frères Migrants*, Ed. Seuil, 2017, 139p.

[^7]: Patrick Chamoiseau, *Les flux migratoires sont comme un réveil du
    sang de la terre*, Libération, 4 juni 2017

[^8]: Pier Paolo Pasolini, *Il vuoto del potere in Italia* (Machtsvacuüm
    in Italië), Corriere della sera, 1 februari 1975

<figure markdown=true>

![]({attach}Sous-le-marais_les-lucioles.png)

Onder het moeres de lichtjes

Sous le marais les lucioles

</figure>