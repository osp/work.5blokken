Title: Makers <br>Producteurs
cols: 8 / span 4
rows: page-12-7 / span 7
count: 1
fill: auto
type: colofon

### Brussels Brecht-Eislerkoor

#### solisten/solistes
Christine Leboutte, Farbod Fathinejadfard I

#### slam
Celine Gaza

#### tekst/texte
Sybille Cornet

#### componist/musique
Kaat Dewindt

#### regie/mise en scène
Vital Schraenen

#### dirigent/chef de choeur
Lieve Franssen

#### geluidsmontage/montage sonore
Alejandro Elias

#### onderzoek/dramaturgie
Raf Custers

#### in samenwerking met
Kaaitheater, De Markten | met steun van de stad Brussel, VGC/Bruss-It,
Vlaanderen

http://www.bbek.be
