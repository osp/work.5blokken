Title: Colofon
cols: 2 / span 6
rows: page-12-7 / span 13
count: 2
fill: auto
type: colofon

'Melancholie aan de 5 blokken (waarom revolteren ze niet?)' is een
initiatief van het Brussels Brecht-Eislerkoor (BBEK) I ‘Mélancolie aux
blocs (pourquoi ne se révoltent-ils pas?) est une initiative de la
chorale Brecht-Eisler de Bruxelles  

#### Subsidieverleners | ‘Bailleurs'
stad&nbsp;Brussel-Bruxelles&nbsp;ville, Vlaamse&nbsp;Gemeenschapscommissie / Bruss-it, Vlaamse&nbsp;Gemeenschap

#### Instellingen | Institutions:
Kaaitheater, De&nbsp;Markten, GlobeAroma,
Logement&nbsp;Bruxellois/Brusselse <br>Woning (LBW), archief&nbsp;stad&nbsp;Brussel-Bruxelles&nbsp;ville&nbsp;archives, Koninklijke&nbsp;Bibliotheek-Bibliothèque&nbsp;royale, Hageltoren-Tour à Plomb, bibliotheek&nbsp;Muntpunt

#### Opmaak | Mise en page:
Open Source Publishing (OSP) : Femke Jurissen (magic intern) Gijs de Heij, Ludi Loiseau. Layout drawn with Pelican, Paged.js, CSS Grid, Inkscape, Gimp, git, gitlab, etherpad, GNU/Linux. Fonts: Coconat, Coupeur Reverse, Jost*, Ranchers. 

#### Drukkerij | Impression:
Imprimerie Havaux 6060 Gilly

#### Verenigingen | Associations:
BrusselBehoortOnsToe-BruxellesNousAppartient (BBOT-BNA),
Brukselbinnenstebuiten, La&nbsp;Fonderie, asbl&nbsp;La&nbsp;Rue, Forum/LD3, asbl&nbsp;Formosa, asbl&nbsp;Entraide, asbl&nbsp;Habitat&nbsp;et&nbsp;Rénovation/PCS, asbl&nbsp;Bravvo/Brucity, asbl&nbsp;LesMamsdel’Espoir, Omroerkoor (Hasselt), asbl&nbsp;Gresea, Brusselse&nbsp;Raad&nbsp;Leefmilieu (BRAL), Inter-Environnement&nbsp;Bruxelles
(IEB), Brusselse&nbsp;Raad&nbsp;Recht&nbsp;op&nbsp;Wonen / Rassemblement&nbsp;Bruxellois&nbsp;Droit à
l’Habitat&nbsp;(BBROW/RBDH), uitgeverij&nbsp;EPO, Centre&nbsp;Vidéo&nbsp;de&nbsp;Bruxelles (CVB),
Jeugd&nbsp;en&nbsp;Stad (JES), IMAL&nbsp;a.o.

#### Bewoners, kunstenaars, schrijvers, navorsers | Habitants, artistes, écrivains, chercheurs:
Simon&nbsp;Allemeersch, William&nbsp;Deraedt, Marcel&nbsp;Rijdams, Roel&nbsp;Jacobs, Pierre&nbsp;Blondel, Katrien&nbsp;Reist, Wouter&nbsp;De&nbsp;Raeve,
Koen&nbsp;Berghmans, Maïka&nbsp;De&nbsp;Keyzer, Silvia&nbsp;Federici, Mario&nbsp;Bucci, Erik&nbsp;Rydberg, Bruno&nbsp;Bauraind, Joost&nbsp;Vandenbroele, Dorine&nbsp;Mareel (Campo),
Isabelle&nbsp;Marchal, Mohamed&nbsp;Benzaouia, Willem&nbsp;Defieuw, Steyn&nbsp;Van&nbsp;Assche,
Riet&nbsp;Dhondt, Marie&nbsp;Nagy, Stephan&nbsp;Huegaerts, Christian&nbsp;Fleurs, Ruben&nbsp;Ramboer, Matthieu&nbsp;Tihon, Werner&nbsp;Van&nbsp;Mieghem, Anne-Sophie&nbsp;Dupont,
Caroline&nbsp;Englebert, Els&nbsp;Rochette, Brecht ??? van&nbsp;GlobeAroma, Guy&nbsp;Gypens,
Lieven&nbsp;Soete, Saskia&nbsp;Vanderstichele, Mochelan, Kristien&nbsp;Van&nbsp;den&nbsp;Houte,
Vincen&nbsp;Beeckman, Danja&nbsp;Cauberghs, Sander&nbsp;Tas, Axel&nbsp;Claes, Greet&nbsp;Brauwers, Vincent&nbsp;Carton, Sylvia&nbsp;Gimeno, Rudi&nbsp;De&nbsp;Rechter, Alice&nbsp;Verlaine&nbsp;Corbion, Nunzio&nbsp;Terranova, An&nbsp;Descheemaeker, An&nbsp;Clicteur, Anna&nbsp;Rispoli,
Annemie&nbsp;Maes,  Bart&nbsp;Van&nbsp;de&nbsp;Ven, Bart&nbsp;Van&nbsp;Effelterre, Bie&nbsp;Vancraeynest,
Kristiaan&nbsp;Borret, Lydie&nbsp;Pirson, Gunter&nbsp;Bousset, Marc&nbsp;Boutsen, Bruno&nbsp;Aertssen, Nora&nbsp;De&nbsp;Kempeneer, Frédéric&nbsp;Calmeyn, Juup&nbsp;Coenen, Jan&nbsp;Vromman,
Hendrik&nbsp;De&nbsp;Smedt, Dirk&nbsp;Seghers, Erdem&nbsp;Resne, Florence&nbsp;Lepoudre, Liévin&nbsp;Chemin, Hilde&nbsp;Peeters, Ginette&nbsp;Bauwens, Hilde&nbsp;Van&nbsp;Geel, Sandrine (Hobo),
Grigore&nbsp;Ionita, Isolde&nbsp;Boutsen, Gerben&nbsp;Van&nbsp;den&nbsp;Abbeele, Har&nbsp;Tortike,
Ines&nbsp;Vandermeersch, Myriam&nbsp;Van&nbsp;Imschoot, Eric&nbsp;Corijn, Jessiva&nbsp;van&nbsp;de&nbsp;Ven, Joseph&nbsp;Gourdin, Katelijne&nbsp;Meeusen, Leïla&nbsp;Haloui, Katherine&nbsp;Retsin,
Kristien&nbsp;De&nbsp;Koster, Kristina&nbsp;Borg, Laurent&nbsp;Moulin, Léonor&nbsp;Michiels,
Louise&nbsp;Labib, Claire&nbsp;Verhaeren, Mustapha&nbsp;Bijou, Nathalie&nbsp;Chatzidimitriou, Dominique&nbsp;Meeus, Nathan&nbsp;Ooms, Peter&nbsp;Van&nbsp;Breusegem, Pier&nbsp;De&nbsp;Kock, Stéphane&nbsp;Roy, Sarah&nbsp;De&nbsp;Laet, Stan&nbsp;Spijkers, Thomas&nbsp;Devos, Tim&nbsp;Verheyden, Thierry&nbsp;Timmermans, Veerle&nbsp;Van&nbsp;Schoelant, Rudy&nbsp;Veirman, Yves&nbsp;Bernard&nbsp;a.o.

![]()
