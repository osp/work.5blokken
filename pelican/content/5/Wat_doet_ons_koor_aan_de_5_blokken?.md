Title: Wat doet ons koor aan&nbsp;de&nbsp;5&nbsp;blokken?
rows: page-10-1 / span 14
cols: left / span 8
type: text
count: 2
lang: nl

> In Erwaegung das da Haeuser stehn  
> Waehrend ihr uns ohne Bleibe lasst,  
> Haben wie beschlossen, jetzt dort einzuziehen,  
> Weil es uns in unsern Loechern nicht mehr passt  

> Overwegend dat daar huizen staan  
> maar dat u ons zonder woning laat,  
> hebben wij beslist daar nu in te trekken,  
> want in deze krochten houden we het niet meer uit  

> (Uit: Bertolt Brecht, *Resolution der Kommunarden*, 1934)

Wonen is een basisrecht en de stad is voor en van iedereen. Onrecht
en uitsluiting zijn grensoverschrijdend maar ook nijpend urgent vlak
voor onze ogen.
{: .section-start }

In het centrum van Brussel worden vijf woonblokken uit de jaren 1960
gesloopt.  De blokken werden er in het toenmalig verwaarloosd
niemandsland van de kanaalzone neergeplant.  Niemand wilde er wonen en
dus was het een uitgelezen plaats voor sociale woningen.  Het
architectenbureau Groupe Structures creeerde rond de blokken een
gedeelde groenruimte, een *commons*.  De tijden veranderen en plots
liggen de blokken strategisch want in de Dansaertwijk, een boomend
gebied met een groot gebrek aan leefbaar groen.  De politiek investeerde
niet in onderhoud en vernieuwing van de blokken (er wonen toch maar
‘sociale gevallen’) en komt handenwrijvend tot de vaststelling dat ze
rijp zijn voor de sloophamer.

Een koor moet niet oordelen of de blokken vandaag nog te redden zijn met
een grondige vernieuwbouw.  Nochtans zijn er genoeg Brusselse of
buitenlandse voorbeelden waaruit blijkt dat renovatie wel kan.  En vlak
over het kanaal in Molenbeek is beslist de Brunfauttoren (‘de zesde
toren’) wèl te restaureren, nà acties van buurtbewoners.

Misschien is afbraak *voor de 5 blokken* wel de juiste keuze omwille van
de deplorabele toestand waarin ze zich NU bevinden.

Maar een koor kan in een Brechtiaanse traditie wel vragen stellen:
{: .section-start }

-  waarom moest een levendige volksbuurt (‘de duivelshoek’) plaats ruimen
   voor grote blokken?
-  hoe komt het dat sociale woningbouw van amper 60 jaar oud al
   uitgeleefd en vervallen is?
-  zijn ze slecht gebouwd ?  slecht onderhouden?  bewust verwaarloosd?
-  welk beleid steekt daarachter?  welke economische visie op wonen en
   sociale rechten?
-  wie zullen de nieuwe bewoners zijn die in de nieuwe wooneenheden
   verwelkomd worden?
-  wat zal er gebeuren met hen die verdreven worden?
-  zullen de ‘commons’ nog gekoesterd worden, in deze steeds duurder
   wordende, hippe buurt?

Deze vragen stellen we in de productie ‘Melancholie aan de vijf
blokken'. 

Het koor neemt de rol aan van een bespiegelend Grieks koor dat vragen
opwerpt maar tegelijk wel resoluut kiest voor de stemlozen en
verdrukten.

En natuurlijk gaan die vragen niet enkel over de 5 blokken in de
Brusselse Dansaertwijk. 

We stellen vragen over woonbeleid en sociale rechtvaardigheid in een
neoliberale wereld.

Het Brussels Brecht-Eislerkoor, opgericht in 1978, is een straat- en
protestkoor. In hoeveel optochten en betogingen we al meeliepen, weten
we niet exact.  Er zijn zoveel goede redenen om onvrede tegen onrecht
kenbaar te maken: oorlog, klimaat, sociale onrust, kernwapens, milieu,
... We sluiten we ons bij allerhande sociale bewegingen aan.  Het koor
is ook aanwezig op animaties en protestacties. We hebben flashmobs
gedaan en op politieke meetings gezongen.
{: .section-start }

Maar naast directe politieke actie heeft het koor ook al heel wat
avondconcerten en artistieke creaties op zijn actief. 

We  bezongen de veerkracht van uitgeslotenen in een volksopera naar het
boek ‘Vergeten Straat’ van Louis Paul boon. 

Elvis Peeters schreef een monoloog voor een vluchteling en we zongen er
liederen op teksten van de Turkse dichter Nazim Hikmet bij. 

We riepen samen met de bewoners van de Golanhoogten en componist Orlando
Gough over het *Shouting Fence* heen en vervloekten de grenzen die
mensen scheiden en machtsverhoudingen versterken.

We zongen op muziek van Brusselaar Frederic Rzewski over *WaanVlucht* in
de Eerste Wereldoorlog en de wijsheid van oorlogsweigeraars die *Say No*
zeggen.

Een hoogtepunt was het Eislerconcert samen met vier bevriende Berlijnse
koren in de Kammermusiksaal van de Philharmonie van Berlijn op 22 april
2018.

Het is maar een kleine greep uit de muziekproducties in het veertigjarig
bestaan van het koor.
