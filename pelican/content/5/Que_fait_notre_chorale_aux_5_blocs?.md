Title: Que fait notre chorale aux&nbsp;5&nbsp;blocs?
rows: page-10-1 / span 15
cols: span 8 / right
type: text
count: 2
fill: balance

> In Erwaegung das da Haeuser stehn  
> Waehrend ihr uns ohne Bleibe lasst,  
> Haben wie beschlossen, jetzt dort einzuziehen,  
> Weil es uns in unsern Loechern nicht mehr passt  

> Considérant que, là, s'élèvent des maisons  
> Tandis que vous nous laissez sans demeure,  
> Nous avons décidé de nous y installer maintenant,  
> Parce que dans nos trous, cela ne nous convient plus.  

> (Extrait de: Bertolt Brecht, *Résolution des Communards*, 1934)

Le logement est un droit fondamental et la ville nous appartient à
tous. L’injustice et l’exclusion ne connaissent pas de frontières, elles
sont aussi durement pressantes juste sous nos yeux.
{: .section-start }

Dans le centre de Bruxelles, cinq blocs de logement des années 1960 sont
en cours de démolition.  Les blocs ont été implantés dans la zone du
canal, un no man’s land délaissé à l'époque.  Personne ne voulait y
habiter, c'était donc un endroit idéal pour des logements sociaux.  Le
bureau d’architectes Groupe Structures a créé autour des blocs un espace
vert partagé, un *commun*.  Les temps changent et, tout d’un coup, les
blocs occupent un emplacement stratégique dans le quartier Dansaert, une
zone en plein boum qui manque grandement d’espaces verts viables.  Le
monde politique n’a pas investi dans l’entretien et la rénovation des
blocs (ils ne sont quand même habités que par des ‘cas sociaux’) et se
frotte les mains en constatant qu’ils sont mûrs pour la démolition.

Ce n’est pas à une chorale de juger si les blocs peuvent encore être
sauvés aujourd’hui moyennant une rénovation en profondeur.  Il y a
pourtant assez d’exemples, à Bruxelles ou à l'étranger, qui montrent
qu’une rénovation est possible.  D’ailleurs, juste de l’autre côté du
canal, à Molenbeek, on a bien décidé de rénover la tour Brunfaut (‘la
sixième tour’) à la suite d’actions du voisinage.

Il se peut que la démolition soit le juste choix *pour les 5 blocs*, vu
l'état déplorable dans lequel ils se trouvent MAINTENANT.

Mais une chorale est au moins en droit de poser des questions dans la
tradition brechtienne :
{: .section-start }

-  pourquoi un quartier populaire vivant (‘le coin du diable’) devait-il
   faire place à de grands blocs ?
-  comment se fait-il qu’un immeuble de logements sociaux d'à peine 60
   ans soit déjà dégradé et délabré ?
-  a-t-il été mal construit ?  mal entretenu ?  négligé
   intentionnellement ?
-  quelle est la politique qui se cache là-dessous ?  quelle vision
   économique du logement et des droits sociaux ?
-  quels nouveaux habitants les futures unités d’habitation
   accueilleront-elles ?
-  que se passera-t-il pour ceux qui en seront expulsés ?
-  bichonnera-t-on encore les ‘communs’, dans ce quartier branché
   toujours plus cher ?

Ces questions, nous les posons dans la production ‘Mélancolie aux cinq
blocs'.

La chorale endosse le rôle de chœur grec qui fait écho aux questions
tout en prenant le parti des sans-voix et des opprimés.

Et ces questions ne concernent évidemment pas les seuls 5 blocs
bruxellois du quartier Dansaert.

Nous posons des questions sur la politique du logement et la justice
sociale dans un monde néolibéral.

La chorale Brussels Brecht-Eislerkoor, créée en 1978, porte la
protestation dans la rue. Impossible de dire exactement combien de
marches et de manifestations nous avons accompagnées.  Il y a tellement
de bonnes raisons d’exprimer notre mécontentement à l'égard des
injustices : guerre, climat, troubles sociaux, armes nucléaires,
environnement, ...  Nous nous joignons à toutes sortes de mouvements
sociaux.  La chorale participe aussi à des animations et des actions de
protestation. Nous avons réalisé des flash mobs et chanté à des meetings
politiques. 

Mais à côté de l’action directe politique, la chorale a déjà à son actif
un grand nombre de concerts et de créations artistiques. 

Nous avons chanté la résilience des exclus dans un opéra populaire
d’après l’ouvrage ‘Vergeten Straat’ (La Rue oubliée) de Louis Paul
Boon. 

Nous avons chanté des textes du poète turc Nazim Hikmet sur le monologue
pour un réfugié rédigé par Elvis Peeters. 

Avec les habitants du plateau du Golan et le compositeur Orlando Gough,
nous avons crié par-dessus la *Shouting Fence* et maudit les frontières
qui séparent les gens et consolident les rapports de force.

Sur une musique du bruxellois Frederic Rzewski, nous avons chanté
*WaanVlucht - FuirLaFolie* de la Première Guerre Mondiale et la sagesse
des objecteurs de conscience qui disent *Say No*.

Un de nos temps forts a été le concert Eisler que nous avons donné avec
quatre chorales amies de Berlin dans l’auditorium de musique de chambre
de la Philharmonie de Berlin le 22 avril 2018.

Et ce n’est là qu’une petite sélection des productions musicales
réalisées durant les quarante ans d’existence de la chorale.
