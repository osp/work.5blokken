title: Fragmenten · Extraits
lang: fr
cols: span 4 / center-left
rows: page-11-1 / span 20
count: 1
type: fragments1


<h5>1. Rumeurs </h5>

On tape (SOUNDSCAPE + VOICES)
Sur fond sonore de ville, bribes de musiques d’un peu partout dans le monde,
avec des rumeurs en différentes langues, des bruits de klaxons, des cris d’enfants…  

<h5>2. Prologue (chant nr 1) </h5>

La décision est tombée  
Nos habitats sociaux  
Ceux qu’on appelle les 5 blocs   
Les cinq barres de notre quartier  
nos tours sociales  
vont être démolies  

On reconstruira, et oui, oui mais pour qui?  
Moins de logements sociaux, oui mais pourquoi?  
Plus de logements moyens, oui mais pourquoi?  
Et puis surtout: un parking!  
 (...)   
Place à l’argent, place aux bourgeois  
Aux galeries d’art  
Aux cafés branchés  
Aux lofts de 300 m2  

gentrification, gentrification  
Et buiten les pauvres, les immigrés  
Les sans travail, les abîmés de la vie  
Buiten buiten  

Allez ouste, ouste, on va tout nettoyer !  

<h5>3 & 4. Paul voor de pers/Tricot - Fragmenten </h5>

M-C  
J’aime pas rester seule.   
Alors je vais à l’espace rencontre.   
L’espace rencontre c’est surtout pour les vieux.   
C’est ouvert tous les jours de la semaine.   
Moi je mange là les midis. Y a un repas chaud.   
Sauf le mardi. Le mardi j’y vais pas. Le mardi c’est couscous.   
Il est cuisiné par les femmes arabes et ça j’aime pas.   
Mais l’après-midi il y a les activités.   
Des fois on fait des jeux, ou alors on danse,   
il y a aussi de la gym pour senior, ou alors on fête les anniversaires.   
Moi souvent, j’y tricote.   
Le tricot c’est quelque chose que j’aime.  

Paul  
Als ik opsta, ga ik naar de superette.   
Aan de overkant van de 5 blokken.   
In de superette neem ik een koffie aan de automaat.   
Daar een machine, da’s free coffee.   
Soms neem ik er twee.   
Brood, da’s met de kaart, het 10de is voor niks.   
Om de krant te lezen, ga ik naar De Munt, The Flemish Library.   
Kranten zijn er free access.   
Voor ’s middag, koop ik bij Lidl, da’s niet duur.   
Ofwel in ‘t slachthuis, zeebaars voor 8 of 9 euro.   
In de Carrefour kost die 17 euro.   
Koken doe ik thuis. Ik heb een kleine kookplaat, op de grond naast mijn bed.   
Da’s nie praktisch, vooral in de winter,   
vanwege de geur en de rook en ik kan niet verluchten.  