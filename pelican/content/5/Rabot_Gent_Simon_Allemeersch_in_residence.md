Title: Het Rabot
rows: page-7-1 / span 12
cols: left / span 12
type: text
count: 3

#### Simon Allemeersch in residence

<span class="number">1</span> Ja, ik ben jaloers. (Sinds een tijd staat er geregeld een ‘ja’ in
mijn zinnen). Dus: ja, ik ben jaloers, op Simon Allemeersch omdat hij in
Gent heeft kunnen doen wat mij in Brussel niet is gelukt.
{: .section-start}

<span class="number">2</span> Allemeersch is theatermaker. In 2010 slaagt hij erin een atelier te
krijgen in ‘de eerste blok’ van de woontorens aan het Rabot. De torens
gaan afgebroken worden. December 2010, schrijft hij in zijn prachtige
boek: ‘Er is een appartement vrijgekomen. Nummer 182, op het
gelijkvloers. Ik neem de sleutels over van Gustaaf en Roza. Ze zijn net
verhuisd'. Gustaaf en Roza zijn verhuisd, voorgoed vertrokken uit het
Rabot, zoals spoedig honderden anderen met hen. Allemeersch hangt een
infini in zijn atelier, een achterdoek van een oud theater, ‘een infini
is een onmogelijke plek, een verzonnen perspectief'.
{: .section-start}

'Ik schrijf een brief aan de bewoners en hang die in de inkomhal. Mijn
buurvrouw heet Norma. Ze komt me vragen wat de brief betekent'. Norma
kan lezen noch schrijven. Tijdens zijn driejarig verblijf in Rabot zal
Allemeersch nog ongeletterden tegenkomen. ‘Ik dacht eraan dat de brief
niet werkte. Ik kan beter werken zoals prostituees. Er is een groene
TL-lamp aan het raam. Als de lamp brandt kan je binnenkomen. Als de lamp
niet brandt, ben ik er niet of is er al iemand'.

<span class="number">3</span> Simon Allemeersch is gevraagd of hij iets kan doen in het Rabot door
Wannes Degelin, opbouwwerker van Samenlevingsopbouw. Die schrijft in
april 2009: ‘Ik werk nu al ongeveer zes jaar als opbouwwerker in de
Gentse Rabottorens. ... Al die jaren heb ik onrechtvaardigheid gevoeld
als er naar de blokken gekeken wordt, als er over de blokken geschreven
of gesproken wordt. Steevast worden ze afgeschilderd als een vreselijke
plek die je in het beste geval snel wil passeren. ... Een plek waar je
zeker nooit zou willen wonen. Ik ken deze plek toch wel anders'.
{: .section-start}


<span class="number">4</span> Ik herken *loads of things* in de kroniek van Simon Allemeersch en
zijn club (de opbouwwerker, een tekenares, fotografen een journaliste).
Dat misprijzen voor de blokken, terwijl Degelin ‘het een voorrecht vindt
om samen met de bewoners van de Rabottorens aan de slag te gaan'.
Opvouwwerker, schrijft mijn klavier. Dikwijls zijn sociaal-werkers
sloven die het vuil werk doen voor de woningmaatschappij, die pleisters
plakken, die soep bedelen, maar die machteloos staan, die soms zelfs
STATUTAIR NIET MOGEN tussenkomen als de bewoners (beter woord dan
huurders!) zich zouden organiseren en eisen zouden stellen.
Opvouwwerkers houden het ook rustig wanneer het opstandig zou kunnen
zijn.
{: .section-start}


<span class="number">5</span> In de Rabottorens konden kunstenaars vanuit hun eigen atelier onder
de bewoners werken toen het begon te spannen. Wat een uitzonderlijk
voorrecht ook! ‘De betekenis van die fysieke ruimte voor het vervolg van
dit project kan moeilijk overschat worden', noteert Wouter Hillaert in
de Rabot-kroniek. De kunstenaars hebben op de tast hun weg gezocht, ook
letterlijk soms want voor mensen die niet in de torens wonen, zijn ze
een labyrint; hulpdiensten verliezen er hopen tijd omdat ze juiste
ingang niet vinden. En dus, schrijft Allemeersch: ‘De bewoners en
Samenlevingsopbouw hebben elke blok een kleur gegeven: geel, blauw en
rood. Ik geval van nood kan je de kleur van het gebouw vermelden'.
{: .section-start}


<span class="number">6</span> De 5 blokken van de Papenvest in Brussel hebben vijf kleuren: rood,
oranje, groen en twee keer blauw (!). Een ingreep van buitenstaanders,
aangebracht tijdens het tweede Wijkcontract in 2014 of zo. De bewoners
onder elkaar noemen hun huisnummer als ze zeggen waar ze wonen, maar ook
dat neemt de verwarring niet weg want de 5 blokken liggen niet aan één
maar aan drie straten.
{: .section-start}


<span class="number">7</span> De Brusselse Woning beloofde ons, het Brussels Brecht-Eislerkoor,
een lokaal te geven in de 5 blokken, waar we open repetities zouden
kunnen houden en onze documentatie exposeren. Die belofte is niet
vervuld (nog niet), ik heb zo een vermoeden waarom niet ("je mag de hand
die geeft niet bijten"). Hadden we dat lokaal gehad, dan hadden we ons
gemakkelijker onder de bewoners kunnen mengen. Maar wat heet
gemakkelijk? Simon Allemeersch, en ook dat klinkt bekend, ‘krijgt
lelijke blikken in de lift. De roddel doet de ronde dat ik in dienst van
de woningmaatschappij oude mensen film en interview...'. Er wordt in
zijn atelier ingebroken, zijn experiment loopt niet over rozen. Maar uit
'Rabot 4-358', zo heet zijn boek, spreekt zoveel genegenheid, warmte,
verbondenheid dat dat de tegenslag doet vergeten. ‘Toen het gerucht (van
de dreigende sloop) een bericht was geworden, was over het gebouw een
finale vermoeienis neergedaald', aldus Wouter Hillaert. En dan neemt
daar zo’n stelletje kunstenaars zijn intrek... Het moet wel degelijk
sprankels licht hebben gebracht in de doodsstrijd van Rabot.
{: .section-start}


*Rabot 4-358*, 320 pagina’s, een rijkelijk geïllustreerd boek, gemaakt
door Maarten De Vrieze, Sofie Van der Linden, Mario Debaene en Bart
Capelle, is een uitgave van Kunstenwerkplaats Scheld’apen, met steun van
Samenlevingsopbouw Gent, Kunstencentrum Vooruit en de stad Gent, 2014.

<span class="number">8</span> *- post scriptum -* Toen ik eens langs het Rabot fietste, had ik daar
al meteen een *touche*, met een zekere Pepe die voor de ingang van een
resterende toren stond. ‘Bij de liefhebbers heb ik nog gekoerst met Eddy
Merckx', zei hij. Ik herkende hem van de film ‘Rabot’ van Christina
Vandekerckhove, òòk de moeite maar mismoediger van inslag. ‘Klopt', zegt
Pepe, ‘ge hoort mij aan het einde van de film zeggen dat ze Jean Gabin
moeten spelen op mijn begrafenis’ (was dat: *Je sais*...?). Pepe (°1944)
woonde in 2018 al 42 jaar in het Rabot.
{: .section-start}

<span class="number">9</span> *post post scriptum* - De voorbije jaren bracht het
'gelegenheidscollectief’ Lucinda Ra [het stuk Rabot
4-358](http://lucindara.be/en/rabot-4-358-en/) als een documentaire
theatervoorstelling. In 2017-2018 volgde het Grondwerk-project, een
driedaags open atelier ‘waar theater en werkelijkheid voortdurend door
elkaar liepen'. De groep hield ook halt aan de 5 blokken in Brussel en
had de Kaaistudio’s toen als uitvalsbasis. Onderdeel van de driedaagse
was een optocht van TOSO, [The Ostend Street
Orkestra](http://kleinverhaal.be/Projecten/Danse_Massacre). TOSO is een
project van Klein Verhaal in Oostende, waar het Brussels
Brecht-Eislerkoor zijn voorstelling over de 5 blokken zal gaan opvoeren.
En zo is deze cirkel rond.
{: .section-start}