Title: Rabot
rows: page-7-1 / span 12
cols: center-right / span 12
type: text
count: 3

#### Simon Allemeersch en résidence

<span class="number">1</span> Eh bien oui, je suis jaloux. (Depuis quelque temps, il y a
régulièrement un « oui » dans mes phrases). Donc : oui, je suis jaloux,
de Simon Allemeersch parce qu’il a pu faire à Gand ce que je n’ai pas
réussi à faire à Bruxelles.
{: .section-start}

<span class="number">2</span> Allemeersch est metteur en scène de théâtre. En 2010, il réussit à
obtenir un atelier dans le ‘premier bloc’ des tours d’habitation au
Rabot. Les tours vont être démolies. En décembre 2010, il écrit dans son
livre (un très bel ouvrage) : ‘Un appartement s’est libéré. Le numéro
182, au rez-de-chaussée. J’en reçois les clefs des mains de Gustaaf et
Roza. Ils viennent de déménager.' Gustaaf et Roza ont déménagé, sont
partis pour toujours du Rabot, comme le feront bien vite des centaines
d’autres avec eux. Allemeersch suspend un infini dans son atelier, une
toile de fond d’un vieux théâtre, ‘un infini est un lieu impossible, une
perspective imaginaire'.
{: .section-start}

'J'écris une lettre aux habitants et je la suspends dans le hall
d’entrée. Ma voisine s’appelle Norma. Elle vient me demander ce que la
lettre signifie.'. Norma ne peut ni lire, ni écrire. Au cours de son
séjour au Rabot, pendant trois ans, Allemeersch rencontrera d’autres
analphabètes. ‘Je me suis dit que la lettre de fonctionnait pas. Je
ferais mieux de travailler comme les prostituées. Il y a un néon vert à
la fenêtre. Si la lampe est allumée, tu peux entrer. Si la lampe n’est
pas allumée, c’est que je n’y suis pas ou qu’il y a déjà quelqu’un.'

<span class="number">3</span> C’est Wannes Degelin, un assistant social qui travaille pour
l’association d’aide au logement Samenlevingsopbouw, qui a demandé à
Simon Allemeersch s’il pouvait faire quelque chose au Rabot. Il lui a
écrit ceci en avril 2009 : ‘Je travaille depuis environ six ans déjà en
tant qu’assistant social dans les tours du Rabot à Gand ... Pendant
toutes ces années, j’ai ressenti comme une injustice quand on regardait
les blocs, quand on parlait des blocs ou qu’on écrivait à leur propos. À
tous les coups, on les dépeint comme un lieu horrible qu’on s’empresse,
dans le meilleur des cas, de traverser le plus vite possible ... Un
endroit où on ne voudrait sûrement pas habiter. Pourtant, je le connais
cet endroit, et je le vois autrement'.
{: .section-start}

<span class="number">4</span> Dans la chronique de Simon Allemeersch et de son club (l’assistant
social, une dessinatrice, des photographes et une journaliste), je
reconnais des tas de choses. Ce mépris envers les blocs, alors que
Degelin trouve que ‘c’est un privilège de pouvoir faire des choses avec
les habitants des tours du Rabot'. Voilà que mon clavier écrit ‘apaisant
social’ ... Souvent, les assistants sociaux sont ceux qui font le sale
boulot pour la société de logement, qui collent les pansements, qui
distribuent la soupe, mais qui sont impuissants, qui même parfois ont
l’INTERDICTION STATUTAIRE d’intervenir au cas où les habitants (à
préférer au mot ‘locataires'!) s’organiseraient et formuleraient des
revendications. Les apaisants sociaux préservent le calme lorsqu’il y a
risque de tempête.
{: .section-start}

<span class="number">5</span> Dans les tours du Rabot, des artistes ont pu travailler parmi les
habitants à partir de leur propre atelier quand les tensions ont
commencé à surgir. Quel privilège exceptionnel ! ‘On peut difficilement
surévaluer l’importance de cet espace physique pour la suite de ce
projet', note Wouter Hillaert dans sa chronique du Rabot. Les artistes
ont cherché leur chemin à tâtons, littéralement parfois d’ailleurs car
pour ceux qui n’habitent pas les tours, celles-ci sont un véritable
labyrinthe ; les services de secours y perdent un tas de temps parce
qu’ils ne trouvent pas la bonne entrée. Allemeersch écrit donc : ‘Les
habitants et Samenlevingsopbouw ont donné une couleur à chaque bloc :
jaune, bleu et rouge. En cas de besoin, on peut indiquer la couleur du
bâtiment.'
{: .section-start}

<span class="number">6</span> Les 5 blocs du Rempart des Moines ont cinq couleurs : rouge, orange,
vert et deux fois bleu (!). Une intervention de tiers, mise en œuvre
dans le cadre du deuxième contrat de quartier, en 2014 environ. Entre
eux, les habitants se donnent le numéro de leur maison lorsqu’ils disent
où ils vivent, mais la confusion subsiste car les 5 blocs ne donnent pas
sur une seule, mais sur trois rues.
{: .section-start}

<span class="number">7</span> Le Logement bruxellois nous avait promis, à nous Brussels
Brecht-Eislerkoor, un local dans les 5 blocs où nous pourrions organiser
des répétitions ouvertes et exposer notre documentation. Cette promesse
n’a (pas encore) été tenue, j’ai une vague idée du pourquoi (« il ne
faut pas mordre la main qui te nourrit »). Si nous avions eu ce local,
nous aurions pu nous mêler plus facilement aux habitants. Mais que veut
dire ‘facilement’ ? Simon Allemeersch, et c’est un discours qu’on
connaît bien, ‘affronte des regards méchants dans l’ascenseur. Il se
raconte que je filme et interviewe des vieilles personnes et que je le
fais au service de la société de logement ...' Son atelier est
cambriolé, l’expérience ne se déroule pas sans heurts. Mais son livre –
'Rabot 4-358’ – respire une telle chaleur, une telle empathie, une telle
solidarité qu’on en oublie les revers. ‘Lorsque la rumeur (la menace
d’une démolition) devint information, le bâtiment finissait de sombrer
dans une grande fatigue', écrit Wouter Hillaert. Et voilà qu’une bande
d’artistes vient s’y loger ... Il faut bien qu’ils aient apporté une
étincelle de lumière dans l’agonie du Rabot.
{: .section-start}

LIVRE

*Rabot 4-358*, 320 pages, un livre richement illustré, dont les auteurs
sont Maarten De Vrieze, Sofie Van der Linden, Mario Debaene et Bart
Capelle, est une édition du Kunstenwerkplaats Scheld’Apen, avec l’appui
de Samenlevingsopbouw Gent, du Kunstencentrum Vooruit et de la ville de
Gand, 2014.

<span class="number">8</span> *post scriptum* – Tandis que je traversais une fois le Rabot à vélo,
j’ai eu tout aussitôt une touche, un certain Pepe qui se tenait devant
l’entrée d’une des tours restantes. ‘J’ai participé à des courses chez
les amateurs avec Eddy Merckx', me dit-il. Je l’ai reconnu : on le voit
dans le film ‘Rabot’ de Christina Vandekerckhove, un film qui vaut la
peine également. ‘Eh oui', m’a dit Pepe, ‘dans le film je dis à la fin
qu’ils doivent jouer Jean Gabin à mon enterrement’ (voulait-il dire :
*Je sais* ...?). Pepe (°1944) habitait, en 2018, au Rabot depuis 42 ans.
{: .section-start}

<span class="number">9</span> *post post scriptum* – Ces dernières années, le ‘collectif de
circonstance’ Lucinda Ra a monté la pièce [Rabot
4-358](http://lucindara.be/en/rabot-4-358-en/), un spectacle de théâtre
documentaire. Suivi en 2017-2018 du projet Grondwerk, un atelier ouvert
de trois jours ‘où le théâtre et la réalité ne cessaient de se croiser'.
Le groupe s’est également arrêté aux 5 blocs à Bruxelles et était alors
basé aux studios du Kaai. Parmi les éléments de cet atelier, il y avait
un défilé, celui de TOSO : [The Ostend Street
Orchestra](http://kleinverhaal.be/Projecten/Danse_Massacre). TOSO est un
projet du Klein Verhaal à Ostende, où le Brussels Brecht-Eislerkoor va
présenter son spectacle sur les 5 blocs. C’est ainsi que le cercle se
referme.
{: .section-start}