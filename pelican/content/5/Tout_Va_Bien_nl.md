title: Tout Va Bien
slug: tout-va-bien-nl
lang: nl
type: fragments
cols: 2 / span 4
rows: page-11-1 / span 14
count: 1

Elk mens kent haar  
elk mens van wie een kind wordt afgepakt,  
een wieg, een steeg  
een wijk, een boomgaard  
van wie de wortels worden uitgerukt,  
de waardigheid gewist  

elk mens die wordt vertrapt, verjaagd,  
die op de vlucht moet  
onder het bommentapijt, onder de haat  

elke eeuw kent haar  
elke eeuw van oorlog  
van trappen naar beneden  
elke eeuw weet hoe zij daarboven  
met immens plezier   
vanuit hun hoogste verdieping, hun Mansions,  
hun hoofdkwartieren,  
alles weer teniet doen  
morzel na morzel, akker na akker,   
recht na recht, opstand na opstand,   
en alles weer vernielen dat lang door ons verworven was  

elk mens kent dan de vernedering   
na de nederlaag  
omdat het wringt  

hun grens schuift   
over mijn graven  
over mijn tuin  
hun grens eet mijn leven op  

en wat doe ik ? wat zal ik doen ? zal ik het hierbij laten ?  
en dan te weten dat ik niets heb gedaan toen het moest ?  
dat ik de huisbaas heb geloofd die altijd heeft gelogen  
dat ik de bullebak liet brullen die ons opjaagt  
houd ik mijn hand dan op ?  
laat ik hen mijn vel ?  
schuif ik aan, voor een gunst, aan hun kabinet ?   
zodat zij hooghartig mij een dienst betonen ?  

en, slaan zij, draai ik mijn andere wang hen toe ?  
\[voorgoed\]  
de spons erover ?  
terwijl elk neuron, elke pees, elk woedend woord aan onze keukentafel  
zegt :  
dit zal niet waar zijn  
uw feest gaat niet door  
uw Tout Va Bien zal niet voorgoed in onze weemoed verzanden  

Elk mens kent dat  
elk mens die zweert:  
tot hier uw geweld,  
maar verder komt gij niet  

van nu af aan aan u om terug te deinzen  

in dat gebaar, in dat verzet  
in die sprankelende melancholie  
daar vindt elk mens zichzelf terug »  
