title: Fragmenten · Extraits 3
lang: fr
type: fragments
cols: 2 / span 12
rows: page-12-1 / span 15
count: 2

<h5>11. The youth and the police. </h5>

We
The youth from the 5 bloks
We make up a good team
 “5 Bloks” it’s a small world
Feels as if there were walls
And yet there are no walls
You know it’s positive
Here it is our castle
We feel strong together
Here we feel protected

<h5>12. Faible d’esprit</h5>

... inutile de sonner pour venir me voir,
la sonnette ne fonctionne pas.
(...)
Quand je suis seul chez moi, j’écris, parfois.
Par exemple : si tu ne connais pas l’amour, tu ne connais pas Dieu.
J’en ai une autre phrase, ici,
mais à cause de mon cerveau qui ne fonctionne pas bien
je ne m’en souviens plus.

J’aime bien la poésie mais aussi la musique.
Sylvie Vartan, Fance Gall.
J’écoute de la musique tout seul.
Sheila, Claude François mais pas de Johnny.
(...)
Ah oui, la voilà ma phrase :
‘La beauté est éphémère mais la bonté est éternelle.’
C’est joli hein ?

<h5>13. Parlement (Chant Nr 4)</h5>

Solo:
Ze was gekleed in het rood,
Zeg maar  knalrood.
Ze belde overal aan,

Elle avait amené du thé
Et on l’a partagé
(...)
En toen hebben we allemaal gepraat
Over ons leven
Onze levensomstandigheden

Solo :
Et elle, elle savait pas tout ça

KOOR :
Ascenceurs en pannes
Champignons sur nos murs
Punaises dans nos lits
Rats dans nos tuyauteries
Kapotte liften
Schimmel op de muren
Luizen in ons bed
Ratten in de afvoer
Aux 5 blocs on vit pire que des animaux
In de vijfblokken hokken we erger dan de beesten.

<h5>14. Melancholie (Chant nr 5) - (Regine zingt) </h5>

<h5>15. La Durée du Chantier (Epiloog) </h5>

(...)
10 ans de marteaux piqueurs !
Vous vous imaginez ça, vous ?
