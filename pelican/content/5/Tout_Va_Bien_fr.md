title: Tout Va Bien
slug: tout-va-bien-fr
lang: fr
type: fragments
cols: 6 / span 4
rows: page-11-1 / span 15
count: 1

Chaque être humain la connaît  
chaque être humain à qui l’on enlève un enfant,  
un berceau, une ruelle  
un quartier, un verger  
à qui l’on arrache ses racines,  
à qui l’on efface sa dignité  

chaque être humain écrasé, chassé,  
qui doit fuir  
le tapis de bombes, la haine  

chaque siècle la connaît  
chaque siècle de guerre  
d’effondrement  
chaque siècle sait comment eux, ceux d’en haut  
prennent un immense plaisir  
depuis leur dernier étage, leurs manoirs,  
leurs quartiers généraux,  
à tout ruiner  
parcelle après parcelle, champ après champ  
droit après droit, révolte après révolte  
et détruire à nouveau ce que nous avions acquis depuis longtemps  

chaque être humain connaît alors l’humiliatio  
après la défaite  
et ça nous ronge  

leur frontière grignote lentemen  
mes tombes  
mon jardi  
leur frontière dévore ma vie  

et qu’est-ce que je fais ? qu’est-ce que je vais faire? en rester là ?  
et puis savoir que je n’ai rien fait quand il le fallait ?  
que j’ai cru le proprio qui n’arrêtait pas de mentir  
que j’ai supporté les hurlements de la brute qui nous traque  
dois-je mendier ? leur supplier ?  
leur laisser ma peau ?  
faire la queue pour une faveur à la porte de leur cabinet   
pour qu’ils condescendent à me faire l’aumône ?  

Et s’ils frappent, vais-je tendre l’autre joue ?  
[pour toujours]  
passer l'éponge   
alors que chaque neurone, chaque tendon, chaque mot furieusement prononcé autour de notre table de cuisine dit :  
cela ne se passera pas comme ça  
votre fête est finie  
votre Tout Va Bien ne va pas s’enliser pour toujours dans notre mélancolie  

Chaque être humain la connaît  
chaque être humain qui le jure :  
votre violence s’arrête ici,  
vous n’irez pas plus loin  

désormais c’est à vous de reculer  

c’est dans ce geste, dans cette résistance  
dans cette mélancolie effervescente  
que chaque être humain retrouve son soi  
