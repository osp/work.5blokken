title: Fragmenten · Extraits 2
lang: fr
type: fragments
cols: center-right / span 12
rows: page-11-1 / span 15
count: 2

<h5>5. Inhuldiging van de torens (Lied Nr 2 ) </h5>  

Wanneer politici hun torens bouwen,  
Hun sociale torens,  
Zijn die schitterend wit, kraaknet  
Ze huldigen ze in met veel omhaal  
Ze roepen met luide stem  
“ik ben het die…”  
Applaus alom.   
En dan ineens  
Zijn het hun torens, eigenhandig gebouwd.  

<h5>6. Dokter </h5>

Ik ben huisarts.   
Ik werk al 25 jaar in een gezondheidscentrum van de Vijf-Blokkenwijk.   
Ons doelpubliek is een arme bevolking.  
Volksmensen, als ge wil,   
met een laag inkomen, kansarm, zeg maar.   
Zoals die twee zussen die ik opvolgde :  
Die mensen werden overgeplaatst naar Neder-over-Heembeek,   
voor de duur van een andere renovatie,   
en daar waren ze helemaal de kluts kwijt, ontworteld,   
ze klaagden telkens ik er kwam.   
Ge zou kunnen zeggen dat hun nieuwe woonst beter was,   
ruimer, luchtiger; er was een lift, in een groene buurt:   
Maar voor de zussen was het een echte ramp,   
omdat ze hun vertrouwde plekken, hun winkels kwijt waren.   
Een paar maanden later ging ik bij hen op huisbezoek   
en ze waren dolblij !   
Dolblij, omdat een van beide eindelijk een GB had ontdekt.  
De gedachte dat er in hun buurt een GB was bracht hen troost.   

<h5>7. Être social  </h5>

Moi je me demande,   
est-ce que nos élus locaux savent ce que ça veut dire être social ?   

Je veux pas faire la maline hein, mais j’ai 85 ans,   
et parti socialiste ou pas, je m’en fous,   
moi j’en fais du social et point.   
Et moi, je vais te dire c’est quoi mon social à moi.  

Tu dois savoir, moi j’avais travaillé pendant 40 ans,   
me dépêcher le matin,   
faire la gamelle de mon mari,   
les enfants, les conduire, aller travailler.   
J’avais toujours travaillé   
et puis je tombe en pension et qu’est-ce que je fais ?   
Moi, si je reste à la maison je m’ennuie,   
alors un jour je suis allée au foyer bruxellois,   
(le foyer bruxellois c’est le propriétaire des tours sociales),   
et j’ai demandé un rez de chaussée.   
Je voulais faire des petits déjeuner pour les gens des 5b.   

J’ai reçu un petit local. Tout petit.   
Il y avait pas de wc, il y avait pas d’eau, il y avait rien, ca puait le pipi de chats.   
Alors j’ai nettoyé, nettoyé comme y faut.   
Je faisais le déjeuner à 2,5 francs   
et le libraire, un brave homme,   
ben il offrait le journal,   
enfin deux, un flamand et un francophone.   

Le local je l’ai appelé l’Entraide.   
Quand ils ont vu que j’étais sérieuse,   
du foyer bruxellois j’ai reçu l’ancien salon de coiffure,   
où on est encore maintenant.   
Et puis un jour, qu’est-ce qui m’a pris j’ai dit   
« et si on faisait une aide aux devoirs ? »   
Et là c’était parti.   
Avec Marie-Christine et Maïka, et puis Rachida.   

On a vu passer des centaines d’enfants ici,   
et attention, des qui après ont fait l’université !   
Y en a qui sont tellement reconnaissants,   
ils viennent de loin, ils prennent le train pour venir me saluer.   

Moi j’aime bien m’occuper des autres   
parce que je suis orpheline depuis toute jeune.  
M’occuper des autres, vous voyez, j’ai ça dans la peau.   
C’est un truc, je peux juste pas m’en empêcher.  

Bientôt on va démolir ici,   
mais moi, si j’ai plus de local pour l’école de devoir vous verrez,   
c’est pas pour ça que je vais abandonner,   
ça non,   
je trouverai un autre endroit,   
je me battrai si y faut mais je continuerai.   

Parce que je suis une révolutionnaire, moi.   
Quand je suis pas d’accord,   
quand je trouve qu’y a pas de justice,   
alors je me bats.   
Et moi quand je m’y mets attention hein,   
vous devriez voir ça,   
y a rien qui m’arrête.   

Je veux pas faire la maline hein attention   
mais vraiment ce qu’on fait nous ici, pour moi,   
ça c’est du vrai social,   
pas comme leur boniments là, à ces politiques,   
et à tous ces beaux parleurs en cravatte.  

<h5>8. Réunion avec le bourgmestre </h5>

Bourgmestre   
Bonsoir à toutes et tous.   
Ik heb niet veel tijd, waarvoor alvast mijn excuses.   
Vous allez bien ?   
(long silence, tout le monde s’assied)  
Bon alors, dan kunnen we beginnen ?   
Parfait !  
(silence)  
 (...)  
Oui c’est vrai il y a eu du retard mais… ne vous inquiétez pas   
maintenant je prends le dossier personnellement en main.   
Voor mij is dit dossier prioritair.  
(...)  
Et ça va bien se passer,   
tout va bien se passer ne vous inquiétez pas !  
(regardant sa montre)    
Ca va pour les questions ?  
(...)  

<h5>9. Le prix pour être ici c’est le silence et l’oubli (Chant Nr 3)  </h5>

Si jamais tu es pauvre   
Alors on te méprise  
Toujours on te répète  
T’as qu’à faire, t’as qu'à faire autrement  

Tu es noir ou chômeur  
Femme seule avec enfants  
Tu es handicapé  
C’est que tu l’as cherché !  

C’est qu’t’as pas essayé  
Tu t’es pas concentré  
pour naître beau blanc riche   
et puis en bonne santé   
(...)  
Entassés l’un sur l’autre  
Comme des poulets d’batterie  
Le prix pour être ici  
c’est silence et l’oubli  

t’es pas content ? tire-toi !  
y en a plein derrière toi  
y en plein sur la liste  
qui en ont marre d’attendre  
(...)  
Alors quand on nous dit  
Qu’il vaut mieux la fermer  
J’vous avoue on oublie  
Qu’on a voulu parler  

<h5>10. Melancholie. </h5>

En wat doe ik ?   
wat zal ik doen ?   
zal ik het hierbij laten ?  
en dan te weten dat ik niets heb gedaan toen het moest ?  
dat ik de huisbaas heb geloofd die altijd heeft gelogen  
dat ik de bullebak liet brullen die ons opjaagt  
houd ik mijn hand dan op ?  
laat ik hen mijn vel ?  
schuif ik aan, voor een gunst,  aan hun kabinet ?   
zodat zij hooghartig mij een dienst betonen ?  

en, slaan zij, draai ik mijn andere wang hen toe ?  
[voorgoed]  
de spons erover ?  

terwijl elk neuron, elke pees, elk woedend woord aan onze keukentafel zegt :  
dit zal niet waar zijn!  
uw feest gaat niet door!  
