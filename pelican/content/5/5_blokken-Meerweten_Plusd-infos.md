Title: Meer weten <br>En savoir plus
cols: 8 / span 4
rows: page-12-14 / span 7
count: 1
type: colofon

### Papenvest Sociale Woniningen

#### 2017, augustus  
http://soulpress.be/blog-brussel-met-papenvest-verdwijnt-hele-sociale-woonwijk

#### 2017, octobre  
http://www.ieb.be/Rempart-des-Moines-une-cite-de-logements-sociaux-a-Bruxelles-va-disparaitre

#### 2019, maart  
http://soulpress.be/brussel-van-renoveren-naar-slopen-de-papenvest-speelbal-van-de-politiek

#### 2019, mars  
http://www.ieb.be/De-la-renovation-a-la-demolition-Le-Rempart-des-Moines-jouet-de-la-politique

#### 2019, oktober  
http://soulpress.be/brussel-nieuwe-woonkazernes-aan-de-papenvest