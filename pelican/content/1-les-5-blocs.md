Title: Les 5 blocs
lang: fr
rows: page-2-1 / span 15
cols: 6 / span 4
count: 1
category: 1
fill: auto


<span class="number">1</span>‘Les 5 blocs’ est le nom qu’on donne communément au complexe du
Rempart des Moines. Il se compose de cinq tours d’habitation avec au
total 320 logements sociaux. La construction des tours a été entamée en
1964 et s’est achevée pour l’essentiel en 1966. C’est la société
Logements bruxellois- Brusselse Woning (LBW) qui en est propriétaire. Il
s’agit d’une ‘société immobilière de service public’ qui gère des
logements sociaux. Elle s’appelait auparavant le Foyer
bruxellois-Brusselse Haard.
{: .section-start}

<span class="number">2</span>Le Foyer bruxellois a été créé en 1922. La ville de Bruxelles
possède la majorité des parts. Lors de son 75e anniversaire, en 1997, La
Fonderie, le centre de recherche d’histoire sociale de Bruxelles dont le
siège est à Molenbeek, a réalisé un livre commémoratif édité par le
Foyer bruxellois.[^1]
{: .section-start}

Le Logement bruxellois- Brusselse Woning d’aujourd’hui est né en 2016 de
la fusion entre le Foyer bruxellois et une autre société bruxelloise de
logements sociaux, LoReBru-GeBruWo. Le LBW nouveau gérait alors 3.883
logements.

<span class="number">3</span>Les 5 blocs ont été construits sur une parcelle dans la partie Ouest
du centre de Bruxelles (le Pentagone), près du canal Bruxelles-Charleroi
et de l'écluse de la porte de Ninove. Cette partie du Pentagone a
longtemps été appelée quartier des Fabriques, ou encore le Coin perdu.
{: .section-start}

<span class="number">4</span>Le Quartier des Fabriques bruissait d’activités économiques au 19e
siècle. Il comptait bon nombre d’impasses. C'était en fait un labyrinthe
d’ateliers et de taudis. Dans les années 1930, la ville de Bruxelles
projetait d’assainir le Quartier des Fabriques. On allait jeter bas des
centaines d’habitations ouvrières insalubres. Mais il n’y eut pas
d’assainissement. C’est en 1955 seulement que la ville de Bruxelles
remit ses plans sur le métier. Elle décida qu’il fallait construire, là
aussi, des logements sociaux et elle chargea le Foyer bruxellois de
mettre en œuvre le projet Rempart des Moines.
{: .section-start}

<span class="number">5</span>En 1961, la ville de Bruxelles confiait les travaux au Groupe
Structures. ‘Le projet prévoit la construction de quatre tours
d’habitation de 13 étages et d’une tour de 10 étages, avec au total 344
appartements et les bureaux du Foyer bruxellois’, dit le livre
commémoratif. Mais le service d’urbanisme de la ville s’opposait à ce
plan, elle trouvait la densité de population trop élevée. L’urbanisme
suggérait de prévoir moins de studios et davantage d’appartements à 3 ou
4 chambres. Un compromis était conclu avec un nouveau plan de 320
logements sociaux.
{: .section-start}

[^1]: Guido Vanderhulst e.a., *3000 Foyers bruxellois*, Les dossiers de
    La Fonderie, n°2, octobre 1997, bilingue
