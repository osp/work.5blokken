title: De Duivelshoek
lang: nl
category: 1
rows: page-4-1 / span 14
cols: left / span 12
count: 3

<span class="number">1</span> ‘Als je leven (en je portemonnee) je lief is, blijf dan weg uit de
Duivelshoek'. Dat was, tot nog niet zo lang geleden, één van de
ongeschreven aanbevelingen voor toeristen. Gaan wandelen in de
kanaalbuurt van de Vijfhoek, dat deed je niet, want dit was een *No Go
Zone*. De wijk ontleende haar kwalijke reputatie ook aan haar naam. In
de jaren 1600 was hier namelijk een pakt met Belzebub *himself*
gesloten. Dat zat zo.
{: .section-start}

In die tijd moest een architect een brug bouwen over de Kleine Zenne,
tussen de Vlaamse en de Anderlechtsepoort, ergens ter hoogte van Le
Chien Vert. De man heette Olivier (of Bernard) Raessens. Dat schrijft
Eric Timmermans met grote stelligheid. Hij verwijst naar de
duivelsdeskundige Collin de Plancy die in zijn *Histoire Infernal*
geschreven had dat de eerste steen van de brug gelegd was op 28 avril
1658![^1] Soit!

<span class="number">2</span> Architect Raessens had het werk onderschat. Het land waar hij
werkte, was drassig, zijn brug was wankel, de werken liepen vertraging
op, de kosten stegen en Raessens geraakte in geldnood. Op zo’n kritiek
moment verschijnt altijd de duivel en biedt zijn hulp aan. Hier ging het
ook zo. De duivel sloot een contract met Raessens, leende hem 100.000
florijnen maar zou die tien jaar later komen terughalen.
{: .section-start}

Over de afloop circuleren verschillende versies (over al het voorgaande
trouwens ook). Toen de rekening vereffend zou worden, haalde de
architect er een kanunnik van de Sint-Gudulakerk bij (tegenwoordig de
katedraal) die de duivel met een tjeventruuk op de vlucht joeg. De
overlevering wil dat de sluisbrug aan het Klein Kasteeltje sindsdien de
Duivelsbrug heette. De wijk kreeg de naam Duivelshoek.

<span class="number">3</span> Later kwam er een berucht café in de wijk, *Le Coin du Diable*, op
de hoek van de Vandenbrande&shy;straat en de Onze-Lieve-Vrouw-van-Vaakstraat.
En de legende van de architect en de duivel lag aan de basis van de
Duivelskermis en de Duivelsprocessie die elk jaar rond 20 augustus
uittrok. Elke volkse buurt in het land had toen haar jaarlijkse kermis.
William Deraedt, een buurman van de 5 blokken aan de Papenvest, heeft
die geschiedenis uitgezocht. De kermis die minstens een week duurde en
het leven in de wijk stillegde (zelfs de karakollenmarchands voerden dan
geen steek uit) begon met de feestelijke begrafenis van de oude duivel.
Er werd dan een beeld in zwart hout (of was het leder?) van de duivel op
een lijkbaar rondgedragen. Maar meteen daarna werd een nieuw duiveltje
ingehaald en gedoopt, ‘men moest immers het volgende jaar opnieuw een
begrafenis kunnen vieren'.[^2]
{: .section-start}

<span class="number">4</span> Het is niet zeker of het klopt dat de laatste duivelsprocessie in
1939 uittrok, zoals William Deraedt schrijft. En wel om deze reden: het
beeld van de duivel bestaat echt en is in 1936 geschonken aan de stad
Brussel. Het wordt bewaard in de collectie van het Broodhuis, het
stadsmuseum op de Grote Markt. Bij de overdracht in 1936 schreef de
toenmalige eigenaar, een zekere Max Delecosse, dat hij het beeld voor
150 Belgische franken had gekocht van J.B. De Geynst, de uitbater van
het café De Duivelshoek. Waarschijnlijk, maar dat is dus een gissing,
kende de buurt de cafébaas niet als mr De Geynst maar als Jean De Spons,
omdat hij gemakkelijk vocht opnam.
{: .section-start}

<span class="number">5</span> Wat waar is van het duivelspakt, en wat verzonnen, dat weten alleen
architect Raessens en Belzebub de Duivel. Maar de Duivelshoek heeft
nooit enige moeite gedaan om zich een beschaafder reputatie aan te
meten. De jongemannen uit de wijk waren een te duchten partij. Het
gebeurde dat jonge Marolliens of Vaartkapoenen (uit Molenbeek) naar de
kanaalwijk afzakten omdat ze wisten dat ze daar een stevig robbertje
konden knokken.
{: .section-start}

Maar toen in 1907 een ‘sociale enquête’ werd uitgevoerd, bleek de
toestand in de *impasses* van de Duivelshoek erger dan in de Marollen.
Er was veel kinkhoest, kindersterfte, kroep, de riolen lagen open. Er
woonden nu vooral voddenrapers, oudijzermarchands, straatventers en
kruiers. Vanaf de jaren 1930 begon een zekere sanering. Die werd niet
doorgezet, zodat de wijk alleen maar verder achteruit ging. In de jaren
1950 kreeg de kanaalwijk dan, zoals dat heet, een andere bestemming. Het
zag ernaar uit dat het braakland aan vastgoedpromotoren gesleten zou
worden. Maar uiteindelijk begon de stad toch te onteigenen om nieuwe
sociale woningen te bouwen. Dat plan mondde uit in de bouw van de 5
blokken van de Papenvest.

<span class="number">6</span> In zijn Kroniek van de Duivelshoek heeft William Deraedt veertig
kortverhalen uit de wijk verzameld. Stuk voor stuk getuigenissen van hoe
hard en miserabel het leven er was. Maar tegenover de buitenwereld
trokken de mensen er aan één zeel ‘als waren ze gefabriceerd uit
dezelfde broek'. Want als er iets gebeurde in de binnenstad en ‘ze
vinden direct geen coupabele, dan wordt er altijd met de vinger gewezen
naar die van de Marollen of de Duivelshoek'. Als de kranten over de
Duivelshoek schrijven, dan in de kolommen ‘Diefstallen en
vechtpartijen', ‘alsof er alleen hier ambras was en bij de
*faux-collants* alles peis en vree was, veel schone schijn'. In 1879
wordt hier inderdaad de Bende van de Floeren Vesten opgerold. Maar in de
jaren van de Duitse bezetting laat de Duivelshoek zich van een andere
kant zien. Hier gedijde de zwarte markt. Maar in april 1942 voert een
colonne zwaarbewapende Duitse ‘legerknechten’ een razzia uit. Ze halen
een gezin uit een hoekhuis in de Hopstraat, "maar de kleinste
ontbreekt". De kleinste is een joodse jongen die op het allerlaatste
moment door de buren is verstopt. En zo gaat deze kroniek maar door.
{: .section-start}

<span class="number">7</span> De '*faux-collants*' (de burgers met hun uitgestreken gezichten)
houden opstandige wijken onder de duim om ze te temmen. Daarom geven ze
haar een stempel. Dat was al zo toen de *impasses* nog bestonden. En dat
ging voort met de 5 woonblokken, *les impasses verticales*, van de
Brusselse Haard. De blokken worden nog voortdurend gebrandmerkt als
'Oostblokarchitectuur'. Maar die uitdrukking is compleet misplaatst; ze
zit bewust of niet vol Koude Oorlog-venijn. In feite zijn de 5 blokken
*Uncle Sam*-architectuur. Want daar, bij Uncle Sam in de United States,
haalden de architekten van de Groupe Structure hun inspiratie. Wie dat
stigma nog eens op de 5 blokken van de Papenvest wil drukken, moet dus
zijn woordenschat verversen.
{: .section-start}

[^1]: Eric Timmermans, *Le quartier du « Coin du Diable »*, blog op
    belgallica.tumblr, zonder datum

[^2]: William Deraedt, *De Duivelshoek. Kroniek van een verdwenen
    volksbuurt, 1800-1960*, onuitgegeven publicatie.