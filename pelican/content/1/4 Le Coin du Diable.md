Title: Le Coin du Diable
lang: fr
category: 1
rows: page-4-1 / span 14
cols: 16 / span 12
fill: auto
count: 3

<span class="number">1</span> ‘Si tu tiens à la vie (et à ton porte-monnaie) ne va pas au Coin du
diable'. C'était, jusqu’il y a peu, une des recommandations non écrites
adressées aux touristes. Se promener dans le quartier du Coin du Diable,
ça ne se faisait pas, car c'était une *no go-zone*. Le quartier devait
aussi sa fâcheuse réputation à son nom. C’est que dans les années 1600,
un pacte y avait été conclu avec Belzébuth *himself*. Voici comment les
choses se sont passées.
{: .section-start }

À cette époque, un architecte avait été chargé de construire un pont
par-dessus la petite Senne, entre la porte de Flandre et la porte
d’Anderlecht, à peu près à la hauteur du Chien Vert. Il s’appelait
Olivier (ou Bernard) Raessens. C’est ce qu’affirme en tout cas Eric
Timmermans lequel se réfère à Collin de Plancy, un spécialiste en
diableries qui a écrit dans son *Histoire infernale* que la première
pierre du pont a été posée le 28 avril 1658.[^1] Soit !

<span class="number">2</span> Raessens avait sous-estimé l’ampleur de la tâche. Le terrain sur
lequel il travaillait était marécageux, son pont était bancal, le
chantier avait pris du retard, les coûts augmentaient à toute allure et
Raessens avait besoin d’argent. C’est à des moments critiques comme
ceux-là que le diable surgit pour offrir son aide. Le diable conclut un
contrat avec Raessens, lui prêta 100.000 florins en disant qu’il
viendrait les récupérer dix années plus tard.
{: .section-start }

Des versions différentes circulent sur la suite de l’histoire (sur ce
qui précède aussi d’ailleurs). Quand il fallut régler la dette,
l’architecte se fit accompagner par un chanoine de l'église
Sainte-Gudule (l’actuelle cathédrale) qui mit le diable en fuite grâce à
un quelconque tour de passe-passe catho. C’est depuis lors, dit-on, que
le pont-écluse qui jouxte le Petit Château s’appelle le Pont du Diable.

<span class="number">3</span> Par la suite, un café célèbre s’installa dans le quartier, *Le Coin
du Diable*, au coin de la rue Vandenbranden et de la rue
Notre-Dame-du-Sommeil. Et la légende de l’architecte et du diable fut à
la base de la kermesse du Diable et de la procession du Diable qui se
tenaient chaque année vers le 20 août. À l'époque, chaque quartier
populaire du pays avait sa kermesse annuelle. William Deraedt, un voisin
des 5 blocs au Rempart des Moines, a étudié cette histoire. La kermesse,
qui durait au moins une semaine et au cours de laquelle toute vie
s’arrêtait dans le quartier (même les marchands de caricoles cessaient
leurs activités) commençait par l’enterrement du vieux diable. On
portait alors sur un brancard une statuette de bois noir (à moins que ce
ne fût du cuir?) à l’effigie du diable. Mais aussitôt après, on allait
chercher un nouveau diablotin auquel on administrait le baptême, ‘car il
fallait bien pouvoir célébrer un enterrement l’année suivante'.[^2]
{: .section-start }

<span class="number">4</span> Il n’est pas certain que la dernière procession du Diable ait eu
lieu en 1939 comme l'écrit William Deraedt. Et pour une bonne raison :
la statuette du diable existe bel et bien, elle a été offerte en 1936 à
la ville de Bruxelles. On la conserve dans la collection de la Maison du
Roi, le musée de la ville à la Grand-Place. Lors du transfert de 1936
son propriétaire, un certain Max Delecosse, écrivit qu’il avait lui-même
acheté la statuette pour 150 francs belges à J.B. De Geynst,
l’exploitant du café du Coin du Diable. Sans doute – mais c’est donc une
supposition – M. De Geynst était-il alors connu dans le quartier non pas
sous son nom, mais sous celui de Jean L’Éponge (Jan De Spons), parce
qu’il absorbait facilement les liquides en tout genre.
{: .section-start }

<span class="number">5</span> Seuls l’architecte Raessens et Belzébuth savent si ce pacte
diabolique est pure invention ou réalité. Mais le Coin du Diable n’a
jamais rien fait pour se donner meilleure réputation. Les jeunes gens du
quartier étaient connus pour être de solides bagarreurs. Il arrivait que
de jeunes Marolliens ou des Vaartkapoenen de Molenbeek descendent sur le
quartier du canal parce qu’il savaient qu’il y aurait de la castagne.
{: .section-start }

Mais lorsqu’une ‘enquête sociale’ fut effectuée en 1907, il s’avéra que
la situation était pire, au Coin du Diable, que dans les Marolles. La
coqueluche et le croup régnaient, la mortalité infantile était énorme,
les égouts étaient à ciel ouvert. Les habitants étaient désormais
surtout des chiffonniers, des ferrailleurs, des colporteurs et des
porteurs. On commença d’assainir le quartier à partir des années 1930.
Mais l’opération d’assainissement ne fut pas poursuivie, de sorte que le
déclin du quartier se poursuivit de plus belle. C’est dans les années
1950 que le quartier du canal reçut, comme on dit, une autre
affectation. On crut d’abord que les terrains en friche allaient être
cédés à des promoteurs immobiliers. Mais en définitive, la ville se mit
à exproprier pour construire de nouveaux logements sociaux. Ce plan
déboucha sur l'édification des 5 blocs au Rempart des Moines.

<span class="number">6</span> Dans sa *Kroniek van de Duivelshoek* (chronique du Coin du Diable),
William Deraedt a rassemblé quarante récits du quartier. Autant de
témoignages de la vie dure et misérable que les gens y vivaient. Mais
face au monde extérieur, ils étaient comme les doigts d’une main, ‘comme
fabriqués de la même culotte'. Car si quelque chose se passait en ville
et ‘on ne trouve pas tout de suite le coupable, alors on montre
directement du doigt ceux des Marolles ou du Coin du Diable'. Quand les
journaux parlent du Coin du Diable, c’est dans la rubrique des ‘vols et
des échauffourées', ‘comme si on ne faisait que se battre ici et comme
si tout n'était que calme et sérénité chez les *faux-collants*,
tellement soucieux des apparences'. En 1879, c’est vrai, on démantèle
ici la Bande des vestes de velours. Mais sous l’occupation allemande, le
Coin du Diable se montre sous un autre jour. Le marché noir y prospère.
Cependant, en avril 1942, une colonne de soldats allemands lourdement
armés y effectue une rafle. Ils font sortir une famille d’une maison au
coin de la rue du Houblon, ‘mais le plus jeune manque'. Le plus jeune,
c’est un garçon juif que les voisins ont caché au dernier moment. Et la
chronique se poursuit dans le même style ...
{: .section-start }

<span class="number">7</span> Les ‘faux-collants’ (les bourgeois qui tirent la tête) gardent les
quartiers rebelles sous leur coupe pour les dompter. C’est pourquoi ils
leur collent une étiquette. C'était déjà le cas quand les impasses
existaient encore. Et ça a continué avec les 5 blocs, les *impasses
verticales*, du Foyer bruxellois. Les blocs, répétait-on sans cesse,
étaient un exemple d’architecture ‘des pays de l’Est'. L’expression est
totalement déplacée : consciemment ou non, elle a un fort cachet de
Guerre froide. En fait, les 5 blocs sont un exemple d’architecture à la
*Uncle Sam*. Car c’est là, chez l’oncle Sam aux États-Unis, que les
architectes du Groupe Structures ont cherché leur inspiration. Ceux qui
veulent à tout prix stigmatiser les 5 blocs feraient donc bien de
rafraîchir leur vocabulaire.
{: .section-start }

[^1]: Eric Timmermans, *Le quartier du « Coin du Diable »*, blog sur
    belgallica.tumblr, non daté

[^2]: William Deraedt, *De Duivelshoek. Kroniek van een verdwenen
    volksbuurt, 1800-1960*