title: De sloop is beslist
lang: nl
rows: page-2-1 / span 9
cols: 16 / span 6
category: 1
count: 2
fill: balance
borderbottom: none

> La décision est tombée:  
> ceux qu’on appelle les 5 blocs,  
> les 5 barres de notre quartier,  
> nos tours sociales,  
> nos habitations:  
> Rasés!

<span class="number">1</span> Het nieuws dateert van 30 juni 2016: de vijf woontorens aan de
Papenvest worden gesloopt. De Brusselse regering keurt dat project goed.
Het is bedacht door de stad Brussel en de Brusselse Woning, eigenaar en
uitbater van de sociale woningen aan de Papenvest. Volgens een
perscommuniqué leggen de stad en de eigenaar 73,5 miljoen euro samen om
sloop-en-nieuwbouw te bekostigen. In het persbericht luidt het zo : "het
project plant de afbraak van 314 bestaande sociale woningen en de
heropbouw van 350 nieuwe woningen waaronder 210 sociale woningen en 140
woningen voor middeninkomens. (...) Het project neemt ook een
ondergrondse parking op".
{: .section-start}

<span class="number">2</span> *Du coup* zal het nieuwe wooncomplex 110 sociale woningen minder
tellen dan de actuele site. *Du coup* komt er een nieuwe ondergrondse
parking in de wijk. Een parking voor 300 auto’s, denkt men eerst; later
blijkt dat er 400 parkeerplaatsen komen.
{: .section-start}

*Du coup* zal het nieuwe complex niet één maar twee uitbaters hebben: de
Brusselse Woning voor de 210 sociale woningen en de Grondregie van de
stad Brussel voor de middenklassewoningen. Dat komt tijdens de
beraadslaging in de Brusselse gemeenteraad van 7 november 2016 aan het
licht. Het vermoeden rijst dan dat de Grondregie met die 140
middenklassewoningen de commerciële woonmarkt opgaat. Hetgeen het
sociale karakter van het Papenvest-complex zou aantasten.

<span class="number">3</span> Het persbericht van 30 juni zegt dus niet alles. Het stelt de zaken
ook schoner voor dan ze zijn. De sloop wordt bij voorbeeld met deze
passage verantwoord \[onze vertaling uit het Frans\]: "De achterhaalde
bouwvorm en het gebrek aan isolatie van de 5 woontorens waren een bron
van hinder, ongezondheid en ongemak geworden voor de ongeveer 850
huurders waarbij we nog niet spreken over de zware energie-uitgaven waar
ze mee geconfronteerd worden". Er wacht de Papenvest dus een "uitgebreid
renovatieprogramma". Maar slopen is niet renoveren. Slopen is slopen,
punt uit.
{: .section-start}