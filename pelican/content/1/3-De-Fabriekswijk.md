title: De Fabriekswijk in de jaren 1800
lang: nl
category: 1
rows: page-3-1 / span 16
cols: left / span 12
count: 4
fill: auto

<span class="number">1</span> In de tijd van Napoleon woonde er niemand waar nu de 5 blokken
staan. Maar niet lang daarna begon dit westelijke deel van de Vijfhoek
vol te lopen, en snel ook. Hier was plaats, voor de industrie. En de
bazen trokken werkvolk aan. Zoals de Vijfhoek destijds volk opving -
volk in de proletarische betekenis van het woord: werkzoekers,
gelukszoekers, ontheemden, dat moet een ware explosie zijn geweest. Dit
deel van de stad stroomde over. Tot fortuin van de ondernemende stand,
terwijl het volk op elkaar gepakt in krotten moest gaan hokken. En van
zodra het kanaal van Brussel naar Charleroi gegraven en geopend was (in
1832) en de haven voor extra-bedrijvigheid zorgde, ging deze buurt de
Marollen achterna. Ook hier ontstond een wijk van fabrieken en
schoorstenen met daartussen nog enkele rivier-armen van de Zenne en een
wirwar van miserabele beluiken. Vanaf dan maakte de Verloren Hoek zijn
reputatie als een autentieke Brusselse volkswijk waar bourgeois niet
dierven komen.
{: .section-start}

Twee straten die vandaag het perceel van de 5 blokken begrenzen, stonden
in de Franse tijd al op de kaart.[^1] Het Franse regime herdoopte deze
straten tijdelijk: Onze Lieve Vrouw van Vaak werd *Rue du Calendrier
Républicain*, de Papenvest werd *Rempart Cisalpin*! De Papenvest was een
restant van de ‘cleyne vesten’ die doorliepen tot aan het huidige
Fontainasplein. Maar omdat de kern van Brussel groeide, raakte deze
verdedigingswal in de jaren 1300 al in onbruik. In de jaren 1600 werd de
Paepengrecht, naast de vroeger wal, drooggelegd en verkaveld. Ook de
Fabrieksstraat stond al op de kaarten uit de Franse tijd. Ze sneed dwars
door het domein van de Kartuizers (ongeveer aan het Bloemenhofplein) en
liep tot waar de Ninoofsepoort zou komen.

Maar tussen de Fabrieksstraat en de Vlaamse steenweg, lag een natte,
groene zone, die ze later de Verloren Hoek zouden noemen, met grachten
en bleekweiden, en een beek die stonk van het rottend afval en de
beestenkadavers. Die beek, de Bummel, vulden ze later op voor de
Dansaertstraat. Twee-drie-takken van de Zenne stroomden toen nog door
dit stadsdeel. Het waren open riolen, waarin de fabrieken en de mensen
hun afval dumpten.

<span class="number">2</span> Vanaf de vroege jaren 1800 vestigde de nieuwe industrie zich in
Brussel. Dat nam zo’n vaart dat Brussel toen het echte industriële hart
van België was. De industriële bekkens van Wallonië volgden pas later.
De fabrieken verrezen waar er plaats was, in de ‘Verloren Hoek’, de
groene zone van het Westelijke deel van de stad. De bazen woonden in
herenhuizen aan de straatkant, daarachter stonden de ateliers. En
daarachter, in wat er aan ruimte overbleef, pakten zij het werkvolk
samen in krotten.[^2] De mensen zaten er in armoede samengepropt, leven
was er onmogelijk.
{: .section-start}

<span class="number">3</span> Dit hele gebied tussen en naast de rivierarmen werd verkaveld. De
binnenpleinen werden ingenomen door zoveel mogelijk minuscule panden. De
krotten stonden opgesteld in rijen (‘*allées*’) of in vierkanten
(‘*bataillons carrées*’), zoals de beruchte beluiken in Gent. De
Waterpoortgang bij voorbeeld (verdwenen, een zijstraat van de
Kartuizersstraat) was maar 2,5 meter breed maar er stond een 15-tal
krotten, in drie rijen, van 12 centiare elk (12 m<sup>2</sup>). Er vlak naast lag
een brouwerij die van de steeg het licht en de lucht afnam.
{: .section-start}

In 1800 werden in de ‘Vijfhoek’ 66.000 bewoners geteld, in 1890 waren
dat er 159.000. Dat is de statistiek van de explosie. Vanaf dan was de
Westkant het dichtst bevolkt. Dat blijft hij tot vandaag. Uit deze wilde
verkaveling ontstonden de Brusselse ‘*impasses*’, doodlopende gangen
waar het volk op elkaar gepakt leefde. Hier woonden de
fabrieksarbeiders, de dokwerkers, de boottrekkers, het werkvolk, de
kleine stielen. Ze waren er slecht aan toe. Toen rond 1840 de eerste
studies over de sociale situatie in het koninkrijk België verschenen
(van Quetelet en Ducpétiaux), bleken in deze *impasses* méér mensen te
sterven dan elders in Brussel, ‘de sterftecijfers volgden de tracés van
deze beluiken'.[^3] Een latere kronikeur wist niet beter dan dat dit
beestige toestanden waren: ‘mensen hokken boven mekaar zoals kooitjes in
de vitrine van een hondenwinkel, daarin krioelen ze als meikevers in een
doos … soms was er maar één latrine voor zestig gezinnen’.

<span class="number">4</span> **Krotwoningen**
naar het schijnt heeft de fijnbesnaarde Victor Hugo de term *impasses*
bedacht. Daarvoor heetten ze *culs-de-sac*, maar dat vond hij vulgair.
Alsof de mensen die er woonden, daarvan wakker lagen… Zij leefden met de
modder, de stank, het afval, het lawaai en de herrie van de fabrieken.
Brussel kende drie cholera-epidemies, in 1832, 1848 en 1866 en alle drie
begonnen ze in de krottenwijken. De epidemie van 1866 velde, officieel,
3.469 mensen. Dat jaar telde Brussel in totaal bijna 160.000 bewoners
èn, in de Vijfhoek alleen, 375 *impasses*!
{: .section-start}

Rond die tijd begon de bourgeoisie met saneringen. Ze zag namelijk hoe
rijke burgers na de eerste cholera-epidemie de binnenstad ontvluchtten
en naar de nieuwe wijken buiten de ‘kleine ring’ verhuisden. Het
prestige en rendement van Brussel moesten dus opgevijzeld.

Vanaf de jaren 1900 kwamen er ‘sanitaire enquêtes’ om te zien hoe mensen
woonden. De enquête van de Marollen was het eerst klaar, tijdens de
Eerste Wereldoorlog. Saneringen werden nu volgens plan uitgevoerd, maar
nooit zo grondig dat de miserie helemaal werd weggewerkt.

Want kijk, een halve eeuw later, op een gure decemberdag in 1952 trekt
een merkwaardig duo langs de krotten van de hoofdstad. Een krant krijgt
lucht van de expeditie. Blijkt dat de koning (Boudewijn, ‘compleet
incognito’) zich door een zekere *abbé* Froidure zes uur lang langs 30
adressen heeft laten leiden, “*un voyage au bout de la misère*”.
Froidure is de stichter van de katholieke *Stations de Plein Air*. Een
maand later, in januari 1953, ligt er al een wetsontwerp ‘tegen de
krotwoningen’ ter discussie.

De volgende jaren worden in de grote steden de krotten geteld. Froidure
vermeldt de cijfers in zijn boek *Parias 57* (met als veelzeggende
ondertitel: *Les infra-salariés, les taudis, les enfants moralement
abandonnés*). Antwerpen heeft dan 10.000 krotwoningen, Gent 7300, Luik
3800 en Brussel-Stad 1100. Al die steden, en de oude industriestad Gent
nog het ergst, zijn eeuwenlang aangetast door de pest van de *impasses*.
In Brussel bestaan in de jaren 1950 nog altijd 110 van die gangetjes
waar de mensen in krotten wonen.

<span class="number">5</span> **Impasses, culs-de-sac, beluiken, gangen…**
Ook in de Verloren Hoek, tussen de Fabrieksstraat en de Vlaamse
Steenweg, lagen tientallen gangen met meer of minder bewoners… en meer
of minder prozaïsche namen. Je had daar bij voorbeeld de Peerlemoergang,
die via een flessehals uitgaf op de Papenvest. Vanaf de straat zag je
echter niet dat in deze *impasse*, verscholen achter de burgershuizen,
28 huizen stonden. Over de oorsponkelijke naam van deze gang bestaat
discussie. Naar het schijnt heette hij eerst Impasse des *Perle de
Moules*. Dat zou opgehemeld zijn tot Peerlemoergang. Maar die naam
verdwaalde in de Brusselse libertaire linguïstiek. Want later werd hij
*Impasse de la Perle d’Amour* genoemd, en vervolgens weer, in het
Vlaams, Liefdesparelgang. Brusselse zwans op zijn best.[^4]
{: .section-start}

<span class="number">6</span> **Verloren Hoek, Duivels&shy;hoek**
de Fabriekswijk (ook wel de Verloren Hoek of de kanaalwijk genoemd)
stond in Brussel ook als de Duivelshoek bekend. Er was een café met die
naam op de hoek van de Vandenbrandestraat en de
Onze-Lieve-Vrouw-van-Vaakstraat. Maar elk jaar in augustus was het ook
kermis in de wijk en dan trok er een Duivelsstoet uit, om de Duivel van
het voorbije jaren te begraven en de Duivel van het nieuwe jaar tot
leven te wekken en te dopen. Die traditie was geïnspireerd door een
legende die op haar beurt voortkwam uit, zegt men, waar gebeurde feiten.
Er bestaan veel versies van dat verhaal. Maar de versie van een zekere
Eric Timmermans is met namen en data gestoffeerd (al zijn het dan feiten
die uit een *Dictionnaire Infernal* zijn geplukt) en klinkt plausibel.
Een résumé van zijn verhaal in een volgend artikel.
{: .section-start}

[^1]: Lisette Danckaert, *Brussel. Vijf eeuwen cartografie*.

[^2]: Interview Roel Jacobs, 4 juni 2019

[^3]: *Brussel, breken en bouwen. Architectuur en stadsverfraaiing,
    1790-1914*.

[^4]: *Brussel. Stad van kunst en geschiedenis. N°27 Gangen in Brussel*