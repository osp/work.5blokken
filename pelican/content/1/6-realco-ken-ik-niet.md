title: Realco? Ken ik niet!
lang: nl
category: 1
rows: page-6-1 / span 11
cols: left / span 12
count: 2

"Realco ? Connais Pas \[Of hoe Jean-Baptist De Crée niet van zijn eerste
leugen is doodgevallen\]

<span class="number">1</span> Gesjoemel wordt vaak in achterkamers beklonken. Niet zo met de
projectontwikkelaar Realco. Die firma speelde het open en bloot met haar
project voor de Papenvest, zonder gêne, maar met culôt. Toch als we
afgaan op wat de archieven prijsgaven.
{: .section-start}

<span class="number">2</span> We schrijven 1998. De vijf blokken zijn ruim 30 jaar oud. Hoog tijd
voor een grondige renovatie. Maar die komt er niet. De Foyer
bruxellois/Brusselse Haard, de eigenaar, denkt wel aan een facelift en
schrijft een wedstrijd uit. Dan verschijnt de naamloze vennootschap
Realco & Partners op het toneel. Op 30 november 1998 schrijft Realco een
brief aan Jean-Baptiste De Crée (PS-schepen van de stad Brussel) in
diens hoedanigheid van voorzitter van de Foyer, met copie aan FXDD
(François-Xavier de Donnea), burgemeester van Brussel. [^1]
Jean-Baptiste De Crée, onthoud die naam.
{: .section-start}

<span class="number">3</span> Realco heeft eerder al met beide heren vergaderd over een ‘aanbod
voor de herontwikkeling van de Papenvest'. Realco heeft het aanbod ook
op papier gezet, in een brief van begin oktober. Het aanbod is
stoutmoedig. Hier komt het: sloop de 5 blokken, schrijft Realco, en laat
ons nieuwe woningen bouwen met de 1 miljard Belgische frank die het
Brussels Gewest daarvoor ter beschikking heeft. De details zijn nog
gedurfder: Realco zou voor de Foyer 320 sociale woningen bouwen waarvan
de helft aan de Papenvest en de rest elders in Brussel; Realco zou
echter de andere helft van het Papenvest-perceel verwerven en er
middenklassewoningen bouwen die het zelf zou verkopen, zogenaamd op
eigen risico maar wel met de 1 miljard van het Gewest als borg. Volgens
de promotor zou dit plan talrijke voordelen hebben: de Foyer krijgt
nieuwe woningen en in de Papenvest-wijk wordt een hinderlijk obstakel
gesloopt hetgeen de standing van de wijk zou opkrikken. Eén van de vier
afgevaardigd-bestuurders van Realco, Michel Mutsaars, ondertekent de
brief*.* (Terloops, een ander bestuurder - de Hasselaar Johan Tackoen -
staat bekend als ondernemend makelaar in vastgoed en promotor van
vastgoedprojecten).
{: .section-start}

<span class="number">4</span> Realco wil zijn penetratie in het hart van Brussel met 1 miljard
frank publiek geld laten subsidiëren. Daar komt zijn plan op neer. Maar
wie denkt dat De Cree en FXDD de Brusselse zaken gechoqueerd reageren en
de promotor stante pede wandelen sturen, vergist zich. Ze pikken diens
plan integendeel op. Jean-Baptist De Crée verdedigt het zelfs in het
openbaar.
{: .section-start}

Op 1 maart 1999 ontmoet hij bewoners van de 5 blokken, waaronder mevrouw
Stockman, en Johan Proot van het wijkcomité. Er staan volgens De Cree
twee opties open: renoveren of alles gaat weg en er komen klassieke
woningen van 4-5 verdiepingen in de plaats. ‘Mijnheer De Crée verkiest
de tweede optie; zo kunnen het levenskader, de veiligheid enzovoort
sterk verbeterd worden'. Ook aanwezig tijdens deze vergadering is
mijnheer Reyns, dan de directeur-generaal van de Foyer. Voor hem lijkt
het een uitgemaakte zaak dat de 5 blokken één voor één worden afgebroken
en dat de bewoners in fazen elders worden ondergebracht. Volgens De Crée
zal de hele werf zeker 7 jaar duren. Maar : niemand hoeft bang te zijn,
iedereen zal kunnen terugkeren.

<span class="number">5</span> De Foyer Bruxellois maakt zich nu op om de 5 blokken aan de
Papenvest te slopen en nieuwe woningen te bouwen. In augustus 2000
melden enkele kranten dat de Foyer Bruxellois voor de sloop-en-nieuwbouw
de stedenbouwkundige vergunning gaat vragen. FXDD in het stadhuis gaat
daarin mee. Maar de voorzitter van het OCMW, Yvan Mayeur, volgt niet.
Slopen is aanzienlijk duurder dan renoveren, is zijn argument. Een
renovatie zou 850 miljoen Bfr (21,07 miljoen euro) kosten, slopen zou
1,250 miljard Bfr (31 miljoen euro kosten). Volgens Mayeur is er ook
geen garantie dat er evenveel nieuwe sociale woningen worden gebouwd,
maar voorzitter De Crée van de Foyer spreekt dat formeel tegen. Vandanaf
gaat het nieuws dat de 5 blokken worden afgebroken als een makabere grap
rondwaren. Tot de Foyer en de stad in 2016 hun definitieve verdict
vellen: de blokken moeten weg, volgens een nieuw plan dat merkwaardig
veel overeenkomsten vertoont met de blauwdruk van Realco van nu 20 jaar
geleden (maar wel ruim 73 miljoen euro zal kosten).
{: .section-start}

<span class="number">6</span> Realco is sindsdien voor zover bekend van de Papenvest weggebleven.
Jean-Baptiste De Crée raakte in 2006 in woelig water. De Parti
Socialiste zette hem niet opnieuw op de lijst voor de
gemeenteraadsverkiezingen, waarna De Crée naar de liberale MR
overstapte. Die episode hangt vermoedelijk samen met een andere van
eerder dat jaar. Toen kwam bij de Foyer bruxellois namelijk grootschalig
geknoei aan het licht, deels op basis van een rapport dat nog altijd
publiek gemaakt moet worden. De toenmalige directeur van de Foyer,
Jean-Pierre Leenen, had volgens De Crée gevoelige informatie
doorgespeeld aan bouwpromotoren. Ook aan Realco soms? Leenen beet van
zich af: nee, De Crée wil mij buiten terwijl hij juist met Realco onder
één hoedje speelde. Leenen toen in de krant: "Hij (De Crée) zei me: ‘We
gaan vergaderen met bouwpromotor Realco. Misschien gaan we een speciaal
plan kunnen uitvoeren die de sociale mix in de wijk bevordert.' Ik heb
toen begrepen dat hij de helft van de Papenvest wou verkopen, nadat hij
alle blokken zou hebben afgebroken." De Crée antwoordde als zijn
gezapige zelf: Realco? Connais pas! (FM Brussel 3 maart 2006)
{: .section-start}

[^1]: Realco brief 30/11/1998 aan François-Xavier De Donnéa,
    Jean-Baptiste De Crée, Stadsarchief 109853.