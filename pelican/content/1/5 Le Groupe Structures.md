Title: Le Groupe Structures
lang: fr
category: 1
rows: page-5-1 / span 14
cols: span 12 / right
count: 3

<span class="number">1</span> On peut douter que les 5 blocs aient été un modèle d’architecture à
visée sociale. Peut-être leur mission et les objectifs assignés aux
architectes l'étaient-ils, mais non le résultat. Les architectes du
Groupe Structures ont dessiné des boîtes de construction en suivant un
schéma mathématique strict. Par la suite, ils furent critiqués : les 5
blocs et leurs alentours devaient héberger des gens, et cela les
architectes l’avaient apparemment oublié.
{: .section-start}

<span class="number">2</span> En 1961, le Foyer bruxellois chargea Structures d'établir un projet
de logements sociaux pour le Rempart des Moines. Les deux organismes se
connaissaient. Quelques années auparavant, la société de logements avait
demandé à Structures de dessiner les plans du nouveau bloc ‘Potiers'
derrière la place Anneessens.
{: .section-start}

<span class="number">3</span> Pour le Rempart des Moines, Structures proposa d’abord un plan de
base (dont la datest 26-3-1961) avec 4 tours (contenant ensemble 344
appartements), à savoir une tour A avec 56 flats, les tours B et C avec
112 flats chacune et la tour D avec 64 flats.[^1] Mais l’Urbanisme s’y
opposa. Trop haut, et trop dense, déclara ce service communal, il faut
baisser la jauge. De plus, l’administration communale voulait savoir de
quoi auraient l’air les façades et dans quel matériau elles seraient
construites, car « les 4 façades de ce site vont dominer les autres
bâtiments de ce quartier. ». Structures dut modifier son projet, dessina
cinq tours de 8 étages chacune avec 320 appartements au total, et put se
mettre au travail. C’est ainsi que fut édifié le site actuel. Les cinq
blocs furent achevés en 1966.
{: .section-start}

<span class="number">4</span> Les nouveaux blocs de logements devaient insuffler une vie
nouvelle au quartier. Depuis les années 1930, ce quartier n’avait cessé
de péricliter. La ville de Bruxelles n’avait jamais mis à exécution ses
plans d’assainissement de l’ancien quartier industriel, et l’avait
abandonné à son sort. Au milieu des années 1950, elle lança une nouvelle
impulsion et décida qu’il fallait y implanter un projet de grande
ampleur avec des logements sociaux, pour lequel on expropria des maisons
et des terrains. Mais l’implantation des 5 blocs fut un fameux échec.
Revenant sur ce dossier en 1997, l’architecte Marie Demanet ne trouve
rien de positif à dire sur les 5 blocs. Pour elle, c’est une
architecture minable qui a poussé encore davantage les habitants dans la
marginalité.[^2] Sven Sterken, dans un article à propos du Groupe
Structures, confirme l’analyse : plutôt que de revaloriser le quartier,
les 5 blocs l’ont encore plus enfoncé.[^3] Sept terrains étaient alors,
en 1997, en jachère autour du site des 5 blocs, signe du déclin du
quartier.
{: .section-start}

<span class="number">5</span> Quel était le ressenti des habitants ? Cette histoire, pour autant
que nous le sachions, est encore à écrire. Au début, certains avaient le
« spleen », la nostalgie du passé. Une habitante du bloc A a demandé au
bourgmestre de Bruxelles, en 1967, de donner aux blocs des noms
historiques, pour qu’ils aient de l’allure. Elle proposait de les
appeler Résidence du Rempart ou Résidence du Grand Serment. Pour l’Hôtel
de ville, c'était un pas trop loin.
{: .section-start}

<span class="number">6</span> L'échec avait des causes multiples. La première est que le Groupe
Structures était convaincu d’avoir raison. Ce bureau avait, dix années
auparavant, envoyé Raymond Stenier participer à un voyage d'étude aux
USA. Raymond Stenier était l’un des cofondateurs du Groupe Structures,
avec Jacques Boseret-Mali et Louis Van Hove. Le voyage aux États-Unis
était organisé par l’Office belge pour l’accroissement de la
productivité, créé dans le cadre du plan Marshall et de l’aide
américaine d’après-guerre en Europe. Stenier voyageait en compagnie
notamment du promoteur immobilier bruxellois Ado Blaton et de Lucien De
Vestel, qui allait dessiner le bâtiment Berlaymont pour la Commission
européenne.
{: .section-start}

Le groupe revint de son voyage enthousiasmé par le rêve américain. La
prospérité américaine, pouvait-on lire dans le rapport du voyage,
n'était pas principalement le résultat de l’avance technologique des
États-Unis mais davantage due au fait qu’y régnait un climat stimulant,
favorable aux entreprises, s’appuyant sur l’optimisme et l’esprit
d’entreprendre. Le Groupe Structures fit sienne cette idéologie. C’est
ainsi qu’il importa, pour un projet de construction à Wezembeek-Oppem,
le modèle du supermarché self-service venu des États-Unis. Le Groupe
Structures proposait également dans les années 1960 un ‘plan de
rénovation’ pour près de la moitié du Quartier Nord (appelé plus tard le
Plan Manhattan) et était ainsi à la base de la destruction ce quartier
densement peuplé.[^4]

<span class="number">7</span> Pour le Rempart des Moines, le Groupe Structures entendait
travailler en hauteur et avec des éléments préfabriqués, comme là aussi
aux États-Unis. Mais le Foyer bruxellois préféra les techniques de
construction classiques, qui étaient moins coûteuses. Par contre,
Structures réussit à imposer une formule mathématique « froide »
permettant de fournir 320 appartements : 5 tours avec 2 entrées et 8
étages chacune, et à chaque étage 4 appartements (un studio, et des
flats de 1, 2 ou 3 chambres à coucher), soit 5x\[2x(8x4)\] =320. Il ne
restait plus, autour des 5 blocs, suffisamment d’espace public pour que
les locataires puissent se l’approprier à leur guise. Le site était
fonctionnel, mais pas du tout convivial.
{: .section-start}

<span class="number">8</span> De son côté, le Foyer bruxellois devait se montrer économe de ses
deniers. Son plan consistait à démolir des taudis et à les remplacer par
des logements sociaux. Les besoins étaient criants. Mais les
expropriations étaient à ce point onéreuses et le prix du foncier si
élevé à Bruxelles que la société de logement chercha à réduire au
minimum les coûts de construction et, en même temps, à accroître la
rentabilité de ses projets. C’est dans cette logique qu’elle voulait
construire plus en hauteur et installer des magasins aux
rez-de-chaussée. Des motifs commerciaux jouaient donc aussi un rôle dans
l’action du Foyer bruxellois. Il en résulta que les locataires du
Rempart des Moines furent contraints d’habiter dans des boîtes de
construction – et encore : des boîtes de mauvaise qualité.
{: .section-start}

[^1]: Groupe Structures, les plans des logements sociaux Rempart des
    Moines ont été consultés aux archives de la ville de Bruxelles.

[^2]: Marie Demanet, *Projet pour l’avenir d’un site : le Rempart des
    Moines*, in : 3000 Foyers bruxellois, 1997, p. 66-69

[^3]: Sven Sterken, *Architecture and the Ideology of Productivity :
    Four Public Housing Projects by Groupe Structures in Brussels
    (1950-65)*, in : *Footprint, The European Welfare State Project :
    Ideals, Politics, Cities and Buildings*, Autumn 2011, vol. 5/2, pp.
    25-40

[^4]: Jozef Lievens, Nicole Brasseur, Albert Martens, *De grote stad,
    een geplande chaos? De Noordwijk van krot tot Manhattan*,
    Horizonreeks nr.29, 1975-2, Davidsfonds, Leuven, 1975, 100p. et
    illustrations en annexes.