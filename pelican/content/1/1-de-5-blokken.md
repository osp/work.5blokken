title: De 5 blokken
lang: nl
rows: page-2-1 / span 15
cols: left / span 4
count: 1
category: 1
fill: auto

<span class="number">1</span>‘5 blokken’ is de gangbare naam voor het complex Papenvest. Dat
bestaat uit vijf woontorens met in totaal 320 sociale woningen. De
bouwwerken begonnen in 1964 en waren in 1966 grotendeels klaar. Eigenaar
is de *Logement bruxellois*-Brusselse Woning (LBW). Dit is een ‘openbare
vastgoedmaatschappij’ die sociale woningen beheert. Voorheen heette ze
de *Foyer bruxellois*-Brusselse Haard.
{: .section-start }

<span class="number">2</span> De Foyer bruxellois is opgericht in 1922. De stad Brussel heeft de
meerderheid van de aandelen. Bij de 75-ste verjaardag in 1997 maakte La
Fonderie, het onderzoekscentrum voor de sociale geschiedenis van Brussel
in Molenbeek, een gedenkboek dat door de Foyer bruxellois werd
uitgegeven.[^1]
{: .section-start }

De huidige *Logement bruxellois*-Brusselse Woning is in 2016 ontstaan
uit de fusie van de Brusselse Haard en een andere sociale
woningmaatschappij voor Brussel, LoReBru\_GeBruWo. De nieuwe LBW
beheerde toen 3.883 woningen.

<span class="number">3</span> De 5 blokken zijn gebouwd op een perceel in het Westelijke deel van
het centrum van Brussel (de Vijfhoek), en vlakbij het kanaal
Brussel-Charleroi en de sluis aan de Ninoofsepoort. Dit deel van de
Vijfhoek droeg lang de naam Fabriekswijk, of ook Verloren Hoek of
Duivelshoek.
{: .section-start }

<span class="number">4</span> De Fabriekswijk was één en al economische activiteit in de 19-de
eeuw. Ze was dichtbebouwd met beluiken, '*impasses*' in het Frans. Het
was in feite een labyrint van ateliers en krotwoningen. In de jaren 1930
had de stad Brussel plannen om de Fabriekswijk te saneren. Honderden
ongezonde arbeiderswoningen zouden worden gesloopt. De
Grootsermentstraat werd getrokken. Maar de sanering volgde niet. Pas in
1955 herzag de stad Brussel haar plannen. Ze besliste dat hier ook
sociale woningen moesten komen en droeg aan de Brusselse Haard de
uitvoering van het project Papenvest op.
{: .section-start }

<span class="number">5</span> In 1961 kende de Brusselse Haard de werken toe aan de Groep
Structuur. ‘Het inplantingsplan voorzag vier woontorens van 13
verdiepingen en één toren van 10 verdiepingen, met in totaal 344
appartementen èn de kantoren van de Brusselse Haard”, zo vermeldt het
gedenkboek. Maar de dienst Stedenbouw van de stad verzette zich tegen
dit plan, hij vond de bevolkingsdichtheid te hoog. Stedenbouw
suggereerde om minder studio’s te voorzien en meer appartementen met 3
of 4 kamers. Er kwam een compromis en een nieuw plan met 320 sociale
woningen.
{: .section-start }

[^1]: Guido Vanderhulst e.a., *3000 Brusselse Haarden, dat wordt niet op
    één dag gebouwd*, Les dossiers de la Fonderie, n°2, oktober 1997,
    2-talig
