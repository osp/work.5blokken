title: Waarom revolteren <br>ze niet?
lang: nl
type: introtext
rows: page-1-15 / span 8
cols: center-right / span 6
count: 2

<!-- <h4>Een triptiek van het Brussels Brecht-Eislerkoor</h4> -->
De setting - In het centrum van Brussel staat een rij verwaarloosde en ongezonde sociale woontorens die binnenkort tegen de grond gaan: het zijn de vijf blokken van de Papenvest. De huidige bewoners moeten plaatsmaken voor een koopkrachtiger publiek.
Ons opzet - Het Brussels Brecht-Eislerkoor vertolkt de verwarring en de verwachtingen van de bewoners. Tenminste dat is onze betrachting. Het vroeg drie jaar onderzoek om deze kritieke situatie te begrijpen en om te peilen naar wat de bewoners denken en doen.
Het triptiek - We verzamelden veel informatie. Die delen we nu (eind 2019-begin 2020) via drie kanalen: ons muziektheaterstuk, vier lezingen (bij Muntpunt) en deze brochure.
Deze brochure bevat informatie over de 5 blokken, fragmenten uit de tekst van het muziektheaterstuk en duiding bij de titel. Want wat verstaan wij onder melancholie? Geen verslagenheid in ieder geval, wel een opstap naar verzet. Zodat wij de stad opnieuw in handen krijgen.
