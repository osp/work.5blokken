Title: Realco ? Connais Pas!
lang: fr
category: 1
rows: page-6-1 / span 11
cols: span 12 / right
count: 2

« Realco ? Connais pas » \[ou comment Jean-Baptiste De Crée n’est pas
mort de son dernier mensonge\]

<span class="number">1</span> Les petits arrangements se font souvent dans les coulisses. Pas pour
le promoteur immobilier Realco. Cette société y est allée, pour son
projet au Rempart des Moines, sans la moindre gêne mais au culot, en
jouant franc jeu. Du moins si l’on se base sur ce que révèlent les
archives.
{: .section-start}

<span class="number">2</span> Nous sommes en 1998. Les cinq blocs ont plus de 30 ans d'âge. Il est
grand temps de les rénover en profondeur. Mais rien ne se passe. Le
Foyer bruxellois/Brusselse Haard, propriétaire des blocs, pense à un
lifting et lance un concours. C’est là qu’apparaît la société anonyme
Realco & Partners. Le 30 novembre 1998, Realco adresse un courrier à
Jean-Baptiste De Crée (échevin PS de la ville de Bruxelles) en sa
qualité de président du Foyer, avec copie à FXDD (François-Xavier de
Donnea), bourgmestre de Bruxelles. [^1] Jean-Baptiste De Crée, retenez
bien ce nom.
{: .section-start}

<span class="number">3</span> Realco a déjà tenu avec les deux intéressés des réunions sur une
'offre pour le redéveloppement du Rempart des Moines'. Realco a même mis
l’offre sur papier, dans une lettre de début octobre. L’offre est
audacieuse. La voici : rasez les 5 blocs, écrit Realco, et laissez-nous
construire de nouveaux logements avec le milliard de francs que la
Région bruxelloise met à disposition à cet effet. Les détails sont plus
osés encore : Realco construirait pour le Foyer 320 logements sociaux
dont la moitié au Rempart des Moines et le reste ailleurs à Bruxelles ;
mais Realco deviendrait propriétaire de la moitié de la parcelle du
Rempart des Moines et y édifierait des logements moyens que la société
mettrait elle-même en vente, soi-disant à ses propres risques mais avec
le milliard de la Région pour garantie. Selon le promoteur, ce plan
présenterait de nombreux avantages : le Foyer recevra de nouveaux
logements et on éliminera, dans le quartier du Rempart des Moines, un
obstacle gênant ce qui revaloriserait le standing du quartier. Michel
Mutsaars, l’un des quatre administrateurs-délégués de Realco, signe la
lettre. (On notera au passage qu’un autre administrateur, Johan Tackoen
d’Hasselt, est connu pour être un intermédiaire entreprenant dans le
domaine de l’immobilier et de la promotion immobilière).
{: .section-start}

<span class="number">4</span> Realco entend faire subsidier sa pénétration au cœur de Bruxelles
par de l’argent public, à hauteur d’un milliard de francs. C’est à cela
que se ramène son plan. De Crée et FXDD vont-ils, choqués, réagir en
envoyant aussitôt promener le promoteur ? Que nenni. Bien au contraire,
ils reprennent le plan à leur compte. Jean-Baptiste De Crée va jusqu'à
le défendre publiquement. Le 1er mars 1999, il rencontre des habitants
des 5 blocs, dont madame Stockman et Johan Proot du comité de quartier.
Selon De Crée, deux options sont ouvertes : rénover ou tout enlever et
le remplacer par des maisons classiques de 4-5 étages. ‘Monsieur De Crée
préférerait la deuxième solution, on pourrait ainsi améliorer beaucoup
plus le cadre de vie, la sécurité, etc.'. Une autre personne présente à
la réunion est monsieur Reyns, alors directeur général du Foyer. Pour
lui, il est clair que les 5 blocs seront démolis l’un après l’autre et
que les habitants seront relogés ailleurs, par phases. D’après De Crée,
l’ensemble du chantier durera certainement 7 ans. Mais personne ne doit
avoir peur, chacun pourra revenir.
{: .section-start}

<span class="number">5</span> Le Foyer bruxellois se prépare alors à démolir les 5 blocs du
Rempart des Moines et à construire de nouveaux logements. En août 2000,
quelques journaux signalent que le Foyer Bruxellois va demander le
permis d’urbanisme pour les travaux de démolition et de reconstruction.
FXDD, à l’Hôtel de ville, est d’accord. Mais Yvan Mayeur, le président
du CPAS, ne suit pas. Son argument : démolir coûte bien plus cher que
rénover. Une rénovation coûterait 850 millions de FB (21,07 millions
d’euros), démolir reviendrait à 1,250 milliard de FB (31 millions
d’euros). D’après Mayeur, on n’a aucune garantie quant à la construction
d’autant de nouveaux logements sociaux, mais le président du Foyer, De
Crée, affirme le contraire. À partir de là, l’information selon laquelle
les 5 blocs vont être rasés continue de faire son macabre chemin.
Jusqu'à ce que le Foyer et la ville prononcent, en 2016, leur verdict
définitif : les blocs doivent disparaître, selon un nouveau plan qui
présente, curieusement, de nombreux points communs avec le projet de
Realco d’il y a 20 ans (mais qui coûtera plus de 73 millions d’euros).
{: .section-start}

<span class="number">6</span> Depuis lors, pour autant que l’on sache, Realco est resté à l'écart
du Rempart des Moines. Jean-Baptiste De Crée s’est retrouvé, en 2006,
dans de sales draps. Le Parti Socialiste ne l’a plus fait figurer sur sa
liste pour les élections communales, sur quoi De Crée est passé chez les
libéraux du MR. Cet épisode n’est sans doute pas sans rapport avec des
événements intervenus un peu plus tôt la même année. À ce moment en
effet, il s’avéra qu’il y avait eu des malversations à grande échelle au
Foyer bruxellois : c’est ce que révéla, en partie, un rapport qui n’a
toujours pas été rendu public. Selon De Crée, Jean-Pierre Leenen –
directeur du Foyer à l'époque ‒ aurait transmis des informations
sensibles à des promoteurs immobiliers. Et pourquoi pas à Realco ?
Leenen se défendit bec et ongles : non, De Crée veut me virer alors que
c’est lui qui travaille main dans la main avec Realco. Il déclara à la
presse : « Il \[De Crée\] m’a dit : ‘On va tenir une réunion avec le
promoteur Realco. Peut-être pourrons-nous mettre en œuvre un plan
spécial qui favorise la mixité sociale dans le quartier.' J’ai compris
alors qu’il voulait vendre la moitié du Rempart des Moines, après avoir
rasé tous les blocs. » De Crée réagit sur son habituel ton complaisant :
Realco ? Connais pas ! (FM Brussel 3 mars 2006).
{: .section-start}

[^1]: Lettre de Realco à François-Xavier De Donnéa et Jean-Baptiste De
    Crée du 30/11/1998. Archives de la ville 109853.