title: De Groupe Structures
lang: nl
category: 1
rows: page-5-1 / span 14
cols: left / span 12
count: 3

<span class="number">1</span> Dat de 5 blokken een modelontwerp van sociaal bewogen architekten
waren, valt te betwijfelen. Misschien waren de opdracht en het opzet
sociaal bewogen, maar de uitkomst was dat niet. De architekten van
Groupe Structures tekenden bouwdozen, volgens een strak wiskundig
schema. Later kregen ze kritiek: in en aan de 5 blokken moesten mensen
wonen, dat waren de architekten blijkbaar vergeten.
{: .section-start}

<span class="number">2</span> Structures kreeg in 1961 van de Brusselse Haard de opdracht om
sociale woningen te ontwerpen voor de Papenvest. De twee kenden elkaar.
Een paar jaar vroeger had de woningmaatschappij aan Structures gevraagd
het nieuw woonblok ‘Potiers’ (Pottenbakkers) te ontwerpen, achter het
Anneessensplein.
{: .section-start}

<span class="number">3</span> Voor de Papenvest stelde Structures eerst een grondplan voor (datum:
26-3-1961) met 4 torens (met tesamen 344 appartementen), namelijk
toren-A met 56 flats, de torens B en C met elk 112 flats en toren D met
64 flats. Maar Stedenbouw verzette zich. Te hoog en teveel, zei deze
stadsdienst, dat moet minder. En het stadsbestuur wilde weten hoe de
gevels eruit zouden zien en in welke materialen ze uitgevoerd zouden
worden, want "de 4 façades van deze site gaan de andere gebouwen van de
wijk domineren". Structures moest bijdraaien, tekende vijf torens van
elk 8 verdiepingen en 320 appartementen en mocht ze realiseren. Zo kwam
de huidige site tot stand. In 1966 waren de vijf blokken klaar.[^1]
{: .section-start}

<span class="number">4</span> De nieuwe woonblokken moesten de wijk nieuw leven inblazen. Met die
wijk was het vanaf de jaren 1930 verder bergaf gegaan. De stad Brussel
had haar plannen om de oude industriële buurt te saneren nooit
uitgevoerd, en de buurt aan haar lot overgelaten. Midden de jaren 1950
gaf ze een nieuwe impuls en besliste ze dat hier een project van omvang
met sociale woningen moest komen, waarvoor ook huizen en terreinen
werden onteigend. Maar de inplanting van de 5 blokken was een fameuze
miskleun. In een terugblik in 1997 heeft de architekte Marie Demanet
geen goed woord voor de 5 blokken. Haar oordeel: minabele architektuur
die de bewoners nog meer in de marginaliteit duwde.[^2] Sven Sterken, in
een artikel over de Groupe Structure, moet dat beamen. In plaats van de
wijk op te krikken, duwden de 5 blokken haar verder in de dieperik.[^3]
Rond de site van de 5 blokken lagen er toen, in 1997, zeven terreinen
braak, als teken van de aftakeling van de wijk.
{: .section-start}

<span class="number">5</span> Hoe voelden de bewoners zich? Die geschiedenis is, voor zover
bekend, niet geschreven. In het begin hadden sommigen "spleen", heimwee
naar vroeger. Een bewoonster van blok-A vroeg in 1967 aan de Brusselse
burgemeester om de blokken historische namen te geven, dan zouden ze
allure krijgen. Ze stelde voor ze Résidence du Rempart of Résidence du
Grand Serment te noemen. Dat vond het stadhuis te ver gaan.
{: .section-start}

<span class="number">6</span> De mislukking had meerdere oorzaken. De eerste is dat ze bij de
Groupe Structures overtuigd waren van hun gelijk. Dit bureau had tien
jaar eerder Raymond Stenier meegestuurd met een studiereis naar de VS.
Raymond Stenier was mede-stichter van de Groupe Structures, samen met
Jacques Boseret-Mali en Louis Van Hove. De trip naar de VS was
georganiseerd door de Belgische Dienst ter Verhoging van de
Productiviteit, opgericht in het kader van de Marshall-plan en de
Amerikaanse na-oorlogse hulp aan Europa. Stenier reisde in het
gezelschap van onder andere de Brusselse vastgoed-baas Ado Blaton en
Lucien De Vestel die voor de Europese Commissie het Berlaymont-gebouw
zou tekenen.
{: .section-start}

De groep keerde terug bezield door de American Dream. De Amerikaanse
welvaart, zo stond in het reisverslag, kwam niet zozeer van het feit dat
ze technisch vooruit zijn, maar omdat daar een stimulerend
ondernemingsklimaat bestaat dat steunt op optimisme en ondernemingszin.
De Groupe Structures slorpte deze ideologie op. Voor een bouwproject in
Wezembeek-Oppem importeerde hij bij voorbeeld het model van de self
service supermarkt uit de VS. Later in de jaren 1960 stelde de Groupe
Structures een ‘vernieuwingsplan’ voor een deel van de Noordwijk op (dat
later het Manhattanplan werd genoemd) en lag zo aan de basis van
verwoesting van deze dichtbevolkte wijk.[^4]

<span class="number">7</span> Voor de Papenvest wilde de Groupe Structure in de hoogte en met
prefab-elementen werken, ook zoals in de VS. Maar de Brusselse Haard
verkoos de klassieke bouwtechnieken want die waren goedkoper. Wat
Structures wel doorduwde, was een koude wiskundige formule die 320
appartementen opleverde: 5 torens met elk 2 ingangen en 8 verdiepingen
met op elke verdieping 4 appartementen (een studio, en flats met 1, 2 of
3 slaapkamers) ofwel 5x\[2x(8x4)\] =320. De site van de 5 blokken was
wel doorwaadbaar, met rondom openbare ruimte die de huurders zich echt
volgens hun gevoel konden eigen maken. De site was functioneel, maar
niet gezellig.
{: .section-start}

<span class="number">8</span> De Brusselse Haard van haar kant moest op de centen letten. Haar
plan was krotwoningen op te ruimen en te vervangen door sociale
woningen. Daar was grote nood aan. Maar de onteigeningen kostten zoveel
geld en de Brusselse grondprijzen lagen zo hoog dat de
woningmaatschappij de bouwkosten zo laag mogelijk hield en tegelijk
probeerde de rendabiliteit van haar projecten op te drijven. Vanuit die
logica wilde ook zij hoger bouwen en van de gelijkvloerse ruimten
winkels maken. Ook bij de Brusselse Haard speelden dus commerciële
motieven. Het gevolg was wel dat de huurders aan de Papenvest in
blokkendozen moesten gaan leven, en dan nog blokkendozen van slechte
kwaliteit.
{: .section-start}

[^1]: Groupe Structures, plannen sociale woningen Papenvest,
    geraadpleegd in het Brussels stadsarchief.
[^2]: Marie Demanet, *Project voor de toekomst van een site: de
    Papenvest*, in: *3000 Brusselse haarden, 75-jarige bestaan De
    Brusselse Haard*, 1997, p.66-69
[^3]: Sven Sterken, *Architecture and the Ideology of Productivity: Four
    Public Housing Projects by Groupe Structures in Brussels (1950-65)*,
    in: *Footprint, The European Welfare State Project: Ideals,
    Politics, Cities and Buildings*, Autumn 2011, vol. 5/2, pp. 25-40
[^4]: Jozef Lievens, Nicole Brasseur, Albert Martens, *De grote stad,
    een geplande chaos? De Noordwijk van krot tot Manhattan*,
    Horizonreeks nr.29, 1975-2, Davidsfonds, Leuven, 1975, 100p. en
    illustraties in bijlagen.