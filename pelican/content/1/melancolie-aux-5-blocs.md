title: Pourquoi ne se révoltent-ils pas?
lang: fr
type: introtext
rows: page-1-15 / span 8
cols: span 6 / right
count: 2

<!-- <h4>Un triptyque de la chorale Brecht-Eisler de Bruxelles</h4> -->
L’endroit - Au centre de Bruxelles se trouvent cinq barres de logements sociaux insalubres et laissés à l’abandon : les cinq immeubles du Rempart aux Moines. Les 5 blocs vont bientôt être rasés. Les habitants doivent faire de la place pour un public plus nanti.
Notre objectif - La chorale Brecht-Eisler de Bruxelles évoque le désarroi et les espoirs de ces habitants. C’est du moins ce que nous tentons de faire. Il nous a fallu trois ans pour saisir cette situation critique et sonder ce que font et pensent les habitants.
Le triptyque - Nous avons récolté beaucoup d’informations. Nous partageons aujourd’hui ces matériaux (fin 2019-début 2020) via trois canaux: notre pièce de théâtre chanté, quatre conférences et la présente brochure.
La brochure contient des infos sur les 5 blocs, des extraits des textes de notre pièce de théâtre musical et une interprétation du titre. Qu’entendons-nous par ‘mélancolie'? Pas la déprime en tout cas, mais bien une dynamique de résistance. Pour que nous puissions nous réapproprier la ville.
