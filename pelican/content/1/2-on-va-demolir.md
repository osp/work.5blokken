Title: C’est décidé : on va démolir
lang: fr
rows: page-2-10 / span 5
cols: 16 / span 12
category: 1
count: 4

> La décision est tombée :  
> ceux qu’on appelle les 5 blocs,  
> les 5 barres de notre quartier,  
> nos tours sociales,  
> nos habitations,  
> Rasés !

<span class="number">1</span> L’info date du 30 juin 2016 : les cinq tours d’habitation au
Rempart des Moines vont être démolies. Le gouvernement bruxellois
approuve ce projet. Il émane de la ville de Bruxelles et du Logement
bruxellois, propriétaire et exploitant des logements sociaux au
Rempart des Moines. Selon un communiqué de presse, la ville et le
propriétaire mettent, ensemble, 73,5 millions d’euros sur la table
pour financer la démolition et la reconstruction. Le communiqué de
presse dit textuellement : « le projet envisage la démolition des
314 logements sociaux existants et la reconstruction de 350 nouveaux
logements, parmi lesquels 210 logements sociaux et 140 logements
moyens . (...) Le projet devrait également intégrer un parking
souterrain. »
{: .section-start}


<span class="number">2</span> Du coup, le nouveau complexe de logements comptera 110 logements
sociaux de moins que le site actuel. Du coup, le quartier se voit
gratifié d’un nouveau parking souterrain. Un parking pour 300 autos
– c’est ce qu’on croit dans un premier temps, il s’avérera par la
suite qu’on prévoit 400 emplacements.
{: .section-start}

Du coup, le nouveau complexe n’aura pas un, mais deux exploitants :
le Logement bruxellois pour les 210 logements sociaux, et la Régie
foncière de la ville de Bruxelles pour les logements moyens. C’est
ce que révèle la discussion au conseil communal de Bruxelles le 7
novembre 2016 : on suppute alors que la Régie foncière entend mettre
ces 140 logements moyens sur le marché commercial de la location. Ce
qui porterait atteinte au caractère social du complexe du Rempart
des Moines.

<span class="number">3</span> Par conséquent, le communiqué de presse du 30 juin ne dit pas tout.
Il présente les choses sous un jour plus favorable que ce qu’elles
sont en réalité. C’est ainsi que la démolition se justifie, nous
citons : «Le bâti obsolète et le manque d’isolation des 5 tours du
site étaient devenues source de nuisances, d’insalubrité et
d’inconfort pour les quelque 850 locataires du site, sans compter
les lourdes dépenses énergétiques auxquelles ils doivent faire
face. ». Le Rempart des Moines doit donc s’attendre à un « vaste
programme de rénovation ». Mais démolir, ce n’est pas rénover.
Démolir, c’est démolir, point à la ligne.
{: .section-start}