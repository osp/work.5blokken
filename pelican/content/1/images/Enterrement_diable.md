title: Enterrement_diable		
type: image
size: medium
category: 1
cols: 10 / span 3
rows: page-4-8 / span 3

<div style="position: relative; top: -8mm" markdown=true>

![]({attach}Enterrement_diable.jpg)

Begrafenis van de Duivel, 1893 (stadsarchief Brussel)

Enterrement du Diable, 1893 (archives Bruxelles)

</div>