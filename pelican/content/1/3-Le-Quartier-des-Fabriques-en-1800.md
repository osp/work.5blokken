Title: Le Quartier des Fabriques en 1800
lang: fr
category: 1
rows: page-3-1 / span 16
cols: center-right / span 12
count: 4
fill: auto

<span class="number">1</span> À l'époque de Napoléon, personne n’habitait à l’endroit où se
trouvent aujourd’hui les 5 blocs. Mais peu de temps après, la partie
occidentale du Pentagone commença à se remplir, et très vite. Il y avait
là de la place, pour l’industrie. Et les patrons attiraient des
ouvriers. De la même façon que le Pentagone accueillait alors des
populations de toutes sortes – des populations au sens prolétaire du
mot : demandeurs d’emploi, candidats à l’Eldorado, déracinés, ça a dû
être une véritable explosion. Cette partie de la ville a débordé. Pour
le plus grand bonheur des classes entrepreneuses, tandis que le peuple,
lui, devait s’entasser dans des taudis. Et dès que le canal de Bruxelles
à Charleroi fut creusé et ouvert aux bateaux (en 1832), et que le port
créa de nouveaux emplois encore, ce quartier se mit à ressembler aux
Marolles. Ici aussi on vit naître un quartier de fabriques et de
cheminées avec entre les deux quelques bras de la Senne et un labyrinthe
d’impasses misérables. À partir de là, le Coin perdu se tailla la
réputation d’un quartier populaire bruxellois authentique que les
bourgeois n’osaient pas fréquenter.
{: .section-start}

Dès l’époque française existaient deux rues qui délimitent aujourd’hui
la parcelle des 5 blocs. Le régime français les rebaptisa pendant un
temps : Notre Dame du Sommeil devint la *Rue du Calendrier Républicain*,
le Rempart des Moines devint le *Rempart Cisalpin.* Le Rempart des
Moines était un vestige des ‘cleyne vesten', les petites fortifications
qui allaient jusqu'à l’actuelle place Fontainas. Mais comme le cœur de
Bruxelles était en pleine croissance, cette enceinte fortifiée cessa
d'être utilisée dès les années 1300. Dans les années 1600, on assécha le
Paepengrecht, le Fossé aux Moines qui jouxtait le rempart, pour en faire
des parcelles. La rue des Fabriques figure déjà, elle aussi, sur les
cartes de l'époque française. Elle traversait de bout en bout le domaine
des Chartreux (situé à peu près à la place du Jardin aux Fleurs) pour
aboutir à l’endroit où serait construite, par la suite, la porte de
Ninove.[^1]

Mais entre la rue des Fabriques et la rue de Flandre se trouvait une
zone verte et humide qu’on allait ensuite appeler la Coin perdu, avec
des fossés et des prés à blanchir le drap, outre un ruisseau empuanti
par les ordures en décomposition et les cadavres d’animaux. Ce ruisseau,
le Bummel, a été comblé par la suite pour édifier la rue Dansaert. Deux
ou trois bras de la Senne coulaient encore alors dans cette partie de la
ville. C'étaient des égouts à ciel ouvert, où les fabriques et les
hommes jetaient leurs ordures.

<span class="number">2</span> Dès le début des années 1800, de nouvelles industries se sont
installées à Bruxelles. À un rythme tel que Bruxelles était alors le
véritable cœur industriel de la Belgique. Les bassins industriels
wallons ne viendront qu’après. Les fabriques s'édifiaient là où il y
avait de la place, au Coin perdu, la zone verte de la partie occidentale
de la ville. Les patrons habitaient des maisons patriciennes côté rue,
les ateliers étaient à l’arrière. Et plus à l’arrière encore, là où
restait de l’espace, ils entassaient les ouvriers dans des taudis.[^2]
Les gens y vivaient les uns sur les autres, pauvrement, dans des
conditions abominables.
{: .section-start}

<span class="number">3</span> L’ensemble de ce terrain situé entre les bras de la rivière et sur
ses berges fut transformé en parcelles, les espaces intérieurs occupés
par un maximum d’habitations minuscules. Les taudis étaient disposés en
'allées’ ou en ‘bataillons carrés', tout comme les fameuses impasses de
Gand. L’impasse de la Porte d’eau par exemple (aujourd’hui disparue,
latérale à la rue des Chartreux) n’avait que 2,5 mètres de large mais on
y trouvait une quinzaine de taudis, en trois allées, de 12 centiares
chacun (12 m2). Et juste à côté il y avait une brasserie qui privait
l’impasse de lumière et d’air.
{: .section-start}

Le Pentagone comptait en 1800 66.000 habitants, en 1890 ils étaient
159.000. Telle est l’ampleur statistique de l’explosion. À partir de ce
moment, c’est la partie Ouest de la ville qui fut la plus densément
peuplée. C’est toujours le cas aujourd’hui. C’est de ce lotissement
sauvage que datent les '*impasses*' bruxelloises, des artères sans issue
où les gens vivaient dans la plus grande promiscuité. C’est ici
qu’habitaient les ouvriers d’usine, les dockers, les bateliers, les
manœuvres, les petites mains de tout genre. Leur situation était
lamentable. Lorsqu’ont été publiées vers 1840 les premières études sur
la situation sociale dans le Royaume de Belgique (de Quételet et
Ducpétiaux), il s’est avéré qu’on mourait davantage dans ces impasses
qu’ailleurs à Bruxelles, ‘les taux de mortalité suivaient le tracé de
ces impasses'.[^3] Par la suite, un chroniqueur écrivit que c'étaient là
des conditions de vie bestiales : ‘les gens sont entassés les uns sur
les autres comme des cages dans la vitrine d’un magasin de chiens, et
ils y grouillent comme des hannetons dans une boîte ... parfois, il n’y
avait qu’une seule latrine pour soixante familles.'

<span class="number">4</span> **Les taudis**
il semble que ce soit Victor Hugo, fin lettré, qui ait créé le mot
*impasses*. Auparavant, on disait *culs-de-sac*, mais il trouvait le
terme vulgaire. Comme si les gens qui y habitaient en avaient quelque
chose à cirer ... Ils vivaient dans la boue, la puanteur, les
moisissures, le bruit et le vacarme des fabriques. Bruxelles a connu
trois épidémies de choléra : en 1832, en 1848 et en 1866. Toutes trois
ont débuté dans les impasses à taudis. L'épidémie de 1866 a
officiellement fait 3.469 victimes. Cette année-là, Bruxelles comptait
au total près de 160.000 habitants et, dans le seul Pentagone, 375
impasses ! C’est à cette époque que la bourgeoisie a commencé des
travaux d’assainissement. Elle se rendait compte, en effet, que les
bourgeois aisés avaient quitté le centre ville après la première
épidémie de choléra pour gagner les nouveaux quartiers situés en dehors
de la ‘petite ceinture'. Il fallait donc rehausser le prestige et le
rendement de Bruxelles.
{: .section-start}

À partir des années 1900, on mena des ‘enquêtes sanitaires’ pour voir
comment les gens étaient logés. L’enquête sur les Marolles fut la
première à être publiée, pendant la Première Guerre mondiale. Les
opérations d’assainissement furent désormais entreprises de façon
planifiée, mais insuffisamment en profondeur de sorte qu’on ne parvint
jamais à éradiquer la pauvreté.

Car voilà qu’un demi-siècle plus tard, par une froide journée de
décembre de 1952, un couple improbable erre parmi les taudis de la
capitale. Un journal a vent de l’expédition. Il s’avère que le roi
(Baudouin, totalement incognito) s’est fait guider six heures durant,
visitant 30 adresses, par un certain abbé Froidure, « *un voyage au bout
de la misère* ». Froidure est le fondateur des *Stations de Plein Air
c*atholiques. Dès le mois suivant, en janvier 1953, un projet de loi
'pour la liquidation des taudis’ est en discussion.

Au cours des années qui suivent, on dénombre les taudis. Froidure
mentionne ces chiffres dans son livre *Parias 57* (dont le sous-titre,
très parlant, est : *Les infra-salariés, les taudis, les enfants
moralement abandonnés*). Anvers compte alors 10.000 taudis, Gand 7.300,
Liège 3.800 et Bruxelles-Ville 1.100. Toutes ces villes, la pire étant
Gand, vieille ville d’industries, ont été touchées pendant des siècles
par la peste des impasses. Dans les années 1950 existent encore toujours
à Bruxelles 110 de ces impasses, où les gens habitent des taudis.

<span class="number">5</span> **Impasses, culs-de-sac**...
Le Coin perdu, entre la rue des Fabriques et la rue de Flandre, abritait
également des dizaines d’impasses avec plus ou moins d’habitants...
portant des noms plus ou moins prosaïques.On y trouvait par exemple
l’impasse de la Perle d’Amour, qui donnait sur le Rempart des Moines via
un goulet. Mais de la rue, on ne voyait pas que cette impasse, cachée
derrière des maisons bourgeoises, comptait 28 maisons. Le nom d’origine
de cette impasse fait l’objet de débats. Il semble qu’elle se soit
d’abord appelée Perle de Moules. Ce qui aurait donné, par un phénomène
de valorisation, Peerlemoergang. Mais ce nom s’est perdu dans les
méandres de la linguistique libertaire de Bruxelles. Car elle a ensuite
été nommée Impasse de la Perle d’Amour puis à nouveau, en néerlandais,
Liefdesparelgang. De la zwanze bruxelloise pur jus.[^4]
{: .section-start}

<span class="number">6</span> **Le Coin perdu, le Coin du diable**
le Coin perdu ou quartier du canal était également connu à Bruxelles
sous le nom de Coin du diable, Duivelshoek. Un café portait ce nom au
coin de la rue Vandenbranden et de la rue Notre-Dame-du-Sommeil. Mais
chaque année, c'était également la fête dans le quartier et un cortège
diabolique était de sortie, afin d’enterrer le Diable de l’année écoulée
et de présenter le Diable de l’année nouvelle, qui était ensuite
baptisé. Cette tradition s’inspirait d’une légende issue elle-même,
dit-on, de faits réels. Il en existe de nombreuses versions. La version
d’un certain Eric Timmermans, dotée de noms et de dates (toutefois
inspiré d’un douteux *Dictionnaire Infernal*), semble plausible. Nous la
résumerons dans un article suivant.
{: .section-start}

[^1]: Lisette Danckaert, *Brussel, Vijf eeuwen cartografie*

[^2]: Interview avec Roel Jacobs, 4 juin 2019

[^3]: *Brussel, breken en bouwen. Architectuur en stadsverfraaiing,
    1790-1914*

[^4]: *Bruxelles. Ville d’art et d’histoire. N°27, Impasses de
    Bruxelles*