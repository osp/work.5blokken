title: Martini_Ground_Zero
type: image
cols: 5 / span 4
rows: page-8-5 / span 3
size: medium
category: 2
source: references


![]({attach}Martini_Ground_Zero.jpg)

Timelapse : sloop van Martini-toren (Yves Bernard)

Timelapse : démolition de la Tour Martini (Yves Bernard)