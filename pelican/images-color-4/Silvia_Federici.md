title: Silvia_Federici.jpg 
type: image
rows: page-9-12 / span 3
cols: 8 / span 3
size: medium
category: 4


![]({attach}Silvia_Federici.jpg)

‘De ‘commons’ (gemeengoed) zijn kiemen van de post-kapitalistische maatschappij’ (Silvia Federici)

‘Les biens communs sont les embryons de la société post-capitaliste’ (Silvia Federici) 