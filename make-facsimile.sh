mutool poster -x 2 5blokken-facsimile-timeline.pdf 5blokken-facsimile-timeline-split.pdf
mutool poster -x 2 5blokken-facsimile-body.pdf 5blokken-facsimile-body-split.pdf


#pdftk A=one.pdf B=two.pdf cat A1-7 B1-5 A8 output combined.pdf

pdftk A=5blokken-facsimile-body-split.pdf B=5blokken-facsimile-timeline-split.pdf cat A2-12 B1-2 A13-23 output 5blokken-facsimile-dirty.pdf

gs \
	-dNOPAUSE \
	-dBATCH \
	-sDEVICE=pdfwrite \
  -dDownsampleColorImages=true \
  -dDownsampleGrayImages=true \
  -dDownsampleMonoImages=true \
  -dColorImageResolution=300 \
  -dGrayImageResolution=300 \
  -dMonoImageResolution=300 \
  -dColorImageDownsampleThreshold=1.0 \
  -dGrayImageDownsampleThreshold=1.0 \
  -dMonoImageDownsampleThreshold=1.0 \
	-sOutputFile=5blokken-facsimile.pdf 5blokken-facsimile-dirty.pdf

gs \
	-dNOPAUSE \
	-dBATCH \
	-sDEVICE=pdfwrite \
  -dDownsampleColorImages=true \
  -dDownsampleGrayImages=true \
  -dDownsampleMonoImages=true \
  -dColorImageResolution=96 \
  -dGrayImageResolution=96 \
  -dMonoImageResolution=96 \
  -dColorImageDownsampleThreshold=1.0 \
  -dGrayImageDownsampleThreshold=1.0 \
  -dMonoImageDownsampleThreshold=1.0 \
	-sOutputFile=5blokken-facsimile-web.pdf 5blokken-facsimile.pdf