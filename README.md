# work.5blokken

Publication project with Raf Custers accompanying the lectures and concerts series “Melancholie aan de 5 blokken”.

# Making the PDF
The make.sh scripts splits the spreads for the body content into pages and inserts the timeline.
( Requirements: mutool, pdftk )

- Print the publication from the browser to `body.pdf` in the root folder of the repository
- Print the timeline from the browser to `timeline.pdf` in the root folder of the repository
- Run `sh make.sh` in the root folder of the repository,
  a new file 5blokken.pdf should have been generated

- Generate a PDF from chrome
- Split pages using mutool
  `mutool poster -x 2 file.pdf file-split.pdf`

