mutool poster -x 2 timeline-black.pdf timeline-black-split.pdf
mutool poster -x 2 body-black.pdf body-black-split.pdf
pdftk A=body-black-split.pdf B=timeline-black-split.pdf cat A2-12 B1-2 A13-23 output 5blokken-black-dirty.pdf

mutool poster -x 2 timeline-purple.pdf timeline-purple-split.pdf
mutool poster -x 2 body-purple.pdf body-purple-split.pdf
pdftk A=body-purple-split.pdf B=timeline-purple-split.pdf cat A2-12 B1-2 A13-23 output 5blokken-purple-dirty.pdf

gs \
	-dNOPAUSE \
	-dBATCH \
	-sDEVICE=pdfwrite \
  -sProcessColorModel=DeviceGray \
  -sColorConversionStrategy=Gray \
  -dDownsampleColorImages=true \
  -dDownsampleGrayImages=true \
  -dDownsampleMonoImages=true \
  -dColorImageResolution=300 \
  -dGrayImageResolution=300 \
  -dMonoImageResolution=300 \
  -dColorImageDownsampleThreshold=1.0 \
  -dGrayImageDownsampleThreshold=1.0 \
  -dMonoImageDownsampleThreshold=1.0 \
	-sOutputFile=5blokken-black.pdf 5blokken-black-dirty.pdf

gs \
	-dNOPAUSE \
	-dBATCH \
	-sDEVICE=pdfwrite \
  -sProcessColorModel=DeviceGray \
  -sColorConversionStrategy=Gray \
  -dDownsampleColorImages=true \
  -dDownsampleGrayImages=true \
  -dDownsampleMonoImages=true \
  -dColorImageResolution=300 \
  -dGrayImageResolution=300 \
  -dMonoImageResolution=300 \
  -dColorImageDownsampleThreshold=1.0 \
  -dGrayImageDownsampleThreshold=1.0 \
  -dMonoImageDownsampleThreshold=1.0 \
	-sOutputFile=5blokken-purple.pdf 5blokken-purple-dirty.pdf